app



.factory('Auth', ['$cookies', '$http', '$base64', function ($cookies, $http, $base64) {

    // initialize to whatever is in the cookie, if anything
    if ($cookies.authdata)
    	$http.defaults.headers.common['Authorization'] = 'Basic ' + $cookies.authdata;
 
    return {
    	hasCredentials: function() {

    		if ($cookies.authdata)
    			return true ;

    		return false ;
    	}, 
        setCredentials: function (username, password) {
            var encoded = $base64.encode(username + ':' + password);
            $http.defaults.headers.common.Authorization = 'Basic ' + encoded;

            console.log($http.defaults.headers.common) ;

            $cookies.authdata = encoded;
        },
        clearCredentials: function () {
            document.execCommand("ClearAuthenticationCache");
            $cookies.authdata = undefined ;
            $http.defaults.headers.common.Authorization = 'Basic ';
        }
    };
}])

.factory('Users', ['Restangular', function(Restangular) {

  var userList = [] ;

  return {

    getUserList: function() {
    	Restangular.all("users").getList().then(
    			function (data) {
    				//userList=data.slice();
    				//userList.push.apply(userList, data);
    				//userList.push(data) ;
    				userList.length = 0;
    				userList.push.apply(userList, data);
    				console.log("Services Data ="+data.length);
    			},
    			function (error)
    			{
    				console.log("Unable to fetch UserList");
    			});
    	return userList ;
    },
    saveUser: function(user)
    {
    	var success=false;
    	Restangular.all("users").post(user)
    		.then(
    			function (result) {
    				console.log("User Posted to Server") ;
    				success=true;
    			},
    			function (error) {
    				$scope.error = { message: error.data.message } ;
    				console.log(error) ;
    				success=true;
    			}
    		);
    	return success;
    	
    }
    

  }
}]);









