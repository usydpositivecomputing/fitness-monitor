var app = angular.module('xp', ['restangular','ui.bootstrap', 'ngRoute', 'ngSanitize', 'ngCookies', 'base64', 'angular-swagger', "ask-bootstrap"]) ;




//Route provider

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'partials/pages/main.html',
        controller: 'MainCtrl'
    })
    .when('/adduser', {
        templateUrl: 'partials/pages/user.html',
        controller: 'AddUserCtrl'
    })
     .when('/dialogue/:username', {
        templateUrl: 'partials/pages/dialogue.html',
        controller: 'DialogueCtrl'
    })
    .when('/login', {
        templateUrl: 'partials/pages/loginOrRegister.html', 
        controller: 'LoginCtrl'
    })
    .when('/register', {
        templateUrl: 'partials/pages/loginOrRegister.html', 
        controller: 'RegisterCtrl'
    })
    .otherwise({
        redirectTo: '/'}
        );
}]);


//Restangular

app.config(function(RestangularProvider) {

    //all api calls start with this prefix
    RestangularProvider.setBaseUrl('restapi/');

}) ;    