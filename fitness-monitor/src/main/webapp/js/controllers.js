app


.controller('MainCtrl', function($scope, $location, Auth, Restangular, Users) {
	
	$scope.users=[];
	
	if (Auth.hasCredentials()) {
		Restangular.one("users","me").get().then(
			function (data) {
				console.log(data) ;
				$scope.me = data ;
				$scope.users=Users.getUserList();
				console.log($scope.users.length);
			},
			function (error) {
				console.log(error) ;
				Auth.clearCredentials() ;
				$location.path("/login") ;
			}) ;
	} else {
		$location.path("/login") ;
	}
	
	$scope.logout = function() {
		Auth.clearCredentials() ;
		$location.path("/login") ;
	}

})

.controller('AddUserCtrl', function($scope, $location, Auth, Restangular, Users) {
	
	$scope.user={};
	$scope.alerts = [];
	$scope.fitbitBtn='Connect';

	$scope.saveUser = function(){
		var success=Users.saveUser($scope.user);
		if(!success)
			$location.path("./#") ;
		else
			$scope.alerts.push({type: 'danger', msg : 'Error Adding User to Server!'});
	};
	
	$scope.connectFitbit = function() {
		OAuth.initialize('GCViN70Wi9J9Sf0dJ7Th2E32VmA');
		
		var promise = OAuth.popup('fitbit');

		promise.done(function (result) {
		    // make API calls
			console.log(result);
			$scope.alerts.push({type: 'success', msg : 'Succesfully Connected to Fitbit!'});
			$scope.fitbitBtn=null;
			$scope.fitbitBtn='Connected';
			
			$scope.user.fitbit_tokenkey=angular.copy(result.oauth_token);
			$scope.user.fitbit_tokensecret=angular.copy(result.oauth_token_secret);
			
		});

		promise.fail(function (error) {
		    // handle errors
			console.log('OAuth Failed');
			$scope.alerts.push({type: 'danger', msg : 'Error authorizeing Fitbit!'});
		});
		
	};
	
	$scope.closeAlert = function(index) {
		$scope.alerts.splice(index, 1);
	};
})

.controller('DialogueCtrl', function($scope, $location, Auth, Restangular, Users, $http, $routeParams) {
	
	$scope.users=[];
	
	var surveyFile = "schema_test.json" ;
	$scope.schema1={};
	$scope.schema2={};
	Restangular.one("Dialogue1?username="+$routeParams.username).get().then(
			function (data) {
				$scope.schema1=data;
			},
			function (error) {
				console.log(error) ;
			}
		) ;
	
	Restangular.one("Dialogue2?username="+$routeParams.username).get().then(
			function (data) {
				$scope.schema2=data;
			},
			function (error) {
				console.log(error) ;
			}
		) ;

		  /*	$http.get(surveyFile)
		    .then(
		      function (response) {

		        console.log(response.data) ;
		        $scope.schema = response.data ;
		      },
		      function (error) {
		        console.log(error) ;
		      }
		    ) ;*/
	
})



.controller('LoginCtrl', function($scope, $location, Auth, Restangular) {

	$scope.mode = 'login' ;



	$scope.login = function() {

		$scope.error = null ;

		if (!$scope.screenName) {
			$scope.error = {message:"You must specify a screen name"} ;
			return ;
		}

		if (!$scope.password) {
			$scope.error = {message:"You must specify a password"} ;
			return ;
		}

		Auth.setCredentials($scope.screenName, $scope.password) ;

		Restangular.one("users","me").get().then(
			function (data) {
				$location.path("/") ;
			},
			function (error) {
				console.log(error) ;
				$scope.error = error ;
			}
		) ;
	}
})


.controller('RegisterCtrl', function($scope, $location, Auth, Restangular) {

	$scope.mode = 'register' ;

	$scope.register = function() {

		$scope.error = null ;


		if (!$scope.screenName) {
			$scope.error = {message:"You must specify a screen name"} ;
			return ;
		}

		if (!$scope.email) {
			$scope.error = {message:"You must specify an email address"} ;
			return ;
		}

		if (!$scope.password) {
			$scope.error = {message:"You must specify a password"} ;
			return ;
		}

		if (!$scope.password2 || $scope.password != $scope.password2) {
			$scope.error = {message:"Passwords do not match"} ;
			return ;
		}

		$scope.unauthorized = false ;

		Restangular.all("users").post(
		{
			screenName:$scope.screenName,
			email:$scope.email, 
			password:$scope.password
		}
		).then(
			function (data) {
				$scope.me = data ;
				Auth.setCredentials($scope.screenName, $scope.password) ;
				$location.path("/") ;
			},
			function (error) {
				$scope.error = error ;
			}
		) ; 
	}
}) 









