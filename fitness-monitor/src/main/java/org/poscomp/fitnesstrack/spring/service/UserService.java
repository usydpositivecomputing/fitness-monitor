package org.poscomp.fitnesstrack.spring.service;
 
import java.util.List;

import org.poscomp.fitnesstrack.spring.model.User;
import org.poscomp.fitnesstrack.spring.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service("UserService")
@Transactional(readOnly = true)
public class UserService {
 
    @Autowired
    UserDAO UserDAO;

    @Transactional(readOnly = false)
    public void addUser(User user) {
        getUserDAO().addUser(user);
    }
    
    @Transactional(readOnly = false)
    public void deleteUser(User user) {
        getUserDAO().deleteUser(user);
    }
 
    @Transactional(readOnly = false)
    public void updateUser(User user) {
        getUserDAO().updateUser(user);
    }
    
    public List<User> getUsers() {
        return getUserDAO().getUsers();
    }
    
    public List<User> getActiveUsers() {
        return getUserDAO().getActiveUsers();
    }
    
    public List<User> getSmsUsers() {
        return getUserDAO().getSmsUsers();
    }
    
   /* public User getUserById(long id) {
        return getUserDAO().getUserById(id);
    }*/
    
   public User getUserByUserName(String username) {
        return getUserDAO().getUserById(username);
   }
    
  
    public UserDAO getUserDAO() {
        return UserDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.UserDAO = userDAO;
    }


}