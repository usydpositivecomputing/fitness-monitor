package org.poscomp.fitnesstrack.spring.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.poscomp.fitnesstrack.fitbit.model.Activities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="Activity")
//@IdClass(value=ActivityPK.class)
@JsonIgnoreProperties({ "user" })
public class Activity implements Serializable {
	
	@Id
	@GeneratedValue
	@Column(name="id", unique = true)
	private int id;
	
	@Type(type="date")
	@Column(name="date")
	Date date;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "username", nullable = false)
	User user;
	
	@Column(name="steps")
	int steps;
	
	@Column(name="distance")
	double distance;
	
	@Column(name="fairly_active_mins")
	int fairly_active_mins;
	
	@Column(name="lightly_active_mins")
	int lightly_active_mins;
	
	@Column(name="very_active_mins")
	int very_active_mins;
	
	@Column(name="sedentary_mins")
	int sedentary_mins;
//	sedentaryMinutes
	
	@Column(name="goal_steps")
	int goal_steps;
	
	@Column(name="goal_distance")
	double goal_distance;
	
	@Column(name="excercise_answer")
	String excercise_answer;
	
	@Column(name = "analysis_hasexcercised")
	boolean analysis_hasexcercised;
	
	@Column(name="steps_yesterday")
	int steps_yesterday;
	
	@Column(name="days_tired")
	int days_tired;
	
	@Column(name="days_sick")
	int days_sick;
	
	@Column(name="days_busy")
	int days_busy;

	@Column(name="days_streak")
	int days_streak;
	
	@Column(name="difficulty_yesterday")
	String difficulty_yesterday;
	
	@Column(name="difficulty_today")
	String difficulty_today;
	
		
	public Activity()
	{
		
	}
	
	// COPY Constructor
	public Activity(Date date,User user,Activities fitbitActivity)
	{
		this.date=date;
		this.user=user;
		this.steps=fitbitActivity.getSummary().getSteps();
		this.distance=fitbitActivity.getSummary().getTotalDistance();
		this.fairly_active_mins=fitbitActivity.getSummary().getFairlyActiveMinutes();
		this.very_active_mins=fitbitActivity.getSummary().getVeryActiveMinutes();
		this.lightly_active_mins=fitbitActivity.getSummary().getLightlyActiveMinutes();
		this.sedentary_mins=fitbitActivity.getSummary().getSedentaryMinutes();
		
				
		
		/*fairly_active_mins
		lightly_active_mins
		very_active_mins*/
	}
	
		public Date getDate() {
		return date;
	}

	public int getSteps() {
		return steps;
	}

	public double getDistance() {
		return distance;
	}

	public int getFairly_active_mins() {
		return fairly_active_mins;
	}

	public int getLightly_active_mins() {
		return lightly_active_mins;
	}

	public int getVery_active_mins() {
		return very_active_mins;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setSteps(int steps) {
		this.steps = steps;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public void setFairly_active_mins(int fairly_active_mins) {
		this.fairly_active_mins = fairly_active_mins;
	}

	public void setLightly_active_mins(int lightly_active_mins) {
		this.lightly_active_mins = lightly_active_mins;
	}

	public void setVery_active_mins(int very_active_mins) {
		this.very_active_mins = very_active_mins;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGoal_steps() {
		return goal_steps;
	}

	public double getGoal_distance() {
		return goal_distance;
	}



	public void setGoal_steps(int goal_steps) {
		this.goal_steps = goal_steps;
	}

	public void setGoal_distance(double goal_distance) {
		this.goal_distance = goal_distance;
	}

	
	public int getSedentary_mins() {
		return sedentary_mins;
	}

	public void setSedentary_mins(int sedentary_mins) {
		this.sedentary_mins = sedentary_mins;
	}

	public boolean isAnalysis_hasexcercised() {
		return analysis_hasexcercised;
	}

	public void setAnalysis_hasexcercised(boolean analysis_hasexcercised) {
		this.analysis_hasexcercised = analysis_hasexcercised;
	}

	public String getExcercise_answer() {
		return excercise_answer;
	}


	public int getSteps_yesterday() {
		return steps_yesterday;
	}

	public int getDays_tired() {
		return days_tired;
	}

	public int getDays_sick() {
		return days_sick;
	}

	public int getDays_busy() {
		return days_busy;
	}


	public void setExcercise_answer(String excercise_answer) {
		this.excercise_answer = excercise_answer;
	}

	public void setSteps_yesterday(int steps_yesterday) {
		this.steps_yesterday = steps_yesterday;
	}

	public void setDays_tired(int days_tired) {
		this.days_tired = days_tired;
	}

	public void setDays_sick(int days_sick) {
		this.days_sick = days_sick;
	}

	public void setDays_busy(int days_busy) {
		this.days_busy = days_busy;
	}

	public String getDifficulty_yesterday() {
		return difficulty_yesterday;
	}

	public String getDifficulty_today() {
		return difficulty_today;
	}

	public void setDifficulty_yesterday(String difficulty_yesterday) {
		this.difficulty_yesterday = difficulty_yesterday;
	}

	public void setDifficulty_today(String difficulty_today) {
		this.difficulty_today = difficulty_today;
	}

	public int getDays_streak() {
		return days_streak;
	}

	public void setDays_streak(int days_streak) {
		this.days_streak = days_streak;
	}

	
	
	

}
