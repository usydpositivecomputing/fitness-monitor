package org.poscomp.fitnesstrack.spring.model;

import java.io.Serializable;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalTime;
import org.poscomp.fitnesstrack.rest.model.UserRestModel;




@Entity
@Table(name="\"User\"")
public class User implements Serializable {

	@Id
	@Column(name="username", unique = true)
    private String username;
	
	@Column(name="password")
	private String password;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="Date_of_Bith")
	private Date dateOfBith;
	
	// Default Value
	@Column(name="Gender")
	private String gender="Male";
	
	@Column(name="Email_address")
	private String emailAddress;
	
	@Column(name="Mobile_number")
	private String mobileNnumber;
	
	@Column(name="created_on")
	private Date created_on;

	@Column(name="status")
	private boolean status;
	
	@Column(name="fitbit_token")
	private String fitbit_tokenkey;
	
	@Column(name="fitbit_tokensecret")
	private String fitbit_tokensecret;
	
	@Column(name="last_synch")
	private Date last_synch;
	
	@Column(name="Country")
	private String country;
	
	@Column(name="Address")
	private String address;
	
	@Column(name="role_admin")
	private boolean roleAdmin;
	
	@Column(name="sms_service")
	private boolean smsService;
	
	@Column(name="initial_goal")
	int initial_goal;
	
	@Column(name="wakeup_time")
	private Time wakeup_time;
	
	@Column(name="adaptive_aproach")
	boolean adaptive_aproach;
	
	public User()
	{
		
	}
	
	public User(UserRestModel restUser)
	{
		username = restUser.getUsername();

		password = restUser.getPassword();

		firstName = restUser.getFirstName();

		lastName = restUser.getLastName();

		dateOfBith = restUser.getDateOfBith();

		gender = restUser.getGender();

		emailAddress = restUser.getEmailAddress();

		mobileNnumber = restUser.getMobileNnumber();

		/*
		 * DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); Date today
		 * = new Date(); Date todayWithZeroTime=null; try { todayWithZeroTime =
		 * formatter.parse(formatter.format(today)); } catch (ParseException e)
		 * { // TODO Auto-generated catch block e.printStackTrace(); }
		 */

		created_on = new Date();

		status = true;

		fitbit_tokenkey = restUser.getFitbit_tokenkey();

		fitbit_tokensecret = restUser.getFitbit_tokensecret();

		last_synch = null;

		country = restUser.getCountry();

		address = restUser.getAddress();
		
		roleAdmin= restUser.isRoleAdmin();
		
		smsService = restUser.isSmsService();
		
		adaptive_aproach= restUser.isAdaptive_aproach();
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Date getDateOfBith() {
		return dateOfBith;
	}

	public String getGender() {
		return gender;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getMobileNnumber() {
		return mobileNnumber;
	}

	public String getCountry() {
		return country;
	}

	public String getAddress() {
		return address;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setDateOfBith(Date dateOfBith) {
		this.dateOfBith = dateOfBith;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setMobileNnumber(String mobileNnumber) {
		this.mobileNnumber = mobileNnumber;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	public Date getCreated_on() {
		return created_on;
	}

	public boolean isStatus() {
		return status;
	}

	public String getFitbit_tokenkey() {
		return fitbit_tokenkey;
	}

	public String getFitbit_tokensecret() {
		return fitbit_tokensecret;
	}

	public Date getLast_synch() {
		return last_synch;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public boolean isRoleAdmin() {
		return roleAdmin;
	}

	public void setRoleAdmin(boolean roleAdmin) {
		this.roleAdmin = roleAdmin;
	}

	public int getInitial_goal() {
		return initial_goal;
	}

	public Time getWakeup_time() {
		return wakeup_time;
	}

	public boolean isAdaptive_aproach() {
		return adaptive_aproach;
	}

	public void setInitial_goal(int initial_goal) {
		this.initial_goal = initial_goal;
	}

	public void setWakeup_time(Time wakeup_time) {
		this.wakeup_time = wakeup_time;
	}

	public void setAdaptive_aproach(boolean adaptive_aproach) {
		this.adaptive_aproach = adaptive_aproach;
	}

	public boolean isSmsService() {
		return smsService;
	}

	public void setSmsService(boolean smsService) {
		this.smsService = smsService;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public void setFitbit_tokenkey(String fitbit_tokenkey) {
		this.fitbit_tokenkey = fitbit_tokenkey;
	}

	public void setFitbit_tokensecret(String fitbit_tokensecret) {
		this.fitbit_tokensecret = fitbit_tokensecret;
	}

	public void setLast_synch(Date last_synch) {
		this.last_synch = last_synch;
	}
	
		
}