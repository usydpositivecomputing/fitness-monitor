package org.poscomp.fitnesstrack.spring.dao;
 
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.poscomp.fitnesstrack.spring.model.Activity;
import org.poscomp.fitnesstrack.spring.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class UserDAO  {
    @Autowired
    private SessionFactory sessionFactory;
 
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 
    public void addUser(User user) {
        getSessionFactory().getCurrentSession().save(user);
    }
   
    public void deleteUser(User user) {
        getSessionFactory().getCurrentSession().delete(user);
    }
    
    public void updateUser(User user) {
      getSessionFactory().getCurrentSession().update(user);
    }
   
    public List<User> getUsers() {
        return getSessionFactory().getCurrentSession().createQuery("from User").list();
    }
    
    public User getUserById(String username) {
        return (User) getSessionFactory().getCurrentSession().get(User.class, username);
    }
    
    public List<User> getActiveUsers() {
    	//return getSessionFactory().getCurrentSession().createQuery("from Activity where").list();
    	Query query = getSessionFactory().getCurrentSession().createQuery("from User where status = :status");
    	query.setParameter("status", true);
    	List<User> activeUsers=  query.list();
    	return activeUsers;
    }
    
    public List<User> getSmsUsers() {
    	//return getSessionFactory().getCurrentSession().createQuery("from Activity where").list();
    	Query query = getSessionFactory().getCurrentSession().createQuery("from User where status = :status and smsService = :smsService");
    	query.setParameter("status", true);
    	query.setParameter("smsService", true);
    	List<User> activeUsers=  query.list();
    	return activeUsers;
    }
    
 
}