package org.poscomp.fitnesstrack.spring.service;
 
import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import org.joda.time.LocalDate;
import org.poscomp.fitnesstrack.beans.DataCollect;
import org.poscomp.fitnesstrack.spring.model.Activity;
import org.poscomp.fitnesstrack.spring.model.User;
import org.poscomp.fitnesstrack.spring.dao.ActivityDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service("ActivityService")
@Transactional(readOnly = true)
@PropertySource("classpath:deployment.properties")
public class ActivityService {
	
	@Autowired
	private Environment env;
 
    @Autowired
    ActivityDAO activityDAO;
    
    private static Logger logger = LoggerFactory.getLogger(ActivityService.class) ;

    @Transactional(readOnly = false)
    public void addActivity(Activity activity) {
    	activityDAO.addActivity(activity);
    }
    
    @Transactional(readOnly = false)
    public void deleteActivity(Activity activity) {
    	activityDAO.deleteActivity(activity);
    }
 
    @Transactional(readOnly = false)
    public void updateActivity(Activity activity) {
    	activityDAO.updateActivity(activity);
    }
    
    // Insert if unique or update if exists
    @Transactional(readOnly = false)
    public void overwriteFitbitData(Activity activity) {
    	//Activity temp=activityDAO.getActivity(activity.getUser().getUsername(), activity.getDate());
    	Activity temp=prepareNewRow(activity.getUser(),new LocalDate(activity.getDate()));
    	if(temp==null)
    		activityDAO.addActivity(activity);
    	else
    	{
    		temp.setDistance(activity.getDistance());
    		temp.setSteps(activity.getSteps());
    		temp.setFairly_active_mins(activity.getFairly_active_mins());
    		temp.setLightly_active_mins(activity.getLightly_active_mins());
    		temp.setVery_active_mins(activity.getVery_active_mins());
    		temp.setSedentary_mins(activity.getSedentary_mins());
    		if(detectExcercise(temp))
    		{
    			temp.setAnalysis_hasexcercised(true);
    			logger.info("User: "+temp.getUser().getUsername()+ "has EXCERCISED today. Steps taken =" +temp.getSteps());
    		}
    		activityDAO.updateActivity(temp);
    	}
    }
    
    @Transactional(readOnly = false)
    public Activity prepareNewRow(User user,LocalDate date) {
    	Activity temp=activityDAO.getActivity(user.getUsername(), date.toDate());
    	
    	Activity newRow=null;
    	if(temp==null)
    	{
    		logger.info("New Activity Row created for user: "+user.getUsername()+" for date:" +date.toString());
    		List<Activity> activityList = activityDAO.getLastWeekActivity(user.getUsername());
    		newRow = getActivityHistory(user, date, activityList);
    		activityDAO.addActivity(newRow);
    		return newRow;
    	}
    	return temp;
    }
    
    private Activity getActivityHistory(User user, LocalDate today, List<Activity> list)
    {
    	Activity activity=new Activity();
    	activity.setUser(user);
    	activity.setDate(today.toDate());
    	
    	
    	int goal_steps=0;
    	//String excercise_answer;
    	int steps_yesterday=0;
    	int days_tired=0;
    	int days_sick=0;   	
    	int days_busy=0;    	
    	int days_streak=0;
    	String difficulty_yesterday=null;
    	boolean lastday_flag=true;
    	boolean streak_broken=false;
    	
    	for(Activity row : list)
    	{
    		// Copy fields from last day
    		if(lastday_flag)
    		{
    			steps_yesterday = row.getSteps();
    			difficulty_yesterday = (row.getDifficulty_today()==null) ? "NoExcercise" : row.getDifficulty_today();
    			goal_steps = row.getGoal_steps();    	
    			
    			if(!row.isAnalysis_hasexcercised())
    			{
    				streak_broken=true;
    				days_streak=0;
    			}
    			else
    			{
    				days_streak++;
    			}
    			lastday_flag=false;
    		}
    		else
    		{
    			if(!streak_broken)
        		{
        			if(!row.isAnalysis_hasexcercised())
        			{
        				streak_broken=true;
        			}
        			else
        			{
        				days_streak++;
        			}	
        		}
    		}
    		
    		if(row.getExcercise_answer()!=null)
    		{
    			switch(row.getExcercise_answer())
    			{
    			case "Tired":
    				days_tired++;
    				break;
    			/*case "Lazy":
    				days_tired++;
    				break;*/
    			case "Sick":
    				days_sick++;
    				break;
    			case "Busy":
    				days_busy++;
    				break;
    			}
    		}
    		
    		

    	}// end loop
    	
    	// If no goal zero, set it to INITIAL Goal
    	if(goal_steps<=0)
    		goal_steps=Integer.parseInt(env.getProperty("excercise.initialgoal"));
    	
   		activity.setGoal_steps(goal_steps);

    	activity.setSteps_yesterday(steps_yesterday);
    	
    	activity.setDays_tired(days_tired);
    	
    	activity.setDays_sick(days_sick);
    	
    	activity.setDays_busy(days_busy);
    	
    	activity.setDays_streak(days_streak);
    	
    	activity.setDifficulty_yesterday(difficulty_yesterday);
    	
    	return activity;
    }
    
    private boolean detectExcercise(Activity activity)
	{
		int active_mins=Integer.parseInt(env.getProperty("excercise.veryactiveminutes"));
		double percentExcercise=Double.parseDouble(env.getProperty("excercise.percentgoal"));
		
		int intitial_goal=Integer.parseInt(env.getProperty("excercise.initialgoal"));
		
		if(activity.getVery_active_mins()>=active_mins)
		{
			return true;
		}
		
		if(activity.getGoal_steps()==0)
		{
			logger.warn("Goal Steps ZERO for user: "+activity.getUser().getUsername());
			activity.setGoal_steps(intitial_goal);
		}
		
		if(activity.getGoal_steps()>0)
		{
			double excerciseAmount=((double) activity.getSteps()) / ((double) activity.getGoal_steps());
			if(excerciseAmount>=percentExcercise)
			{
				return true;
			}
 		}
		return false;
		
	}
    
    public List<Activity> getActivities() {
        return activityDAO.getActivities();
    }
    
   /* public Activity getActivityById(long id) {
        return getActivityDAO().getActivityById(id);
    }*/
    
   public Activity getActivity(String username, Date date) {
       return activityDAO.getActivity(username, date);
   }
       
 
}