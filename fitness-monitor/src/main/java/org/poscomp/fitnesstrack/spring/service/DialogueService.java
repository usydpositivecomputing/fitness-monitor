package org.poscomp.fitnesstrack.spring.service;
 
import java.util.Date;
import java.util.List;

import org.poscomp.fitnesstrack.spring.dao.DialogueDAO;
import org.poscomp.fitnesstrack.spring.model.Dialogue;
import org.poscomp.fitnesstrack.spring.model.Questions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service("DialogueService")
@Transactional(readOnly = true)
public class DialogueService {
 
    @Autowired
    DialogueDAO dialogueDAO;

    @Transactional(readOnly = false)
    public void addDialogue(Dialogue dialogue) {
    	dialogueDAO.addDialogue(dialogue);
    }
     
    @Transactional(readOnly = false)
    public void updateDialogue(Dialogue dialogue) {
    	dialogueDAO.updateDialogue(dialogue);
    }
    
    // Insert if unique or update if exists
    @Transactional(readOnly = false)
    public void overwrite(Dialogue dialogue) {
    	/*Dialogue temp=dialogueDAO.getDialogue(dialogue.getUser().getUsername(), dialogue.getDate());
    	if(temp==null)
    		dialogueDAO.addDialogue(dialogue);
    	else
    	{
    		dialogue.setId(temp.getId());
    		dialogueDAO.updateDialogue(dialogue);
    	}*/
    	dialogueDAO.overwrite(dialogue);
    }
    public List<Questions> getQuestionList()
    {
    	return dialogueDAO.getQuestions();
    }
    
    
   /* public Dialogue getDialogueById(long id) {
        return getDialogueDAO().getDialogueById(id);
    }*/
    
   public Dialogue getDialogue(String username, Date date) {
       return dialogueDAO.getDialogue(username, date);
   }
       
 
}