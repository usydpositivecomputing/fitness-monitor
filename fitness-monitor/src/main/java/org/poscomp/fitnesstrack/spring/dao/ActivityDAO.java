package org.poscomp.fitnesstrack.spring.dao;
 
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.joda.time.LocalDate;
import org.poscomp.fitnesstrack.spring.model.Activity;
import org.poscomp.fitnesstrack.spring.model.ActivityPK;
import org.poscomp.fitnesstrack.spring.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class ActivityDAO  {
    @Autowired
    private SessionFactory sessionFactory;
 
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 
    public Activity addActivity(Activity activity) {
        getSessionFactory().getCurrentSession().save(activity);
        return activity;
    }
   
    public void deleteActivity(Activity activity) {
        getSessionFactory().getCurrentSession().delete(activity);
    }
    
    public void updateActivity(Activity activity) {
      getSessionFactory().getCurrentSession().update(activity);
    }
   
    public List<Activity> getActivities() {
        return getSessionFactory().getCurrentSession().createQuery("from Activity").list();
    }
    
   public Activity getActivity(String username,Date date) {
    	//return getSessionFactory().getCurrentSession().createQuery("from Activity where").list();
    	Query query = getSessionFactory().getCurrentSession().createQuery("from Activity where user.username = :username and date = :date");
    	query.setParameter("username", username);
    	query.setParameter("date", date);
    	Activity activity= (Activity) query.uniqueResult();
    	return activity;
    }
   
   public List<Activity> getLastWeekActivity(String username) {
	LocalDate today=new LocalDate();
	LocalDate lastWeek=new LocalDate().minusDays(7);
	
   	//return getSessionFactory().getCurrentSession().createQuery("from Activity where").list();
   	Query query = getSessionFactory().getCurrentSession().createQuery("from Activity where user.username = :username and date < :today and date >= :lastWeek order by date desc");
   	query.setParameter("username", username);
   	query.setParameter("today", today.toDate());
   	query.setParameter("lastWeek", lastWeek.toDate());
   	List<Activity> activityList= query.list();
   	return activityList;
   }
    
   /* public Activity getActivity(User user,Date date) 
    {
    	ActivityPK activityPK=new ActivityPK(date,user);
    	Activity activity=(Activity) getSessionFactory().getCurrentSession().get(Activity.class, activityPK);
    	return activity;
    }*/
 
}