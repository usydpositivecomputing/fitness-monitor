package org.poscomp.fitnesstrack.spring.dao;
 
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.poscomp.fitnesstrack.spring.model.Dialogue;
import org.poscomp.fitnesstrack.spring.model.Questions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;




@Repository
public class DialogueDAO  {
    @Autowired
    private SessionFactory sessionFactory;
 
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 
    public void addDialogue(Dialogue dialogue) {
        getSessionFactory().getCurrentSession().save(dialogue);
    }
       
    public void updateDialogue(Dialogue dialogue) {
      getSessionFactory().getCurrentSession().update(dialogue);
    }
   
    public List<Dialogue> getDialogue() {
        return getSessionFactory().getCurrentSession().createQuery("from Dialogue").list();
    }
    
   public Dialogue getDialogue(String username,Date date) {
    	Query query = getSessionFactory().getCurrentSession().createQuery("from Dialogue where user.username = :username and date = :date");
    	query.setParameter("username", username);
    	query.setParameter("date", date);
    	Dialogue dialogue= (Dialogue) query.uniqueResult();
    /*	if(dialogue!=null)
    		Hibernate.initialize(dialogue.getQnaList());*/
    	return dialogue;
    }
   
   public List<Questions> getQuestions() {
       List<Questions> list = getSessionFactory().getCurrentSession().createQuery("from Questions").list();
       return list;
   }
   
   public void overwrite(Dialogue dialogue)
   {
	  Dialogue temp=getDialogue(dialogue.getUser().getUsername(), dialogue.getDate());
   		if(temp==null)
   		addDialogue(dialogue);
   		else
   		{
   			dialogue.setId(temp.getId());
   			updateDialogue(dialogue);
   		}
   }
    
   /* public Activity getActivity(User user,Date date) 
    {
    	ActivityPK activityPK=new ActivityPK(date,user);
    	Activity activity=(Activity) getSessionFactory().getCurrentSession().get(Activity.class, activityPK);
    	return activity;
    }*/
 
}