package org.poscomp.fitnesstrack.spring.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name="QnA")
public class QnA implements Serializable
{
	@Id
	@GeneratedValue
	@Column(name="qna_id", unique = true)
	private long qna_id;
		
	@ManyToOne
	@JoinColumn(name = "qid", nullable = false)
	Questions field;
	
	@Column(name="answer")
	String answer;
	
	public long getQna_id() {
		return qna_id;
	}

	public Questions getField() {
		return field;
	}

	public String getAnswer() {
		return answer;
	}

	public void setQna_id(long qna_id) {
		this.qna_id = qna_id;
	}

	public void setField(Questions field) {
		this.field = field;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	
}
