package org.poscomp.fitnesstrack.spring.model;

import java.io.Serializable;
import java.util.Date;

import org.joda.time.LocalDate;



public class ActivityPK implements Serializable 
{

	Date date;
	
	User user;
	
	public ActivityPK()
	{
		
	}
	
	public ActivityPK(Date date,User user)
	{
		this.date=date;
		this.user=user;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ActivityPK) {
			ActivityPK activityPk = (ActivityPK) obj;
			LocalDate date1 = new LocalDate(this.date);
			LocalDate date2 = new LocalDate(activityPk);

			if (!date1.equals(date2)) {
				return false;
			}

			if (!activityPk.getUser().getUsername().equals(this.user.getUsername())) {
				return false;
			}

			return true;
		}

		return false;
	}

	@Override
    public int hashCode() {
        return this.hashCode() + this.user.hashCode();
    }

	public Date getDate() {
		return date;
	}

	public User getUser() {
		return user;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
