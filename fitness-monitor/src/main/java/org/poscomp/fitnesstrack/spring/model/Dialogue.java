package org.poscomp.fitnesstrack.spring.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;


@Entity
@Table(name="Dialogue")
public class Dialogue implements Serializable
{
	@Id
	@GeneratedValue
	@Column(name="id", unique = true)
	private int id;
	
	@Type(type="date")
	@Column(name="date")
	Date date;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "username", nullable = false)
	User user;
	
	/*@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "Dialogue_QnA", joinColumns = {
        @JoinColumn(name = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "qna_id")})
	private List<QnA> qnaList;*/
	
	@Type( type = "org.hibernate.type.TextType" )
	@Column(name = "survey_current")
	String survey_current;
	
	@Type( type = "org.hibernate.type.TextType" )
	@Column(name = "survey_morning")
	String survey_morning;
	
	@Type( type = "org.hibernate.type.TextType" )
	@Column(name = "survey_alert")
	String survey_alert;
	
	@Type( type = "org.hibernate.type.TextType" )
	@Column(name = "survey_evening")
	String survey_evening;
	
	@Type( type = "org.hibernate.type.TextType" )
	@Column(name = "response_morning")
	String response_morning;
	
	@Type( type = "org.hibernate.type.TextType" )
	@Column(name = "response_evening")
	String response_evening;
	

	public int getId() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	public User getUser() {
		return user;
	}

	/*public List<QnA> getQnaList() {
		return qnaList;
	}*/

	public void setId(int id) {
		this.id = id;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getSurvey_current() {
		return survey_current;
	}

	public String getSurvey_morning() {
		return survey_morning;
	}

	public String getSurvey_alert() {
		return survey_alert;
	}

	public String getSurvey_evening() {
		return survey_evening;
	}

	public void setSurvey_current(String survey_current) {
		this.survey_current = survey_current;
	}

	public void setSurvey_morning(String survey_morning) {
		this.survey_morning = survey_morning;
	}

	public void setSurvey_alert(String survey_alert) {
		this.survey_alert = survey_alert;
	}

	public void setSurvey_evening(String survey_evening) {
		this.survey_evening = survey_evening;
	}

	public String getResponse_morning() {
		return response_morning;
	}

	public String getResponse_evening() {
		return response_evening;
	}

	public void setResponse_morning(String response_morning) {
		this.response_morning = response_morning;
	}

	public void setResponse_evening(String response_evening) {
		this.response_evening = response_evening;
	}

	

	/*public void setQnaList(List<QnA> qnaList) {
		this.qnaList = qnaList;
	}*/

		
}
