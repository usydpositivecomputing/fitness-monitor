package org.poscomp.fitnesstrack.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Questions")
public class Questions implements Serializable
{
	@Id
	@GeneratedValue
	@Column(name="qid", unique = true)
	int qid;
	
	@Column(name="is_question")
	boolean is_question;
	
	
	@Column(name="qname")
	String qname;
	
	@Column(name="text")
	String text;

	public int getQid() {
		return qid;
	}

	public boolean isIs_question() {
		return is_question;
	}

	public String getQname() {
		return qname;
	}

	public String getText() {
		return text;
	}

	public void setQid(int qid) {
		this.qid = qid;
	}

	public void setIs_question(boolean is_question) {
		this.is_question = is_question;
	}

	public void setQname(String qname) {
		this.qname = qname;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
