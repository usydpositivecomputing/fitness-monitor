package org.poscomp.fitnesstrack.scheduler;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.LocalDate;
import org.poscomp.fitnesstrack.beans.DataCollect;
import org.poscomp.fitnesstrack.beans.SurveyGenerate;
import org.poscomp.fitnesstrack.controller.AuthenticationController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


@EnableScheduling
public class ScheduledTasks {

	
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    private static Logger logger = LoggerFactory.getLogger(ScheduledTasks.class) ;
    
    @Autowired
    DataCollect dataCollect; 
    
    @Autowired
    SurveyGenerate generateSurvey;
    
    
    @Scheduled(fixedDelay = 1800000)
    public void getFitbitData() {
    	try
    	{
    		logger.info("Data Collection Scheduler Start: " + dateFormat.format(new Date()));
    		dataCollect.synchFitbitData();
    		logger.info("Data Collection Scheduler End: " + dateFormat.format(new Date()));
    	}
    	catch(Exception e)
    	{
    		logger.error("Data Collection Scheduler Exception!!");
    	}
    }
    
   // @Scheduled(cron = "0 0/30 5-11 * * *")
 //   @Scheduled(cron = "0 0 7 * * *")
    public void generateMorningQuestions() 
    {
    	try
    	{
    		logger.info("Morning Scheduler Start: " + dateFormat.format(new Date()));
    		generateSurvey.generateMorningSurvey();
    		logger.info("Morning Scheduler End: " + dateFormat.format(new Date()));
    	}
    	catch(Exception e)
    	{
    		logger.error("Morning Scheduler Exception");
    	}
    }
    
 //   @Scheduled(cron = "0 0 21 * * *")
    public void generateEveningQuestions() 
    {
    	try
    	{
    		logger.info("Evening Scheduler Start: " + dateFormat.format(new Date()));
    		LocalDate date=new LocalDate();
    		generateSurvey.generateEveningSurvey();
    		logger.info("Evening Scheduler End: " + dateFormat.format(new Date()));
    	}
    	catch(Exception e)
    	{
    		logger.error("Evening Scheduler Exception");
    	}
    }
}