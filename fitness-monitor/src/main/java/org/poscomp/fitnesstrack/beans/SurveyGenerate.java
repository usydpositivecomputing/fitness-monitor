package org.poscomp.fitnesstrack.beans;

import static org.poscomp.ask.model.logic.Actions.showField;
import static org.poscomp.ask.model.logic.Triggers.*;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.error.InvalidRule;
import org.poscomp.ask.error.InvalidSurvey;
import org.poscomp.ask.model.Response;
import org.poscomp.ask.model.Schema;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.answers.NumericAnswer;
import org.poscomp.ask.model.answers.SinglechoiceAnswer;
import org.poscomp.ask.model.fields.Instruction;
import org.poscomp.ask.model.fields.NumericQuestion;
import org.poscomp.ask.model.fields.SinglechoiceQuestion;
import org.poscomp.ask.model.logic.FieldRule;
import org.poscomp.ask.model.logic.Reminder;
import org.poscomp.fitnesstrack.controller.HomeController;
import org.poscomp.fitnesstrack.spring.model.Activity;
import org.poscomp.fitnesstrack.spring.model.Dialogue;
import org.poscomp.fitnesstrack.spring.model.User;
import org.poscomp.fitnesstrack.spring.service.ActivityService;
import org.poscomp.fitnesstrack.spring.service.DialogueService;
import org.poscomp.fitnesstrack.spring.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
@Scope(value = "singleton")
@PropertySource("classpath:deployment.properties")
public class SurveyGenerate implements InitializingBean {

	@Resource(name = "UserService")
	UserService userService;

	@Resource(name = "ActivityService")
	ActivityService activityService;

	@Resource(name = "DialogueService")
	DialogueService dialogueService;
	
	@Autowired
	private Environment env;
	
	private static final Logger logger = LoggerFactory.getLogger(SurveyGenerate.class);

	public void generateMorningSurvey() throws Exception
	{
		//Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss Z").create();
		LocalDate date = new LocalDate();
		LocalTime time = new LocalTime();
		
		// get Active Users
		List<User> users = userService.getActiveUsers();
		
		for (User user : users) {
			 
			Schema schema = null;
			try
			{
				if(user.isAdaptive_aproach())
					schema= getMorningSchema(user);
				else
					schema= getMorningSchemaPrescriptive(user);
			// convert java object to JSON format,
			ObjectMapper mapper =new ObjectMapper();
			String jsonResponse=mapper.writeValueAsString(schema);

			logger.info("Schema = "+jsonResponse);
			Dialogue dlg = dialogueService.getDialogue(user.getUsername(), date.toDate());
		
			//Save survey json in Dialogue Table
			if (dlg == null) {
				Dialogue newDlg = new Dialogue();
				newDlg.setDate(date.toDate());
				newDlg.setUser(user);
				newDlg.setSurvey_current(jsonResponse);
				newDlg.setSurvey_morning(jsonResponse);
				dialogueService.addDialogue(newDlg);
			} else {
				dlg.setSurvey_current(jsonResponse);
				dlg.setSurvey_morning(jsonResponse);
				dialogueService.updateDialogue(dlg);
			}
			}
			catch(Exception e)
			{
				logger.error("Error Generating Morning Survey for user: "+user.getUsername());
			}
			// dialogueService.overwrite(dlg);		
		}
	}
	
	public void generateEveningSurvey()
	{
		LocalDate date=new LocalDate();
		//Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss Z").create();
		
		List<User> users = userService.getActiveUsers();
		
		for (User user : users) 
		{
			Schema schema =null;
			try{
				if(user.isAdaptive_aproach())
					schema = getEveningSurvey(user,date);
				else
					schema = getEveningSurveyPrescriptive(user,date);
			
			// convert java object to JSON format,
			// and returned as JSON formatted string
			//String json = gson.toJson(survey);
			// convert java object to JSON format,
			ObjectMapper mapper =new ObjectMapper();
			String json=mapper.writeValueAsString(schema);

			System.out.println(json);

			Dialogue dlg = dialogueService.getDialogue(user.getUsername(), date.toDate());
			
			if (dlg == null) {
				Dialogue newDlg = new Dialogue();
				newDlg.setDate(date.toDate());
				newDlg.setUser(user);
				newDlg.setSurvey_current(json);
				newDlg.setSurvey_evening(json);
				dialogueService.addDialogue(newDlg);
			} else {
				dlg.setSurvey_current(json);
				dlg.setSurvey_evening(json);
				dialogueService.updateDialogue(dlg);
			}
			}
			catch(Exception e){
				logger.error("Error generating Evening Survey for User: " +user.getUsername());
			}
		}
		
	}
	

	private Schema getEveningSurvey(User user,LocalDate date) throws Exception 
	{
		LocalDateTime dateTime=new LocalDateTime();
		
		Activity activity = activityService.getActivity(user.getUsername(), date.toDate());
		
		if(activity==null)
		{
			logger.error("Activity row not found for user: "+ user.getUsername() + "for date = " + date.toString());
			throw new Exception("Error Processing Response");
		}

		Schema schema=new Schema();
		Survey survey = null;
		try {
			Survey.Builder builder = new Survey.Builder("Evening survey");
			
			if (activity.getExcercise_answer() == null || activity.getExcercise_answer().equalsIgnoreCase("Yes")) {
				if(activity.getSteps()>activity.getGoal_steps())
				{
					builder.addField(new SinglechoiceQuestion.Builder()
						.setId("qFitness")
						.setQuestion("Nailed it! You achieved your goal for today. How was your excercise?")
						.setChoices("Easy", "Moderate", "Hard").build());
				}
				else
				{
					builder.addField(new SinglechoiceQuestion.Builder()
					.setId("qFitness")
					.setQuestion("How was your excercise today?")
					.setChoices("Easy", "Moderate", "Hard").build());
				}

			}
			else if(activity.getExcercise_answer().equalsIgnoreCase("Tired"))
			{
				builder.addField(
						new Instruction.Builder().setId("iSleepEarly")
						.setText("Try to sleep early tonight. ").build());
			}
			/*
			 * }
			 * 
			 * // less sleep if(activity.isAnalysis_didntSleep()) {
			 * 
			 * builder.addField( new Instruction.Builder() .setId("iSleepEarly")
			 * .setText("Please dont forget to sleep early last night.")
			 * .build() );
			 * 
			 * }
			 */

			survey = builder.build();
			// survey.setId("morning");
			survey.setUsername(user.getUsername());
			survey.setExpiry(dateTime.plusHours(6).toDate());
			survey.setNeedResponse(true);
			survey.setMobileNumber(user.getMobileNnumber());
			survey.setSurvey_id(10);
			
			schema.setSurvey(survey);
										
			 Response response = new Response(survey, "blah");
			 response.setSurvey_id(10);
			 
			 schema.setResponse(response);
		} catch (Exception e) {
			return null;
		}

		return schema;
	}
	
	private Schema getEveningSurveyPrescriptive(User user,LocalDate date) throws Exception 
	{
		LocalDateTime dateTime=new LocalDateTime();
		
		Activity activity = activityService.getActivity(user.getUsername(), date.toDate());
		
		if(activity==null)
		{
			logger.error("Activity row not found for user: "+ user.getUsername() + "for date = " + date.toString());
			throw new Exception("Error Processing Response");
		}

		Schema schema=new Schema();
		Survey survey = null;
		try {
			Survey.Builder builder = new Survey.Builder("Evening survey");
			
			if (activity.getExcercise_answer() == null || activity.getExcercise_answer().equalsIgnoreCase("Yes")) {
				if(activity.getSteps()>activity.getGoal_steps())
				{
					builder.addField(
							new Instruction.Builder().setId("iGoalAchieved")
							.setText("Nailed it! You achieved your goal for today.. ").build());
				}
				else
				{
					builder.addField(
							new Instruction.Builder().setId("iGoalNotAchieved")
							.setText("You are making good progress! Keep it up..").build());
				}

			}
			else if(activity.getExcercise_answer().equalsIgnoreCase("Tired"))
			{
				builder.addField(
						new Instruction.Builder().setId("iSleepEarly")
						.setText("Try to sleep early tonight. ").build());
			}
			/*
			 * }
			 * 
			 * // less sleep if(activity.isAnalysis_didntSleep()) {
			 * 
			 * builder.addField( new Instruction.Builder() .setId("iSleepEarly")
			 * .setText("Please dont forget to sleep early last night.")
			 * .build() );
			 * 
			 * }
			 */

			survey = builder.build();
			// survey.setId("morning");
			survey.setUsername(user.getUsername());
			survey.setExpiry(dateTime.plusHours(6).toDate());
			survey.setNeedResponse(true);
			survey.setMobileNumber(user.getMobileNnumber());
			survey.setSurvey_id(10);
			
			schema.setSurvey(survey);
										
			 Response response = new Response(survey, "blah");
			 response.setSurvey_id(10);
			 
			 schema.setResponse(response);
		} catch (Exception e) {
			return null;
		}

		return schema;
	}

	public void processResponse(String username, Response response) throws Exception{
		LocalDate date = new LocalDate();

		Activity activity = activityService.getActivity(username, date.toDate());
		if(activity==null)
		{
			System.out.println("Process Response: Activity Row not found");
			throw new Exception("Process Response: Activity Row not found");
		}
		
		//If Morning Response
		if(response.getSurvey_id()==1)
		{
			Answer<?> answer = response.getAnswer("qExerciseToday");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Yes")) {
				activity.setExcercise_answer("Yes");
			}
			
			answer = response.getAnswer("qTiredConfirm");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("No way")) {
				activity.setExcercise_answer("Tired");
			}
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Ok")) {
				activity.setExcercise_answer("Yes");
			}
			
			answer = response.getAnswer("qTiredConsistentConfirm");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("No way")) {
				activity.setExcercise_answer("Tired");
			}
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Ok")) {
				activity.setExcercise_answer("Yes");
			}
			
			answer = response.getAnswer("qBusyConfirm");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("No way")) {
				activity.setExcercise_answer("Busy");
			}
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Ok")) {
				activity.setExcercise_answer("Yes");
			}
			
			answer = response.getAnswer("qBusyConsistentConfirm");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("No way")) {
				activity.setExcercise_answer("Busy");
			}
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Ok")) {
				activity.setExcercise_answer("Yes");
			}
			
			answer = response.getAnswer("qNoExerciseReason");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Lazy")) {
				activity.setExcercise_answer("Lazy");
			}
			
			answer = response.getAnswer("qNoExerciseReason");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Sick")) {
				activity.setExcercise_answer("Sick");
			}
		}
		
		if(response.getSurvey_id()==10)
		{
			int goal_steps=activity.getGoal_steps();
			int steps_taken=activity.getSteps();
			int goal_adapt=Integer.parseInt(env.getProperty("excercise.adaptgoal"));
			
			Answer<?> answer = response.getAnswer("qFitness");
			if (answer != null) {
				
				switch(answer.toString().toLowerCase().trim())
				{
				case "hard":
				//	activity.setDifficulty_today("Hard");
					activity.setDifficulty_today(answer.toString());
					if(steps_taken<=goal_steps)
					{
						activity.setGoal_steps(goal_steps-goal_adapt);
					}
					break;
				case "moderate":
					activity.setDifficulty_today(answer.toString());
					break;
				case "easy":
					if(steps_taken>=goal_steps)
					{
						activity.setGoal_steps(goal_steps+goal_adapt);
					}
					activity.setDifficulty_today(answer.toString());
					break;
				}
			}
		}

		activityService.updateActivity(activity);
		
		
		Dialogue dlg= dialogueService.getDialogue(username,date.toDate());
		if (dlg!= null) 
		{
			/*Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss Z").create();
			// convert java object to JSON format,
			// and returned as JSON formatted string
			String json = gson.toJson(response);*/
			
			String jsonResponse=null;
			ObjectMapper mapper =new ObjectMapper();
			try {
				jsonResponse=mapper.writeValueAsString(response);
			} catch (Exception e) {
			// TODO Auto-generated catch block
				System.out.println("Error: POST Response Process failed");
				e.printStackTrace();
			}
		
			if(response.getSurvey_id()==1)
			{
				dlg.setResponse_morning(jsonResponse);
				dlg.setSurvey_current(null);
			}
			if(response.getSurvey_id()==10)
			{
				dlg.setResponse_evening(jsonResponse);
				dlg.setSurvey_current(null);
			}
				
			
			dialogueService.updateDialogue(dlg);
		}
		
	}
	
	public void processResponsePrescriptive(String username, Response response) throws Exception{
		LocalDate date = new LocalDate();

		Activity activity = activityService.getActivity(username, date.toDate());
		if(activity==null)
		{
			System.out.println("Process Response: Activity Row not found");
			throw new Exception("Process Response: Activity Row not found");
		}
		
		//If Morning Response
		if(response.getSurvey_id()==1)
		{
			Answer<?> answer = response.getAnswer("qExerciseToday");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Yes")) {
				activity.setExcercise_answer("Yes");
			}
			
			answer = response.getAnswer("qTiredConfirm");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("No way")) {
				activity.setExcercise_answer("Tired");
			}
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Ok")) {
				activity.setExcercise_answer("Yes");
			}
			
			answer = response.getAnswer("qTiredConsistentConfirm");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("No way")) {
				activity.setExcercise_answer("Tired");
			}
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Ok")) {
				activity.setExcercise_answer("Yes");
			}
			
			answer = response.getAnswer("qBusyConfirm");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("No way")) {
				activity.setExcercise_answer("Busy");
			}
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Ok")) {
				activity.setExcercise_answer("Yes");
			}
			
			answer = response.getAnswer("qBusyConsistentConfirm");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("No way")) {
				activity.setExcercise_answer("Busy");
			}
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Ok")) {
				activity.setExcercise_answer("Yes");
			}
			
			answer = response.getAnswer("qNoExerciseReason");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Lazy")) {
				activity.setExcercise_answer("Lazy");
			}
			
			answer = response.getAnswer("qNoExerciseReason");
			if (answer != null && answer.toString().trim().equalsIgnoreCase("Sick")) {
				activity.setExcercise_answer("Sick");
			}
		}
		
		if(response.getSurvey_id()==10)
		{
			
		}

		activityService.updateActivity(activity);
		
		
		Dialogue dlg= dialogueService.getDialogue(username,date.toDate());
		if (dlg!= null) 
		{
			/*Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss Z").create();
			// convert java object to JSON format,
			// and returned as JSON formatted string
			String json = gson.toJson(response);*/
			
			String jsonResponse=null;
			ObjectMapper mapper =new ObjectMapper();
			try {
				jsonResponse=mapper.writeValueAsString(response);
			} catch (Exception e) {
			// TODO Auto-generated catch block
				System.out.println("Error: POST Response Process failed");
				e.printStackTrace();
			}
		
			if(response.getSurvey_id()==1)
			{
				dlg.setResponse_morning(jsonResponse);
				dlg.setSurvey_current(null);
			}
			if(response.getSurvey_id()==10)
			{
				dlg.setResponse_evening(jsonResponse);
				dlg.setSurvey_current(null);
			}
				
			
			dialogueService.updateDialogue(dlg);
		}
		
	}

	
	private Survey getMorningSurvey(User user) throws InvalidField, InvalidRule, InvalidSurvey {
		Survey survey = null;
		Survey.Builder builder = new Survey.Builder("Morning survey");
		builder.addField(new SinglechoiceQuestion.Builder().setId("qfeeling")
				.setQuestion("Hi, do you feel like excercising today?")
				.setChoices("Yes", "No").build());
		builder.addField(new Instruction.Builder()
				.setId("iGood")
				.setText("Great, you did a good job yesterday, can you excecise a bit harder?")
				.build());
		builder.addField(new SinglechoiceQuestion.Builder().setId("qWhyNot")
				.setQuestion("Oh why not?").addChoice("Busy", "Im busy today")
				.addChoice("Tired", "Didnt sleep well last night")
				.addChoice("Bad Mood", "I'm not in agood mood")
				.addChoice("Stressed", "I'm stressed out").build());
		builder.addField(new Instruction.Builder().setId("iSleepWell")
				.setText("Okay please remember to sleep early tonight").build());
		builder.addField(new SinglechoiceQuestion.Builder()
				.setId("qExcercise")
				.setQuestion("Sorry to hear that but you know you'll feel a lot better if you go for a walk. I hope you change your mind?")
				.addChoice("Yes", "Okay I'll excercise today")
				.addChoice("No", "No, I'll take a break today").build());
		builder.addField(new Instruction.Builder().setId("iGiveup")
				.setText("Oh I hope you feel better tomorow").build());
		builder.addField(new Instruction.Builder().setId("iAgree")
				.setText("Great! Dont forget to excercise today").build());
		builder.addFieldRule(new FieldRule.Builder()
				.addTrigger(singlechoiceTrigger("qfeeling").is("Yes"))
				.addAction(showField("iGood")).build());
		builder.addFieldRule(new FieldRule.Builder()
				.addTrigger(singlechoiceTrigger("qfeeling").is("No"))
				.addAction(showField("qWhyNot")).build());
		builder.addFieldRule(new FieldRule.Builder()
				.addTrigger(singlechoiceTrigger("qWhyNot").is("Busy"))
				.addAction(showField("qExcercise")).build());
		builder.addFieldRule(new FieldRule.Builder()
				.addTrigger(singlechoiceTrigger("qWhyNot").is("Stressed"))
				.addAction(showField("qExcercise")).build());
		builder.addFieldRule(new FieldRule.Builder()
				.addTrigger(singlechoiceTrigger("qWhyNot").is("Bad Mood"))
				.addAction(showField("qExcercise")).build());
		builder.addFieldRule(new FieldRule.Builder()
				.addTrigger(singlechoiceTrigger("qWhyNot").is("Tired"))
				.addAction(showField("iSleepWell")).build());
		builder.addFieldRule(new FieldRule.Builder()
				.addTrigger(singlechoiceTrigger("qExcercise").is("Yes"))
				.addAction(showField("iAgree")).build());
		builder.addFieldRule(new FieldRule.Builder()
				.addTrigger(singlechoiceTrigger("qExcercise").is("No"))
				.addAction(showField("iGiveup")).build());
		survey = builder.build();
		survey.setCompletionMessage("Thank you for your time!");
		
		survey.setUsername(user.getUsername());
		LocalDateTime date=new LocalDateTime();
		survey.setExpiry(date.plusHours(7).toDate());
		survey.setNeedResponse(true);
		survey.setMobileNumber(user.getMobileNnumber());
		survey.setSurvey_id(1);
		
		// ADD Reminders
		List<Reminder> reminders=new ArrayList<Reminder>();
		Reminder rem=new Reminder("Please complete the survey!",date.toDate());
		reminders.add(rem); 
		survey.setReminders(reminders);
		
		return survey;
	}
	
	private Schema getMorningSchema(User user) throws InvalidRule, InvalidField, InvalidSurvey {


	        Survey survey= new Survey.Builder("FitCoach morning (adaptive)")
	                .addField(
	                        new NumericQuestion.Builder()
	                                .setId("qStreak")
	                                .setQuestion("How many days in a row have you exercised")
	                                .hide()
	                                .build()
	                )
	                .addField(
	                        new NumericQuestion.Builder()
	                                .setId("qDaysTired")
	                                .setQuestion("How many days in the last week have you been too tired?")
	                                .hide()
	                                .build()
	                )
	                .addField(
	                        new NumericQuestion.Builder()
	                                .setId("qSickStreak")
	                                .setQuestion("How many days in a row have you been too sick?")
	                                .hide()
	                                .build()
	                )
	                .addField(
	                        new NumericQuestion.Builder()
	                                .setId("qDaysBusy")
	                                .setQuestion("How many days in the last week have you been too busy?")
	                                .hide()
	                                .build()
	                )
	                .addField(
	                        new SinglechoiceQuestion.Builder()
	                                .setId("qGender")
	                                .setQuestion("What gender are you?")
	                                .setChoices("Male", "Female")
	                                .hide()
	                                .build()
	                )
	                .addField(
	                        new NumericQuestion.Builder()
	                                .setId("qStepsYesterday")
	                                .setQuestion("How many steps did you make yesterday?")
	                                .hide()
	                                .build()
	                )
	                .addField(
	                        new SinglechoiceQuestion.Builder()
	                                .setId("qDifficulty")
	                                .setQuestion("How difficult was your exercise yesterday?")
	                                .setChoices("Easy", "Moderate", "Hard", "NoExcercise")
	                                .hide()
	                                .build()
	                )
	                .addField(
	                        new NumericQuestion.Builder()
	                                .setId("qGoal")
	                                .setQuestion("What is your current goals(steps)?")
	                                .hide()
	                                .build()
	                )
	                
	                
	                
	                .addField(
	                        new Instruction.Builder()
	                                .setId("iConsistencyMale")
	                                .setText("Good morning, Mr. Consistent :)")
	                                .build()
	                ).addField(
	                new Instruction.Builder()
	                        .setId("iConsistencyFemale")
	                        .setText("Good morning, Miss Consistent :)")
	                        .build()
	        ).addField(
	                new SinglechoiceQuestion.Builder()
	                        .setId("qExerciseToday")
	                        .setQuestion("Do you feel like exercising today?")
	                        .setChoices("Yes","No")
	                        .build()
	        ).addField(
	                new SinglechoiceQuestion.Builder()
	                        .setId("qNoExerciseReason")
	                        .setQuestion("Oh no, how come?")
	                        .setChoices("Tired","Sick","Busy","Lazy")
	                        .build()
	        ).addField(
	                new SinglechoiceQuestion.Builder()
	                        .setId("qNoExerciseReasonWithStreak")
	                        .setQuestion("Oh no, you'll ruin your streak! How come?")
	                        .setChoices("Tired", "Sick", "Busy", "Lazy")
	                        .build()
	        ).addField(
	                new SinglechoiceQuestion.Builder()
	                        .setId("qTiredConfirm")
	                        .setQuestion("Some Light exercise will help you sleep better tonight. Are you sure you can't fit any in today?")
	                        .setChoices("No way", "Ok")
	                        .build()
	        ).addField(
	                new SinglechoiceQuestion.Builder()
	                        .setId("qTiredConsistentConfirm")
	                        .setQuestion("You've used that excuse a lot, sleepyhead. Come on, lets go for a walk today.")
	                        .setChoices("No way", "Ok")
	                        .build()
	        ).addField(
	                new Instruction.Builder()
	                        .setId("iTiredConfirmed")
	                        .setText("Ok, fine. Get to bed early tonight, ok? We're going for a walk tomorrow.")
	                        .build()
	        ).addField(
	                new Instruction.Builder()
	                        .setId("iSickConfirmed")
	                        .setText("Ok, you take it easy today. I'll check in with you tomorrow")
	                        .build()
	        ).addField(
	                new Instruction.Builder()
	                        .setId("iSickConsistentConfirmed")
	                        .setText("Sick [[qSickStreak]] days in a row? This is pretty serious! See a doctor or something, ok? I'll check in on you tomorrow")
	                        .build()
	        ).addField(
	                new SinglechoiceQuestion.Builder()
	                        .setId("qBusyConfirm")
	                        .setQuestion("You sure you can't fit in a walk, or take your work with you?")
	                        .setChoices("No way","Ok")
	                        .build()
	        ).addField(
	                new SinglechoiceQuestion.Builder()
	                        .setId("qBusyConsistentConfirm")
	                        .setQuestion("You've used that excuse [[qDaysBusy]] times in the last week. Sure you can't make time for me?")
	                        .setChoices("No way", "Ok")
	                        .build()
	        ).addField(
	                new Instruction.Builder()
	                        .setId("iBusyConfirmed")
	                        .setText("Ok, but set some time aside for me tomorrow. I'm feeling neglected =(")
	                        .build()
	        ).addField(
	                new Instruction.Builder()
	                        .setId("iLazy")
	                        .setText("Oh, come on. That's no excuse")
	                        .build()
	        ).addField(
	                new Instruction.Builder()
	                        .setId("iGoalEasy")
	                        .setText("All right! Yesterday we did [[qStepsYesterday]] steps, piece of cake. Lets aim higher today (and avoid the cake)")
	                        .build()
	        ).addField(
	                new Instruction.Builder()
	                        .setId("iGoalModerate")
	                        .setText("All right! Yesterday we did [[qStepsYesterday]] steps. A bit more today?")
	                        .build()
	        ).addField(
	                new Instruction.Builder()
	                        .setId("iGoalHard")
	                        .setText("All right! Yesterday we did [[qStepsYesterday]] steps. Try for that again, but no worries if we do a bit less")
	                        .build()
	         ).addField(
		                new Instruction.Builder()
                        .setId("iGoalNoExcercise")
                        .setText("All right! As you didn't excercise yesterday. Lets try to take [[qGoal]] steps today, but no worries if we do a bit less")
                        .build()
	         )




	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(numericTrigger("qStreak").gt(2))
	                                .addTrigger(singlechoiceTrigger("qGender").is("Male"))
	                                .withAndOperator()
	                                .addAction(showField("iConsistencyMale"))
	                                .build()
	                ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(numericTrigger("qStreak").gt(2))
	                        .addTrigger(singlechoiceTrigger("qGender").is("Female"))
	                        .withAndOperator()
	                        .addAction(showField("iConsistencyFemale"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qExerciseToday").is("No"))
	                        .addTrigger(numericTrigger("qStreak").lt(3))
	                        .withAndOperator()
	                        .addAction(showField("qNoExerciseReason"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qExerciseToday").is("No"))
	                        .addTrigger(numericTrigger("qStreak").gt(2))
	                        .withAndOperator()
	                        .addAction(showField("qNoExerciseReasonWithStreak"))
	                        .build()
	        )


	                //no, without streak

	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Tired"))
	                                .addTrigger(numericTrigger("qDaysTired").lt(3))
	                                .withAndOperator()
	                                .addAction(showField("qTiredConfirm"))
	                                .build()
	                ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Tired"))
	                        .addTrigger(numericTrigger("qDaysTired").gt(2))
	                        .withAndOperator()
	                        .addAction(showField("qTiredConsistentConfirm"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Sick"))
	                        .addTrigger(numericTrigger("qSickStreak").lt(2))
	                        .withAndOperator()
	                        .addAction(showField("iSickConfirmed"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Sick"))
	                        .addTrigger(numericTrigger("qSickStreak").gt(1))
	                        .withAndOperator()
	                        .addAction(showField("iSickConsistentConfirmed"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Busy"))
	                        .addTrigger(numericTrigger("qDaysBusy").lt(3))
	                        .withAndOperator()
	                        .addAction(showField("qBusyConfirm"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Busy"))
	                        .addTrigger(numericTrigger("qDaysBusy").gt(2))
	                        .withAndOperator()
	                        .addAction(showField("qBusyConsistentConfirm"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
	                        .addAction(showField("iLazy"))
	                        .build()
	        )


	                //no, with streak

	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Tired"))
	                                .addTrigger(numericTrigger("qDaysTired").lt(3))
	                                .withAndOperator()
	                                .addAction(showField("qTiredConfirm"))
	                                .build()
	                ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Tired"))
	                        .addTrigger(numericTrigger("qDaysTired").gt(2))
	                        .withAndOperator()
	                        .addAction(showField("qTiredConsistentConfirm"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Sick"))
	                        .addTrigger(numericTrigger("qSickStreak").lt(2))
	                        .withAndOperator()
	                        .addAction(showField("iSickConfirmed"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Sick"))
	                        .addTrigger(numericTrigger("qSickStreak").gt(1))
	                        .withAndOperator()
	                        .addAction(showField("iSickConsistentConfirmed"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Busy"))
	                        .addTrigger(numericTrigger("qDaysBusy").lt(3))
	                        .withAndOperator()
	                        .addAction(showField("qBusyConfirm"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Busy"))
	                        .addTrigger(numericTrigger("qDaysBusy").gt(2))
	                        .withAndOperator()
	                        .addAction(showField("qBusyConsistentConfirm"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
	                        .addAction(showField("iLazy"))
	                        .build()
	        )




	                //no confirmations

	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qTiredConfirm").is("No way"))
	                                .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("No way"))
	                                .withOrOperator()
	                                .addAction(showField("iTiredConfirmed"))
	                                .build()
	                ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qBusyConfirm").is("No way"))
	                        .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("No way"))
	                        .withOrOperator()
	                        .addAction(showField("iBusyConfirmed"))
	                        .build()
	        )



	                //yes, easy

	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalEasy"))
	                                .build()
	                ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
	                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
	                        .withAndOperator()
	                        .addAction(showField("iGoalEasy"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
	                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
	                        .withAndOperator()
	                        .addAction(showField("iGoalEasy"))
	                        .build()
	        )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalEasy"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalEasy"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalEasy"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalEasy"))
	                                .build()
	                )

	                        //yes, moderate

	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalModerate"))
	                                .build()
	                ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
	                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
	                        .withAndOperator()
	                        .addAction(showField("iGoalModerate"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
	                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
	                        .withAndOperator()
	                        .addAction(showField("iGoalModerate"))
	                        .build()
	        )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalModerate"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalModerate"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalModerate"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalModerate"))
	                                .build()
	                )

	                        //yes, hard

	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalHard"))
	                                .build()
	                ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
	                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
	                        .withAndOperator()
	                        .addAction(showField("iGoalHard"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
	                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
	                        .withAndOperator()
	                        .addAction(showField("iGoalHard"))
	                        .build()
	        )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalHard"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalHard"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalHard"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalHard"))
	                                .build()
	                )
	                
	                         //yes, NoExcercise Yesterday 

	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalNoExcercise"))
	                                .build()
	                ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
	                        .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
	                        .withAndOperator()
	                        .addAction(showField("iGoalNoExcercise"))
	                        .build()
	        ).addFieldRule(
	                new FieldRule.Builder()
	                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
	                        .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
	                        .withAndOperator()
	                        .addAction(showField("iGoalNoExcercise"))
	                        .build()
	        )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalNoExcercise"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalNoExcercise"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalNoExcercise"))
	                                .build()
	                )
	                .addFieldRule(
	                        new FieldRule.Builder()
	                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
	                                .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
	                                .withAndOperator()
	                                .addAction(showField("iGoalNoExcercise"))
	                                .build()
	                )
	                
	                .build() ;
	        
	
	        survey.setUsername(user.getUsername());
			LocalDateTime date=new LocalDateTime();
			survey.setExpiry(date.plusHours(7).toDate());
			survey.setNeedResponse(true);
			survey.setMobileNumber(user.getMobileNnumber());
			survey.setSurvey_id(1);
	        
			
			// Create response for hidden fields
	        
			//date.toDate();
			Activity activity= activityService.prepareNewRow(user, date.toLocalDate());
			
			
	        Response response = new Response(survey, "blah")
            .setAnswer("qStreak", NumericAnswer.withNumber(activity.getDays_streak()))
            .setAnswer("qDaysTired", NumericAnswer.withNumber(activity.getDays_tired()))
            .setAnswer("qSickStreak", NumericAnswer.withNumber(activity.getDays_sick()))
            .setAnswer("qDaysBusy", NumericAnswer.withNumber(activity.getDays_busy()))
            .setAnswer("qGender", SinglechoiceAnswer.withChoice(user.getGender()))
            .setAnswer("qStepsYesterday", NumericAnswer.withNumber(activity.getSteps_yesterday()))
            .setAnswer("qDifficulty", SinglechoiceAnswer.withChoice(activity.getDifficulty_yesterday()))
            .setAnswer("qGoal", NumericAnswer.withNumber(activity.getGoal_steps()));
	        response.setSurvey_id(1);
	       
	        
	        Schema schema = new Schema();
	        schema.setSurvey(survey);
	        schema.setResponse(response);
	        return schema;

	    }
	
	private Schema getMorningSchemaPrescriptive(User user) throws InvalidRule, InvalidField, InvalidSurvey {


        Survey survey= new Survey.Builder("FitCoach morning (Prescriptive)")
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qStreak")
                                .setQuestion("How many days in a row have you exercised")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qDaysTired")
                                .setQuestion("How many days in the last week have you been too tired?")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qSickStreak")
                                .setQuestion("How many days in a row have you been too sick?")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qDaysBusy")
                                .setQuestion("How many days in the last week have you been too busy?")
                                .hide()
                                .build()
                )
                .addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qGender")
                                .setQuestion("What gender are you?")
                                .setChoices("Male", "Female")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qStepsYesterday")
                                .setQuestion("How many steps did you make yesterday?")
                                .hide()
                                .build()
                )
                
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qGoal")
                                .setQuestion("What is your current goals(steps)?")
                                .hide()
                                .build()
                )
                
                
                
                .addField(
                        new Instruction.Builder()
                                .setId("iConsistencyMale")
                                .setText("Good morning, Mr. Consistent :)")
                                .build()
                ).addField(
                new Instruction.Builder()
                        .setId("iConsistencyFemale")
                        .setText("Good morning, Miss Consistent :)")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qExerciseToday")
                        .setQuestion("Do you feel like exercising today?")
                        .setChoices("Yes","No")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qNoExerciseReason")
                        .setQuestion("Oh no, how come?")
                        .setChoices("Tired","Sick","Busy","Lazy")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qNoExerciseReasonWithStreak")
                        .setQuestion("Oh no, you'll ruin your streak! How come?")
                        .setChoices("Tired", "Sick", "Busy", "Lazy")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qTiredConfirm")
                        .setQuestion("Some Light exercise will help you sleep better tonight. Are you sure you can't fit any in today?")
                        .setChoices("No way", "Ok")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qTiredConsistentConfirm")
                        .setQuestion("You've used that excuse a lot, sleepyhead. Come on, lets go for a walk today.")
                        .setChoices("No way", "Ok")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iTiredConfirmed")
                        .setText("Ok, fine. Get to bed early tonight, ok? We're going for a walk tomorrow.")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iSickConfirmed")
                        .setText("Ok, you take it easy today. I'll check in with you tomorrow")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iSickConsistentConfirmed")
                        .setText("Sick [[qSickStreak]] days in a row? This is pretty serious! See a doctor or something, ok? I'll check in on you tomorrow")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qBusyConfirm")
                        .setQuestion("You sure you can't fit in a walk, or take your work with you?")
                        .setChoices("No way","Ok")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qBusyConsistentConfirm")
                        .setQuestion("You've used that excuse [[qDaysBusy]] times in the last week. Sure you can't make time for me?")
                        .setChoices("No way", "Ok")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iBusyConfirmed")
                        .setText("Ok, but set some time aside for me tomorrow. I'm feeling neglected =(")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iLazy")
                        .setText("Oh, come on. That's no excuse")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iGoal")
                        .setText("All right! Yesterday we did [[qStepsYesterday]] steps, Lets aim for 10,000 steps")
                        .build()
        )




                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(numericTrigger("qStreak").gt(2))
                                .addTrigger(singlechoiceTrigger("qGender").is("Male"))
                                .withAndOperator()
                                .addAction(showField("iConsistencyMale"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(numericTrigger("qStreak").gt(2))
                        .addTrigger(singlechoiceTrigger("qGender").is("Female"))
                        .withAndOperator()
                        .addAction(showField("iConsistencyFemale"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qExerciseToday").is("No"))
                        .addTrigger(numericTrigger("qStreak").lt(3))
                        .withAndOperator()
                        .addAction(showField("qNoExerciseReason"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qExerciseToday").is("No"))
                        .addTrigger(numericTrigger("qStreak").gt(2))
                        .withAndOperator()
                        .addAction(showField("qNoExerciseReasonWithStreak"))
                        .build()
        )


                //no, without streak

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Tired"))
                                .addTrigger(numericTrigger("qDaysTired").lt(3))
                                .withAndOperator()
                                .addAction(showField("qTiredConfirm"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Tired"))
                        .addTrigger(numericTrigger("qDaysTired").gt(2))
                        .withAndOperator()
                        .addAction(showField("qTiredConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").lt(2))
                        .withAndOperator()
                        .addAction(showField("iSickConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").gt(1))
                        .withAndOperator()
                        .addAction(showField("iSickConsistentConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").lt(3))
                        .withAndOperator()
                        .addAction(showField("qBusyConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").gt(2))
                        .withAndOperator()
                        .addAction(showField("qBusyConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
                        .addAction(showField("iLazy"))
                        .build()
        )


                //no, with streak

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Tired"))
                                .addTrigger(numericTrigger("qDaysTired").lt(3))
                                .withAndOperator()
                                .addAction(showField("qTiredConfirm"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Tired"))
                        .addTrigger(numericTrigger("qDaysTired").gt(2))
                        .withAndOperator()
                        .addAction(showField("qTiredConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").lt(2))
                        .withAndOperator()
                        .addAction(showField("iSickConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").gt(1))
                        .withAndOperator()
                        .addAction(showField("iSickConsistentConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").lt(3))
                        .withAndOperator()
                        .addAction(showField("qBusyConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").gt(2))
                        .withAndOperator()
                        .addAction(showField("qBusyConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
                        .addAction(showField("iLazy"))
                        .build()
        )




                //no confirmations

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qTiredConfirm").is("No way"))
                                .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("No way"))
                                .withOrOperator()
                                .addAction(showField("iTiredConfirmed"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qBusyConfirm").is("No way"))
                        .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("No way"))
                        .withOrOperator()
                        .addAction(showField("iBusyConfirmed"))
                        .build()
        )



                //yes, easy

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
                                .withAndOperator()
                                .addAction(showField("iGoal"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
                        .withAndOperator()
                        .addAction(showField("iGoal"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
                        .withAndOperator()
                        .addAction(showField("iGoal"))
                        .build()
        )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
                                .withAndOperator()
                                .addAction(showField("iGoal"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
                                .withAndOperator()
                                .addAction(showField("iGoal"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
                                .withAndOperator()
                                .addAction(showField("iGoal"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
                                .withAndOperator()
                                .addAction(showField("iGoal"))
                                .build()
                )

                .build() ;
        

        survey.setUsername(user.getUsername());
		LocalDateTime date=new LocalDateTime();
		survey.setExpiry(date.plusHours(7).toDate());
		survey.setNeedResponse(true);
		survey.setMobileNumber(user.getMobileNnumber());
		survey.setSurvey_id(1);
        
		
		// Create response for hidden fields
        
		//date.toDate();
		Activity activity= activityService.prepareNewRow(user, date.toLocalDate());
		
		
        Response response = new Response(survey, "blah")
        .setAnswer("qStreak", NumericAnswer.withNumber(activity.getDays_streak()))
        .setAnswer("qDaysTired", NumericAnswer.withNumber(activity.getDays_tired()))
        .setAnswer("qSickStreak", NumericAnswer.withNumber(activity.getDays_sick()))
        .setAnswer("qDaysBusy", NumericAnswer.withNumber(activity.getDays_busy()))
        .setAnswer("qGender", SinglechoiceAnswer.withChoice(user.getGender()))
        .setAnswer("qStepsYesterday", NumericAnswer.withNumber(activity.getSteps_yesterday()));
//        .setAnswer("qDifficulty", SinglechoiceAnswer.withChoice(activity.getDifficulty_yesterday()))
//        .setAnswer("qGoal", NumericAnswer.withNumber(activity.getGoal_steps()));
        response.setSurvey_id(1);
       
        
        Schema schema = new Schema();
        schema.setSurvey(survey);
        schema.setResponse(response);
        return schema;

    }


	 
	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

}
