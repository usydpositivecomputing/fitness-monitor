package org.poscomp.fitnesstrack.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.poscomp.fitnesstrack.fitbit.FitbitUtils;
import org.poscomp.fitnesstrack.fitbit.model.Activities;
import org.poscomp.fitnesstrack.fitbit.model.Device;
import org.poscomp.fitnesstrack.scheduler.ScheduledTasks;
import org.poscomp.fitnesstrack.spring.model.Activity;
import org.poscomp.fitnesstrack.spring.model.User;
import org.poscomp.fitnesstrack.spring.service.ActivityService;
import org.poscomp.fitnesstrack.spring.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Scope(value="singleton")
@PropertySource("classpath:deployment.properties")
public class DataCollect implements InitializingBean{
	
	@Autowired
    private Environment env;
	
	@Resource(name = "UserService")
	UserService userService;
	
	@Resource(name = "ActivityService")
	ActivityService activityService;
	
	@Autowired
    private FitbitUtils fitbit ;
	
	private static Logger logger = LoggerFactory.getLogger(DataCollect.class) ;
	
	public void synchFitbitData()
	{
		// get Active Users
		List<User> users=userService.getActiveUsers();
		
		for(User user:users)
		{
			try
			{
			fitbit.getService().oauth_reuseAccessToken(user.getFitbit_tokenkey(), user.getFitbit_tokensecret());
			Date fitbitLastSynchDate=getLastSynch();
			synchUserData(fitbitLastSynchDate,user);
			}
			catch(Exception e)
			{
				logger.error("Error Collecting Data for Username: " +user.getUsername());
			}
				
			
/*			fitbit.getService().
			if(user.getLast_synch()==null || user.get)*/
		}
	}
	
	// Synchronise User data with Fitbit Data if outdated
	public void synchUserData(Date fitbitLastSyncDate,User user)
	{
		LocalDateTime today = new LocalDateTime();
		LocalDateTime fitbitLastSynchDate=new LocalDateTime(fitbitLastSyncDate);
		LocalDateTime userLastSynch=null;
		List<Activity> activityList=new ArrayList<Activity>();
		
		if(user.getLast_synch()==null)
		{
			userLastSynch=new LocalDateTime();
			userLastSynch=userLastSynch.minusWeeks(1);
		}
		else 
			userLastSynch=new LocalDateTime(user.getLast_synch());
		//if synched then exit
		if(fitbitLastSynchDate.isEqual(userLastSynch))
			return;
		
		// if synch equal to same day but different time then synch only today
		if(fitbitLastSynchDate.toLocalDate().equals(userLastSynch.toLocalDate()) && fitbitLastSynchDate.isAfter(userLastSynch))
		{
			Activities fitbitActivity=fitbit.getService().api_getActivities(fitbitLastSynchDate.toDate());
			activityList.add(new Activity(fitbitLastSynchDate.toDate(),user,fitbitActivity));
		}
		else
		{
			LocalDate userSynchDateLocalDate=userLastSynch.toLocalDate();
			LocalDate fitbitSynchDateLocalDate=fitbitLastSynchDate.toLocalDate();
			
			while(!userSynchDateLocalDate.isAfter(fitbitSynchDateLocalDate))
			{
				Activities fitbitActivity=fitbit.getService().api_getActivities(userSynchDateLocalDate.toDate());
				//user.setLast_synch(fitbitLastSyncDate);
				activityList.add(new Activity(userSynchDateLocalDate.toDate(),user,fitbitActivity));
				userSynchDateLocalDate=userSynchDateLocalDate.plusDays(1);
			}
		}
		
		// add activity list to DB
		for(Activity activity: activityList)
		{
			activityService.overwriteFitbitData(activity);
		}
		
		//set user last synch date
		user.setLast_synch(fitbitLastSyncDate);
		userService.updateUser(user);
	}
	
	

	// Check if SYNCH is required
	private Date getLastSynch()
	{
		Date lastSynch=null;
		List<Device> devices=fitbit.getService().api_getDevices();
		
		for(Device device:devices)
		{
			if(lastSynch==null)
			{
				lastSynch=device.getLastSyncTime();
			}
			else
			{
				if(lastSynch.before(device.getLastSyncTime()))
					lastSynch=device.getLastSyncTime();
			}
		}
		return lastSynch;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}
	

}
