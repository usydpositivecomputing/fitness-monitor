package org.poscomp.fitnesstrack.controller;




import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.poscomp.fitnesstrack.fitbit.FitbitService;
import org.poscomp.fitnesstrack.fitbit.FitbitUtils;
import org.poscomp.fitnesstrack.fitbit.model.Device;
import org.scribe.model.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class AuthenticationController {

    private static Logger logger = LoggerFactory.getLogger(AuthenticationController.class) ;


    @Autowired
    private FitbitUtils fitbit ;


    @RequestMapping(value="/login", method = RequestMethod.GET)
    private String login(HttpServletRequest request) {
    	//test server url
    	String url=fitbit.getService().oauth_getAuthorizationUrl();
    	
        return "redirect:" + url;
    }

    @RequestMapping(value="/callback", method = RequestMethod.GET)
    private String handleCallback(
            @RequestParam String oauth_token,
            @RequestParam String oauth_verifier
    )   {

        Token accessToken=fitbit.getService().oauth_getAccessToken(oauth_verifier);
        logger.info("Access token: " + accessToken);

        return "redirect:./me" ;
    }

    @RequestMapping(value="/me", method = RequestMethod.GET)
    private @ResponseBody
    String geMe() throws ParseException {
    	
        return fitbit.getService().api_getUserInfo().toString();

    }
    

    @RequestMapping(value="/activity", method = RequestMethod.GET)
    private @ResponseBody
    String getActivity() throws ParseException {
    	fitbit.getService().oauth_reuseAccessToken("e9883243baa28eec04658eccd91dc8f3", "a0193a3d742109ee3ce446d1c2d5c87b");
  
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    	Date d = sdf.parse("23/12/2014");
        return fitbit.getService().api_getActivities(d).toString();
    }
    
    @RequestMapping(value="/devices", method = RequestMethod.GET)
    private @ResponseBody
    List<Device> getDevices() {
    	fitbit.getService().oauth_reuseAccessToken("e9883243baa28eec04658eccd91dc8f3", "a0193a3d742109ee3ce446d1c2d5c87b");
  
        return fitbit.getService().api_getDevices();

    }
    
    public String getServerUrl(HttpServletRequest req) {

        String scheme = req.getScheme();
        String serverName = req.getServerName();
        int serverPort = req.getServerPort();
        String contextPath = req.getContextPath();

        StringBuilder url = new StringBuilder() ;

        url
                .append(scheme)
                .append("://")
                .append(serverName);

        if ((serverPort != 80) && (serverPort != 443)) {
            url
                    .append(":")
                    .append(serverPort);
        }

        url.append(contextPath) ;

        return url.toString() ;
    }
}
