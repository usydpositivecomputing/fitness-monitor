package org.poscomp.fitnesstrack.controller;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import org.poscomp.fitnesstrack.error.Unauthorized;
import org.poscomp.fitnesstrack.spring.model.User;
import org.poscomp.fitnesstrack.spring.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;

import java.nio.charset.Charset;

/**
 * Created by dmilne on 6/11/14.
 */
public class ControllerBase {

    private static Logger logger = LoggerFactory.getLogger(ControllerBase.class) ;

    private static final String USER_ID = "userId" ;

    @Autowired
    protected UserService userService;


    public User getCaller(String auth) throws Unauthorized {

        if (auth == null || !auth.startsWith("Basic"))
            throw new Unauthorized("No authorization header") ;

        //logger.info("header: " + auth) ;

        String base64Credentials = auth.substring("Basic".length()).trim();
        String credentials = new String(Base64.decode(base64Credentials), Charset.forName("UTF-8"));

        String[] values = credentials.split(":",2);

        String screenName = values[0] ;
        String password = values[1] ;

        //logger.info("screenName " + screenName) ;
        //logger.info("password " + password) ;

        User user = userService.getUserByUserName(screenName) ;

        if (user == null)
            throw new Unauthorized("Invalid username") ;

        if (!user.getPassword().equals(password))
            throw new Unauthorized("Invalid password") ;

        return user ;
    }
    
    public User getAdminCaller(String auth) throws Unauthorized {

    	User user= getCaller(auth);
    	if (!user.isRoleAdmin())
             throw new Unauthorized("Invalid password") ;
        return user ;
    }

}
