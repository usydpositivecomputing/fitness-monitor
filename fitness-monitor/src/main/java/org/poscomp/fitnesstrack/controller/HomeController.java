package org.poscomp.fitnesstrack.controller;

import static org.poscomp.ask.model.logic.Actions.showField;
import static org.poscomp.ask.model.logic.Triggers.singlechoiceTrigger;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.joda.time.LocalDate;
import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.error.InvalidRule;
import org.poscomp.ask.error.InvalidSurvey;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.fields.Instruction;
import org.poscomp.ask.model.fields.SinglechoiceQuestion;
import org.poscomp.ask.model.logic.FieldRule;
import org.poscomp.fitnesstrack.beans.DataCollect;
import org.poscomp.fitnesstrack.beans.SurveyGenerate;
import org.poscomp.fitnesstrack.error.BadRequest;
import org.poscomp.fitnesstrack.spring.model.Dialogue;
import org.poscomp.fitnesstrack.spring.model.QnA;
import org.poscomp.fitnesstrack.spring.model.Questions;
import org.poscomp.fitnesstrack.spring.model.User;
import org.poscomp.fitnesstrack.spring.service.DialogueService;
import org.poscomp.fitnesstrack.spring.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	@Resource(name = "UserService")
	UserService userService;
	
	@Resource(name = "DialogueService")
	DialogueService dialogueService;
	
	@Autowired
	SurveyGenerate surveyGenerate; 
	 
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value="/test", method = RequestMethod.GET)
	public @ResponseBody String test() 
	{
		/*User user=userService.getUserByUserName("admin");
		List<Questions> qList=dialogueService.getQuestionList();
		
		QnA qna=new QnA();
		qna.setAnswer("NA");
		LocalDate date = new LocalDate().minusDays(1);
		qna.setField(qList.get(1));
		
		Dialogue dlg= dialogueService.getDialogue("admin", date.toDate());
	//	List<QnA> qnalist=new ArrayList<QnA>();
		dlg.getQnaList().add(qna);
		Dialogue dlg=new Dialogue();
		dlg.setQnaList(qnalist);
		dlg.setDate(date.toDate());
		dlg.setUser(user);
		dialogueService.updateDialogue(dlg);	
		//dialogueService.overwrite(dlg);
*/		return "Success";
	}
	
	@RequestMapping(value="/survey1", method = RequestMethod.GET)
	public @ResponseBody String survey1() throws BadRequest
	{
		try {
			surveyGenerate.generateMorningSurvey();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new BadRequest("Failed");
		}
		return "Success";
	}

	@RequestMapping(value="/survey2", method = RequestMethod.GET)
	public @ResponseBody String survey2() 
	{
		logger.info("Survey2 API CAlled");
		surveyGenerate.generateEveningSurvey();
		logger.info("Survey2 API Finished");
		return "Success";
	}
	
	private Survey generateTestSurvey() throws InvalidField, InvalidRule, InvalidSurvey
	{

			Survey survey = new Survey.Builder("Morning survey")
			.addField(
			        new SinglechoiceQuestion.Builder()
			                .setId("qfeeling")
			                .setQuestion("Hi, do you feel like excercising today?")
			                .addChoice("Yes", "Yes")
			                .addChoice("No", "No")
			                .build()
			).addField(
			        new Instruction.Builder()
			                .setId("iGood")
			                .setText("Great, you did a good job yesterday, can you excecise a bit harder?")
			                .build()
			).addField(
					 new Instruction.Builder()
		                .setId("iBad")
		                .setText("Oh I hope you feel better tomorow")
		                .build()
			).addFieldRule(
			        new FieldRule.Builder()
			                .addTrigger(singlechoiceTrigger("qfeeling").is("Yes"))
			                .addAction(showField("iGood"))
			                .build()
			).addFieldRule(
			        new FieldRule.Builder()
			                .addTrigger(singlechoiceTrigger("qfeeling").is("No"))
			                .addAction(showField("iBad"))
			                .build()
			).build() ;
			
			return survey;
	}
	
}
