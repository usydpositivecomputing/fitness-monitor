package org.poscomp.fitnesstrack.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import org.poscomp.fitnesstrack.error.BadRequest;
import org.poscomp.fitnesstrack.error.NotFound;
import org.poscomp.fitnesstrack.error.Unauthorized;
import org.poscomp.fitnesstrack.rest.model.UserRestModel;
import org.poscomp.fitnesstrack.spring.model.User;
import org.poscomp.fitnesstrack.spring.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;



@Controller
//@Api(value = "users", position = 1)
public class UserController extends ControllerBase {

	@Autowired
	UserService userService;

    private static Logger logger = LoggerFactory.getLogger(UserController.class) ;

   /* @ApiOperation(
            value = "Returns the caller",
            notes = "Returns the caller."
    )*/
    @RequestMapping(value="/users/me", method= RequestMethod.GET)
    public @ResponseBody UserRestModel getMe(

            @RequestHeader(value="Authorization")
            String auth
    ) throws Unauthorized {

        User caller = getCaller(auth) ;
       
        return new UserRestModel(caller).redactPassword() ;
    }
    
    @RequestMapping(value="/users/admin", method= RequestMethod.GET)
    public @ResponseBody UserRestModel getAdminMe(

            @RequestHeader(value="Authorization")
            String auth
    ) throws Unauthorized {

        User caller = getAdminCaller(auth);
       
        return new UserRestModel(caller).redactPassword() ;
    }

   /* @ApiOperation(
            value = "Either registers a new user, or edits a existing one",
            notes = "Either registers a new user, or edits an existing one\n\n" +
                    "* To register a new user, simply specify the user's details without an id (which will be automatically assigned). No authentication is required to do this.\n" +
                    "* To edit an existing user, specify the new details and the user's id. You must authenticate as the user you are attempting to edit."
    )*/
    @RequestMapping(value="/users", method= RequestMethod.POST)
    public @ResponseBody UserRestModel postUser(

            @RequestBody UserRestModel me,

            @RequestHeader(value="Authorization")
            String auth
    ) throws Unauthorized, NotFound , Exception{

    	//Check if Authorised
    	User caller = getAdminCaller(auth);
    	
    	User userToSave=new User(me);
    	userToSave.setCreated_on(new Date());
		userToSave.setStatus(true);
    	try
		{
			userService.addUser(userToSave);
		}
		catch(Exception e)
		{
			throw new Exception("Unable to add user");
		}
    	
    	return new UserRestModel(userToSave);
    }
    
    
    // RETURN OF ALL USERS
    @RequestMapping(value="/users", method= RequestMethod.GET)
    public @ResponseBody List<UserRestModel> getUserList(
    		@RequestParam(required=false)
	        String service,
            @RequestHeader(value="Authorization")
            String auth
    ) throws Unauthorized, BadRequest {

        User caller = getAdminCaller(auth) ;
        List<User> users=null;
        if(service==null)
        {
        	users=userService.getUsers();
        }
        else
        {
        	if(service.toLowerCase().equals("sms"))
        		users=userService.getSmsUsers();
        	else
        		throw new BadRequest("Invalid request param Service="+service);
        }
        return convertToRestUserModel(users) ;
    }

    private static List<UserRestModel> convertToRestUserModel(List<User> users)
    {
    	List<UserRestModel> userRestList=new ArrayList<UserRestModel>();
    	for(User user:users)
    	{
    		UserRestModel temp=new UserRestModel(user);
    		userRestList.add(temp.redactPassword());
    	}
    	return userRestList;
    }
   /* public boolean isEmailUnused(String email) {

        User u = userRepo.findByEmail(email) ;

        return u == null ;
    }

    public boolean isScreenNameUnused(String screenName) {

        User u = userRepo.findByScreenName(screenName) ;

        return u == null ;
    }*/

}
