package org.poscomp.fitnesstrack.controller;

import static org.poscomp.ask.model.logic.Actions.showField;
import static org.poscomp.ask.model.logic.Triggers.singlechoiceTrigger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.error.InvalidRule;
import org.poscomp.ask.error.InvalidSurvey;
import org.poscomp.ask.model.Response;
import org.poscomp.ask.model.Schema;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.fields.FreetextQuestion;
import org.poscomp.ask.model.fields.Instruction;
import org.poscomp.ask.model.fields.SinglechoiceQuestion;
import org.poscomp.ask.model.logic.FieldRule;
import org.poscomp.ask.util.JsonUtils;
import org.poscomp.fitnesstrack.beans.SurveyGenerate;
import org.poscomp.fitnesstrack.error.BadRequest;
import org.poscomp.fitnesstrack.error.Unauthorized;
import org.poscomp.fitnesstrack.rest.model.UserRestModel;
import org.poscomp.fitnesstrack.spring.model.Activity;
import org.poscomp.fitnesstrack.spring.model.Dialogue;
import org.poscomp.fitnesstrack.spring.model.QnA;
import org.poscomp.fitnesstrack.spring.model.Questions;
import org.poscomp.fitnesstrack.spring.model.User;
import org.poscomp.fitnesstrack.spring.service.ActivityService;
import org.poscomp.fitnesstrack.spring.service.DialogueService;
import org.poscomp.fitnesstrack.spring.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;




@Controller
public class SurveyController extends ControllerBase {
	
	@Resource(name = "UserService")
	UserService userService;
	
	@Resource(name = "DialogueService")
	DialogueService dialogueService;
	
	@Resource(name = "ActivityService")
	ActivityService activityService;
	
	@Autowired
	SurveyGenerate surveyGenerate; 
	
	private static Logger logger = LoggerFactory.getLogger(SurveyController.class) ;
	

	/*@RequestMapping(value = "/restapi/user", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> addUser(
			@RequestBody UserRestModel restUserModel)
	{
		try
		{
			User userToSave=new User(restUserModel);
			userService.addUser(userToSave);
		}
		catch(Exception e)
		{
			return new ResponseEntity<String>("fail", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>("success", HttpStatus.OK);
	}*/
	
	@RequestMapping(value="/Survey", method = RequestMethod.GET)
	public @ResponseBody Schema getSurvey(
			@RequestHeader(value="Authorization")
            String auth) throws Unauthorized, BadRequest
	{
		User caller = getCaller(auth);
		logger.info("/restapi/Questions Get called");
		Schema schema=null;
		try{
			LocalDate date=new LocalDate();
			Dialogue dlg=dialogueService.getDialogue(caller.getUsername(), date.toDate());
			if(dlg==null)
				return null;
			String json=dlg.getSurvey_current();
			if(json!=null)
			{
				schema = Schema.fromJson(json);
			
			}
			
		}
		catch(Exception e)
		{
			throw new BadRequest("Error generating Survey");
		}
		 
		return schema;
	}
	
	@RequestMapping(value="/Surveys", method = RequestMethod.GET)
	public @ResponseBody List<Schema> getSurveyList(
			@RequestParam
	        String service,
			@RequestHeader(value="Authorization")
            String auth) throws Unauthorized, BadRequest
	{ 
		User caller = getAdminCaller(auth);
		List<Schema> schemaList=new ArrayList<Schema>();
		
		// check if req param equal to sms, if not return an empty list
		if(!service.equals("sms"))
			return schemaList;
			
		logger.info("restapi/Surveys Get called");
		
		try{
			List<User> userList=userService.getSmsUsers();
			LocalDate date=new LocalDate();
			
			for(User user:userList)				
			{
				Schema schema=null;
				try{
				Dialogue dlg = dialogueService.getDialogue(user.getUsername(), date.toDate());
				
				if(dlg==null)
				{
					logger.warn("User :"+user.getUsername()+" dialogue row entry not found.");
					continue;
				}
				String json=dlg.getSurvey_current();
				if(json!=null)
				{
					schema = Schema.fromJson(json);
					schemaList.add(schema);
				}
				}
				catch(Exception e)
				{
					logger.error("Error in RestAPI Surveys for user: "+user.getUsername());
				}
				
			}
			
			
		}
		catch(Exception e)
		{
			logger.error("Error in Get Suryeys API");
			throw new BadRequest("Error in Get Suryeys API");
		}
		 
		return schemaList;
	}
	
	@RequestMapping(value="/Response", method = RequestMethod.POST)
	public @ResponseBody  Response saveResponse(
			@RequestBody String responseJson,
			@RequestHeader(value="Authorization")
            String auth) throws Unauthorized
	{
		User caller = getCaller(auth) ;
		logger.info("/restapi/Response POST called");
		
		Response response=null;
		try {
			response = fromJson(responseJson);
			if(caller.isAdaptive_aproach())
				surveyGenerate.processResponse(response.getUsername(),response);
			else
				surveyGenerate.processResponsePrescriptive(response.getUsername(),response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
				
		return response;
	}
	
	@RequestMapping(value="/Response/admin", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> saveResponseAdmin(
			@RequestBody String responseJson,
			@RequestHeader(value="Authorization")
            String auth) throws Unauthorized, BadRequest
	{
		User caller = getAdminCaller(auth) ;
		logger.info("/restapi/Response/admin POST called");
		// checking mandatory fields
		
		
		Response response=null;
		try {
			
			if(responseJson==null)
				throw new BadRequest("Response Object Empty");	
			response = fromJson(responseJson);
			
			if(response.getUsername()==null || response.getSurvey_id()==0)
				throw new BadRequest("Mandatory fields missing");
			
			User user= userService.getUserByUserName(response.getUsername());
			
			if(user==null )
				throw new BadRequest("User name missing");
			if(user.isAdaptive_aproach())
				surveyGenerate.processResponse(response.getUsername(),response);
			else
				surveyGenerate.processResponsePrescriptive(response.getUsername(),response);
			
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
				
		return new ResponseEntity<String>("success",HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(value="/Dialogue1", method = RequestMethod.GET)
	public @ResponseBody Schema getUserDialogue1(
			@RequestHeader(value="Authorization")
            String auth,
            @RequestParam
	        String username,
	        @RequestParam(required= false)
	        String date) throws Unauthorized, BadRequest
	{
		User caller = getAdminCaller(auth);
		logger.info("/restapi/Questions Get called");
		Schema schema=null;
		Response response = null;
		try{
			LocalDate localdate=new LocalDate();
			Dialogue dlg=dialogueService.getDialogue(caller.getUsername(), localdate.toDate());
			if(dlg==null)
				return null;
			String survey=dlg.getSurvey_morning();
			if(survey!=null)
			{
				schema = Schema.fromJson(survey);
			}
			String responseString=dlg.getResponse_morning();
			if(survey!=null)
			{
				response = fromJson(responseString);
				schema.setResponse(response);
			}
			
			
		}
		catch(Exception e)
		{
			throw new BadRequest("Error generating Survey");
		}
		 
		return schema;
	}
	
	@RequestMapping(value="/Dialogue2", method = RequestMethod.GET)
	public @ResponseBody Schema getUserDialogue2(
			@RequestHeader(value="Authorization")
            String auth,
            @RequestParam
	        String username,
	        @RequestParam(required= false)
	        String date) throws Unauthorized, BadRequest
	{
		User caller = getAdminCaller(auth);
		logger.info("/restapi/Questions Get called");
		Schema schema=null;
		Response response = null;
		try{
			LocalDate localdate=new LocalDate();
			Dialogue dlg=dialogueService.getDialogue(caller.getUsername(), localdate.toDate());
			if(dlg==null)
				return null;
			String survey=dlg.getSurvey_evening();
			if(survey!=null)
			{
				schema = Schema.fromJson(survey);
			}
			String responseString=dlg.getResponse_evening();
			if(survey!=null)
			{
				response = fromJson(responseString);
				schema.setResponse(response);
			}
						
		}
		catch(Exception e)
		{
			throw new BadRequest("Error generating Survey");
		}
		 
		return schema;
	}
	
	 public static Response fromJson(String json) throws IOException, InvalidSurvey {

	        ObjectMapper mapper = JsonUtils.getObjectMapper() ;

	        Response resp = mapper.readValue(json, Response.class) ;

	       // survey.assertIsComplete();

	        return resp ;
	    }

	/*@RequestMapping(value="/Questions", method = RequestMethod.GET)
	public @ResponseBody List<Question> getQuestions(
			@RequestHeader(value="Authorization")
            String auth) throws Unauthorized
	{
		User caller = getCaller(auth) ;
		logger.info("/restapi/Questions Get called");
		return null;//generateList();
		
	}
	
	
	@RequestMapping(value="/Questions", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> setQuestions(
			@RequestBody List<Question> questions,
			@RequestHeader(value="Authorization")
            String auth) throws Unauthorized
	{
		User caller = getCaller(auth) ;
		logger.info("/restapi/Questions POST called");
		return new ResponseEntity<String>(HttpStatus.ACCEPTED);
	}*/
	 
	@RequestMapping(value = "/DailySummary", method = RequestMethod.GET)
	public @ResponseBody Activity getDailyFitnessSummary(
			@RequestHeader(value = "Authorization") String auth)
			throws Unauthorized, BadRequest {
		User caller = getCaller(auth);
		logger.info("/restapi/Questions Get called");

		try {
			LocalDate date = new LocalDate();
			Activity activity = activityService.getActivity(
					caller.getUsername(), date.toDate());
			if (activity == null)
				return new Activity();

			return activity;
		} catch (Exception e) {
			throw new BadRequest("Error generating Survey");
		}

	}
	
	private Survey generateTestSurvey() throws InvalidField, InvalidRule, InvalidSurvey
	{

			Survey survey = new Survey.Builder("Morning survey")
			.addField(
			        new SinglechoiceQuestion.Builder()
			                .setId("qfeeling")
			                .setQuestion("Hi, do you feel like excercising today?")
			                .addChoice("Yes", "Yes")
			                .addChoice("No", "No")
			                .build()
			).addField(
			        new Instruction.Builder()
			                .setId("iGood")
			                .setText("Great, you did a good job yesterday, can you excecise a bit harder?")
			                .build()
			).addField(
					 new Instruction.Builder()
		                .setId("iBad")
		                .setText("Oh I hope you feel better tomorow")
		                .build()
			).addFieldRule(
			        new FieldRule.Builder()
			                .addTrigger(singlechoiceTrigger("qfeeling").is("Yes"))
			                .addAction(showField("iGood"))
			                .build()
			).addFieldRule(
			        new FieldRule.Builder()
			                .addTrigger(singlechoiceTrigger("qfeeling").is("No"))
			                .addAction(showField("iBad"))
			                .build()
			).build() ;
			
			return survey;
	}
	
}
