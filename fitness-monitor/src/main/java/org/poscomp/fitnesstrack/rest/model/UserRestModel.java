package org.poscomp.fitnesstrack.rest.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.poscomp.fitnesstrack.spring.model.User;





public class UserRestModel implements Serializable {


    private String username;
	
	private String password;
	
	private String firstName;
	
	private String lastName;
	
	private Date dateOfBith;
	
	private String gender;

	private String emailAddress;
	
	private String mobileNnumber;
	
	
	private Date created_on;

	private boolean status;
	
	private String fitbit_tokenkey;
	
	private String fitbit_tokensecret;
	
	private Date last_synch;
	
	private String country;
	
	private String address;
	
	private boolean roleAdmin;
	
	private boolean smsService;
	
	boolean adaptive_aproach;

	public UserRestModel()
	{
		
	}
	
	public UserRestModel(User user)
	{

		username = user.getUsername();

		password = user.getPassword();

		firstName = user.getFirstName();

		lastName = user.getLastName();

		dateOfBith = user.getDateOfBith();

		gender = user.getGender();

		emailAddress = user.getEmailAddress();

		mobileNnumber = user.getMobileNnumber();

		created_on = user.getCreated_on();

		status = user.isStatus();

		fitbit_tokenkey = user.getFitbit_tokenkey();

		fitbit_tokensecret = user.getFitbit_tokensecret();

		setLast_synch(user.getLast_synch());

		country = user.getCountry();

		address = user.getAddress();
		
		roleAdmin = user.isRoleAdmin();
		
		smsService = user.isSmsService();
		
		adaptive_aproach=user.isAdaptive_aproach();
		
	}
	
	public UserRestModel redactPassword()
	{
		password=null;
		return this;
	}
	
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Date getDateOfBith() {
		return dateOfBith;
	}

	public String getGender() {
		return gender;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getMobileNnumber() {
		return mobileNnumber;
	}

	public String getCountry() {
		return country;
	}

	public String getAddress() {
		return address;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setDateOfBith(Date dateOfBith) {
		this.dateOfBith = dateOfBith;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setMobileNnumber(String mobileNnumber) {
		this.mobileNnumber = mobileNnumber;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFitbit_tokenkey() {
		return fitbit_tokenkey;
	}

	public void setFitbit_tokenkey(String fitbit_tokenkey) {
		this.fitbit_tokenkey = fitbit_tokenkey;
	}

	public String getFitbit_tokensecret() {
		return fitbit_tokensecret;
	}

	public void setFitbit_tokensecret(String fitbit_tokensecret) {
		this.fitbit_tokensecret = fitbit_tokensecret;
	}

	public Date getCreated_on() {
		return created_on;
	}

	public boolean isStatus() {
		return status;
	}

	public boolean isAdaptive_aproach() {
		return adaptive_aproach;
	}

	public void setAdaptive_aproach(boolean adaptive_aproach) {
		this.adaptive_aproach = adaptive_aproach;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getLast_synch() {
		return last_synch;
	}

	public void setLast_synch(Date last_synch) {
		this.last_synch = last_synch;
	}

	public boolean isRoleAdmin() {
		return roleAdmin;
	}

	public void setRoleAdmin(boolean roleAdmin) {
		this.roleAdmin = roleAdmin;
	}

	public boolean isSmsService() {
		return smsService;
	}

	public void setSmsService(boolean smsService) {
		this.smsService = smsService;
	}
	
		
}