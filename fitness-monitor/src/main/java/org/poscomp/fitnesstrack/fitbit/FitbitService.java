package org.poscomp.fitnesstrack.fitbit;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.poscomp.fitnesstrack.fitbit.model.Activities;
import org.poscomp.fitnesstrack.fitbit.model.Device;
import org.poscomp.fitnesstrack.fitbit.model.Devices;
import org.poscomp.fitnesstrack.fitbit.model.User;
import org.poscomp.fitnesstrack.fitbit.model.UserInfo;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


public class FitbitService 
{


    private OAuthService service=null;

   /* private String clientId;
    private String clientSecret; 
    private String callbackUrl;*/
    private Token accessToken=null;
    private Token requestToken=null;
    

    public FitbitService(String clientId,String clientSecret,String callbackUrl) 
    {

       /* this.clientId = clientId;
        this.clientSecret = clientSecret ;
        this.callbackUrl = callbackUrl ;*/


        service = new ServiceBuilder()
        .provider(FitbitApi.class)
        .apiKey(clientId)
        .apiSecret(clientSecret)
        .callback(callbackUrl)
        .build();
    }
   
    public Token oauth_getAccessToken(String oauth_verifier)
    {
    	Verifier verifier = new Verifier(oauth_verifier);
    	accessToken = service.getAccessToken(requestToken, verifier);
    	return accessToken;
    }
    
    // get Fitbit Authorization URL
    public String oauth_getAuthorizationUrl()
    {
    	requestToken = service.getRequestToken();
    	String url=service.getAuthorizationUrl(requestToken);
    	return url;
    }
    
    // Reuse previously generated Access token
    public void oauth_reuseAccessToken(String tokenKey,String tokenSecret)
    {
    	accessToken=new Token(tokenKey,tokenSecret);
    }
    
    // Fetch User Info
    public User api_getUserInfo()
    {
    	OAuthRequest request = new OAuthRequest(Verb.GET, FitbitUrls.getProfileUrl());
	    service.signRequest(accessToken, request);
	    Response response = request.send();
	    String json=response.getBody();
	    //convert the json string back to object
	    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
	  	User userInfo = gson.fromJson(json, User.class);
	    return userInfo;
    }
    
    // Fetch Activity
    public Activities api_getActivities(Date date)
    {
    	OAuthRequest request = new OAuthRequest(Verb.GET, FitbitUrls.getActivitiesUrl(date));
	    service.signRequest(accessToken, request);
	    Response response = request.send();
	    String json=response.getBody();
	    //convert the json string back to object
	    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
	    Activities activities = gson.fromJson(json, Activities.class);
	    return activities;
    }
    
    // Fetch Devices
    public List<Device> api_getDevices()
    {
    	OAuthRequest request = new OAuthRequest(Verb.GET, FitbitUrls.getDevicesUrl());
	    service.signRequest(accessToken, request);
	    Response response = request.send();
	    String json=response.getBody();
	    //convert the json string back to object
	    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
	    Type listType = new TypeToken<ArrayList<Device>>() {}.getType();
	    List<Device> deviceList = gson.fromJson(json, listType);
	    
	    return deviceList;
    }
    

}

