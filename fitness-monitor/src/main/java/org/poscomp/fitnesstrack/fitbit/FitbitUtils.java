package org.poscomp.fitnesstrack.fitbit;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.poscomp.fitnesstrack.fitbit.model.Activities;
import org.poscomp.fitnesstrack.fitbit.model.Device;
import org.poscomp.fitnesstrack.fitbit.model.Devices;
import org.poscomp.fitnesstrack.fitbit.model.User;
import org.poscomp.fitnesstrack.fitbit.model.UserInfo;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


@Component
@Scope(value="singleton")
@PropertySource("classpath:fitbit.properties")
public class FitbitUtils implements InitializingBean {


    private FitbitService service=null;


    @Autowired
    private Environment env;


    private String clientId;
    private String clientSecret; 
    private String callbackUrl;
    
    @Override
    public void afterPropertiesSet() throws Exception {

        clientId = env.getProperty("fitbit.clientId") ;
        clientSecret = env.getProperty("fitbit.clientSecret") ;
        callbackUrl = env.getProperty("fitbit.callbackUrl") ;

        service = new FitbitService(clientId,clientSecret,callbackUrl);
    }
   
    public FitbitService getService()
    {
    	return service;
    }
    

}

