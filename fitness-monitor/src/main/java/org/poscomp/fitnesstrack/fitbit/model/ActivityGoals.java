package org.poscomp.fitnesstrack.fitbit.model;

public class ActivityGoals {
	private Integer caloriesOut;
	private Integer steps;
	private Double distance;
	private Integer activeScore;
	private Integer floors;
	
	public Integer getCaloriesOut() {
		return caloriesOut;
	}
	public Integer getSteps() {
		return steps;
	}
	public Double getDistance() {
		return distance;
	}
	public Integer getActiveScore() {
		return activeScore;
	}
	public Integer getFloors() {
		return floors;
	}
	public void setCaloriesOut(Integer caloriesOut) {
		this.caloriesOut = caloriesOut;
	}
	public void setSteps(Integer steps) {
		this.steps = steps;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public void setActiveScore(Integer activeScore) {
		this.activeScore = activeScore;
	}
	public void setFloors(Integer floors) {
		this.floors = floors;
	}

}
