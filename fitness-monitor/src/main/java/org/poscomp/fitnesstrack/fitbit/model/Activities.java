package org.poscomp.fitnesstrack.fitbit.model;

import java.util.List;


public class Activities {
	
	  private ActivitiesSummary summary;
//	  private List<ActivityLog> activities;
	  private ActivityGoals activityGoals;
	  
	public ActivitiesSummary getSummary() {
		return summary;
	}
	public ActivityGoals getActivityGoals() {
		return activityGoals;
	}
	public void setSummary(ActivitiesSummary summary) {
		this.summary = summary;
	}
	public void setActivityGoals(ActivityGoals activityGoals) {
		this.activityGoals = activityGoals;
	}

}
