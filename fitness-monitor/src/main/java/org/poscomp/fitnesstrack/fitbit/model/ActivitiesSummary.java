package org.poscomp.fitnesstrack.fitbit.model;

import java.util.List;


public class ActivitiesSummary {
	 private int caloriesOut;
	    private int activityCalories;
	    private int marginalCalories;
	    private int activeScore;
	    private int steps;
	    private Integer floors = null;
	    private Double elevation = null;
	    private int sedentaryMinutes;
	    private int lightlyActiveMinutes;
	    private int fairlyActiveMinutes;
	    private int veryActiveMinutes;
	    private List<ActivityDistance> distances;
	    
	    public double getTotalDistance()
	    {
	    	double totalDistance=0;
	    	for(ActivityDistance distance:distances)
	    	{
	    		if(distance.getActivity().toLowerCase().equals("total"))
	    			return distance.getDistance();
	    	}
	    	return totalDistance;
	    }
	    
		public int getCaloriesOut() {
			return caloriesOut;
		}
		public int getActivityCalories() {
			return activityCalories;
		}
		public int getMarginalCalories() {
			return marginalCalories;
		}
		public int getActiveScore() {
			return activeScore;
		}
		public int getSteps() {
			return steps;
		}
		public Integer getFloors() {
			return floors;
		}
		public Double getElevation() {
			return elevation;
		}
		public int getSedentaryMinutes() {
			return sedentaryMinutes;
		}
		public int getLightlyActiveMinutes() {
			return lightlyActiveMinutes;
		}
		public int getFairlyActiveMinutes() {
			return fairlyActiveMinutes;
		}
		public int getVeryActiveMinutes() {
			return veryActiveMinutes;
		}
		public List<ActivityDistance> getDistances() {
			return distances;
		}
		public void setCaloriesOut(int caloriesOut) {
			this.caloriesOut = caloriesOut;
		}
		public void setActivityCalories(int activityCalories) {
			this.activityCalories = activityCalories;
		}
		public void setMarginalCalories(int marginalCalories) {
			this.marginalCalories = marginalCalories;
		}
		public void setActiveScore(int activeScore) {
			this.activeScore = activeScore;
		}
		public void setSteps(int steps) {
			this.steps = steps;
		}
		public void setFloors(Integer floors) {
			this.floors = floors;
		}
		public void setElevation(Double elevation) {
			this.elevation = elevation;
		}
		public void setSedentaryMinutes(int sedentaryMinutes) {
			this.sedentaryMinutes = sedentaryMinutes;
		}
		public void setLightlyActiveMinutes(int lightlyActiveMinutes) {
			this.lightlyActiveMinutes = lightlyActiveMinutes;
		}
		public void setFairlyActiveMinutes(int fairlyActiveMinutes) {
			this.fairlyActiveMinutes = fairlyActiveMinutes;
		}
		public void setVeryActiveMinutes(int veryActiveMinutes) {
			this.veryActiveMinutes = veryActiveMinutes;
		}
		public void setDistances(List<ActivityDistance> distances) {
			this.distances = distances;
		}

}
