package org.poscomp.fitnesstrack.fitbit.model;

import java.util.Date;



public class Device {
	 /**
     * Device id
     */
    private String id;
    private String type;
    /**
     * The battery level of the Fitbit device, one of Low, Medium, High, and Full. The level is "Low" when no
     * information is available.
     */
    private String battery;

    private Date lastSyncTime;
    private String deviceVersion;
    
	public String getId() {
		return id;
	}
	public String getType() {
		return type;
	}
	public String getBattery() {
		return battery;
	}
	public Date getLastSyncTime() {
		return lastSyncTime;
	}
	public String getDeviceVersion() {
		return deviceVersion;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setBattery(String battery) {
		this.battery = battery;
	}
	public void setLastSyncTime(Date lastSyncTime) {
		this.lastSyncTime = lastSyncTime;
	}
	public void setDeviceVersion(String deviceVersion) {
		this.deviceVersion = deviceVersion;
	}

}
