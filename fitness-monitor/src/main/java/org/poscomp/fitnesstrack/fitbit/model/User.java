package org.poscomp.fitnesstrack.fitbit.model;

public class User {
	UserInfo user;

	public UserInfo getUser() {
		return user;
	}

	public void setUser(UserInfo user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "User [user=" + user + "]";
	}
	

}
