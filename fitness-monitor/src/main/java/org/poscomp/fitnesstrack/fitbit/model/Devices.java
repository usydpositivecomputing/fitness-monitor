package org.poscomp.fitnesstrack.fitbit.model;

import java.util.List;

public class Devices {
	private List<Device> device;

	public List<Device> getDevice() {
		return device;
	}

	public void setDevice(List<Device> device) {
		this.device = device;
	}
	
}
