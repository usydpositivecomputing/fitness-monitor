package org.poscomp.fitnesstrack.fitbit.model;

public class ActivityDistance {
	 private String activity;
	 private double distance;
	public String getActivity() {
		return activity;
	}
	public double getDistance() {
		return distance;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}

}
