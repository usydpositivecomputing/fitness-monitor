package org.poscomp.fitnesstrack.fitbit;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FitbitUrls {
	// url to get user info
	 private static final String PROFILE_URL = "https://api.fitbit.com/1/user/-/profile.json";
	// url to get activity info for a given day
	 private static final String ACTIVITIES_URL = "https://api.fitbit.com/1/user/-/activities/date/%s.json";
	// url to get activity info for a given day
	 private static final String DEVICES_URL = "https://api.fitbit.com/1/user/-/devices.json";
	 
	public static final SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
	 
	public static String getProfileUrl() {
		return PROFILE_URL;
	}
	public static String getActivitiesUrl(Date date) {
		return String.format(ACTIVITIES_URL, dateformat.format(date));
		
	}
	public static String getDevicesUrl() {
		return DEVICES_URL;
	}
	

}
