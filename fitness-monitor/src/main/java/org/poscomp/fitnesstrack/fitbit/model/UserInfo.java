package org.poscomp.fitnesstrack.fitbit.model;

import java.util.Date;
import java.util.TimeZone;


public class UserInfo {
    private String encodedId;
    private String displayName;
    private String gender;
    private Date dateOfBirth;
    private double height;
    private double weight;
    private double strideLengthWalking;
    private double strideLengthRunning;
    private String fullName;
    private String nickname;
    private String country;
    private String state;
    private String city;
    private String aboutMe;
    private Date memberSince;
//    private TimeZone timezone;
    
	public String getEncodedId() {
		return encodedId;
	}
	public String getDisplayName() {
		return displayName;
	}
	public String getGender() {
		return gender;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public double getHeight() {
		return height;
	}
	public double getWeight() {
		return weight;
	}
	public double getStrideLengthWalking() {
		return strideLengthWalking;
	}
	public double getStrideLengthRunning() {
		return strideLengthRunning;
	}
	public String getFullName() {
		return fullName;
	}
	public String getNickname() {
		return nickname;
	}
	public String getCountry() {
		return country;
	}
	public String getState() {
		return state;
	}
	public String getCity() {
		return city;
	}
	public String getAboutMe() {
		return aboutMe;
	}
	public Date getMemberSince() {
		return memberSince;
	}
	
	public void setEncodedId(String encodedId) {
		this.encodedId = encodedId;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public void setStrideLengthWalking(double strideLengthWalking) {
		this.strideLengthWalking = strideLengthWalking;
	}
	public void setStrideLengthRunning(double strideLengthRunning) {
		this.strideLengthRunning = strideLengthRunning;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}
	public void setMemberSince(Date memberSince) {
		this.memberSince = memberSince;
	}
	
	@Override
	public String toString() {
		return "UserInfo [encodedId=" + encodedId + ", displayName="
				+ displayName + ", gender=" + gender + ", dateOfBirth="
				+ dateOfBirth + ", height=" + height + ", weight=" + weight
				+ ", strideLengthWalking=" + strideLengthWalking
				+ ", strideLengthRunning=" + strideLengthRunning
				+ ", fullName=" + fullName + ", nickname=" + nickname
				+ ", country=" + country + ", state=" + state + ", city="
				+ city + ", aboutMe=" + aboutMe + ", memberSince="
				+ memberSince+"]";
	}

}
