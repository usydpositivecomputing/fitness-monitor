package org.poscomp.ask.askers;

import org.poscomp.ask.model.Response;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.FreetextAnswer;
import org.poscomp.ask.model.answers.MultichoiceAnswer;
import org.poscomp.ask.model.answers.NumericAnswer;
import org.poscomp.ask.model.answers.SinglechoiceAnswer;
import org.poscomp.ask.model.data.Choice;
import org.poscomp.ask.model.fields.*;
import org.poscomp.ask.state.SurveyState;
import org.poscomp.ask.util.StringNormalizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import org.poscomp.ask.model.answers.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
* Created by dmilne on 15/12/14.
*/
public abstract class SerialAsker {

   private static Logger logger = LoggerFactory.getLogger(SerialAsker.class) ;

   private static final int DELAY = 1000 ;

   private Survey survey ;



   public SerialAsker(Survey survey) {

       this.survey = survey ;

   }


   public abstract void sendMessage(String message) ;

   public abstract String getAnswerAsString() ;

   public abstract String getSkipPrompt() ;

   public Response buildResponse(Response response) {

       SurveyState state = new SurveyState(survey, response) ;

       while (!response.isCompleted()) {

           List<Field> fields = state.getRelevantFields(response.getPageIndex()) ;

           for (Field field:fields) {

               if (!state.isVisible(field))
                   continue ;

               if (field.isHidden())
                   continue ;

               switch(field.getType()) {

                   case pageBreak:
                       handlePageBreak((PageBreak)field, state, response);
                       break ;
                   case sectionBreak:
                       handleSectionBreak((SectionBreak) field, state, response);
                       break ;
                   case instruction:
                       handleInstruction((Instruction)field, state, response) ;
                       break ;
                   case singlechoice:
                       handleSinglechoice((SinglechoiceQuestion)field, state, response);
                       break ;
                   case multichoice:
                       handleMultichoice((MultichoiceQuestion) field, state, response);
                       break ;
                   case freetext:
                       handleFreetext((FreetextQuestion) field, state, response);
                       break ;
                   case numeric:
                       handleNumeric((NumericQuestion) field, state, response);
                       break ;
                   default:
                       logger.error("Cannot handle " + field.getType() + " field") ;
               }

               if (state.isQuestion(field))
                   state.handleAnswerChanged(field.getId());

           }

           state.handleContinue() ;
       }

       if (survey.getCompletionMessage() != null)
           sendMessage(survey.getCompletionMessage());

       return response ;
   }



   private void sleep() {

       try {
           Thread.sleep(DELAY) ;
       } catch (InterruptedException e) {
           logger.warn("Insomnia") ;
       }
   }


   private void handlePageBreak(PageBreak pageBreak, SurveyState state, Response response) {

       StringBuilder msg = new StringBuilder() ;

       if (pageBreak.getTitle() != null)
           msg.append(replacePlaceholders(pageBreak.getTitle(), response).toUpperCase() + ": ") ;

       if (pageBreak.getText() != null)
           msg.append(replacePlaceholders(pageBreak.getText(), response)) ;

       if (pageBreak.getNotes() != null)
           msg.append("(" + replacePlaceholders(pageBreak.getNotes(), response) + ")") ;

       if (msg.length() > 0) {
           sendMessage(msg.toString()); ;
           sleep();
       }

   }

   private void handleSectionBreak(SectionBreak sectionBreak, SurveyState state, Response response) {

       StringBuilder msg = new StringBuilder() ;

       if (sectionBreak.getTitle() != null)
           msg.append(replacePlaceholders(sectionBreak.getTitle(), response).toUpperCase() + ": ") ;

       if (sectionBreak.getText() != null)
           msg.append(replacePlaceholders(sectionBreak.getText(), response)) ;

       if (sectionBreak.getNotes() != null)
           msg.append("(" + replacePlaceholders(sectionBreak.getNotes(), response) + ")") ;

       if (msg.length() > 0) {
           sendMessage(msg.toString()); ;
           sleep();
       }

   }


   private void handleInstruction(Instruction instruction, SurveyState state, Response response) {

       StringBuilder msg = new StringBuilder() ;

       if (instruction.getText() != null)
           msg.append(replacePlaceholders(instruction.getText(), response)) ;

       if (instruction.getNotes() != null)
           msg.append("(" + replacePlaceholders(instruction.getNotes(), response) + ")") ;

       if (msg.length() > 0) {
           sendMessage(msg.toString()); ;
           sleep();
       }

   }

   private void handleSinglechoice(SinglechoiceQuestion question, SurveyState state, Response response) {

       StringBuilder msg = new StringBuilder() ;
       msg.append(replacePlaceholders(question.getQuestion(), response)) ;

       LinkedHashSet<String> choices = new LinkedHashSet<String>() ;
       for (Choice choice:question.getChoices())
           choices.add(StringNormalizer.normalize(choice.getName())) ;

       if (question.getAllowOther())
           choices.add("other") ;

       SinglechoiceAnswer answer = null ;
       while (answer == null) {

           if (question.getChoices().size() == 2)
               msg.append("\nPlease respond with either " + getChoicesPrompt(question.getChoices(), question.getAllowOther())) ;
           else
               msg.append("\nPlease respond with one of " + getChoicesPrompt(question.getChoices(), question.getAllowOther())) ;

           if (question.isOptional())
               msg.append(" or " + getSkipPrompt() + " to skip") ;

           sendMessage(msg.toString()); ;

           String cmd = StringNormalizer.normalize(getAnswerAsString()) ;

           if (cmd.length() == 0 && question.isOptional())
               break ;

           if (question.getAllowOther() && cmd.equals("other")) {

               sendMessage("What do you mean by \"other\"?");

               String cmdOther = getAnswerAsString();

               answer = SinglechoiceAnswer.withOther(cmdOther);
               break;
           }

           if (choices.contains(cmd)) {
               answer = SinglechoiceAnswer.withChoice(cmd) ;
               break ;
           }

           msg = new StringBuilder("Sorry, I don't follow. ") ;
       }

       if (answer != null)
           response.setAnswer(question, answer) ;

   }


   private void handleMultichoice(MultichoiceQuestion question, SurveyState state, Response response) {

       StringBuilder msg = new StringBuilder() ;
       msg.append(replacePlaceholders(question.getQuestion(), response)) ;

       LinkedHashSet<String> choices = new LinkedHashSet<String>() ;
       for (Choice choice:question.getChoices())
           choices.add(StringNormalizer.normalize(choice.getName())) ;

       if (question.getAllowOther())
           choices.add("other") ;

       MultichoiceAnswer answer = null ;
       while (answer == null) {

           msg.append("\nPlease respond with one or more of " + getChoicesPrompt(question.getChoices(), question.getAllowOther()) + " separated by commas") ;

           if (question.isOptional())
               msg.append(" or " + getSkipPrompt() + " to skip") ;

           sendMessage(msg.toString()) ;

           String cmd = getAnswerAsString() ;

           if (StringNormalizer.normalize(cmd).length() == 0 && question.isOptional())
               break ;

           List<String> validChoices = new ArrayList<String>() ;
           String invalidChoice = null ;
           String otherDescription = null ;

           for (String cmdChoice : cmd.split(",")) {

               cmdChoice = StringNormalizer.normalize(cmdChoice) ;

               if (question.getAllowOther() && cmdChoice.equals("other")) {
                   sendMessage("What do you mean by \"other\"?");
                   otherDescription = getAnswerAsString();
                   continue ;
               }

               if (choices.contains(cmdChoice)) {
                   validChoices.add(cmdChoice);
                   continue;
               }

               invalidChoice = cmdChoice ;
               break ;
           }

           if (invalidChoice != null) {

               msg = new StringBuilder("Sorry, \"" + invalidChoice + "\" is not a valid choice") ;
               continue ;
           }

           if (otherDescription != null) {
               answer = MultichoiceAnswer.withChoicesAndOther(validChoices, otherDescription);
               break;
           }

           if (validChoices.isEmpty()) {
               msg = new StringBuilder("Sorry, you must enter at least one valid choice") ;
               continue ;
           }

           answer = MultichoiceAnswer.withChoices(validChoices) ;
       }

       if (answer != null)
           response.setAnswer(question, answer) ;

   }

   private void handleFreetext(FreetextQuestion question, SurveyState state, Response response) {

       StringBuilder msg = new StringBuilder() ;
       msg.append(replacePlaceholders(question.getQuestion(), response)) ;

       FreetextAnswer answer = null ;

       while (answer == null) {

           if (question.isOptional())
               msg.append("\n" + getSkipPrompt() + " to skip") ;

           sendMessage(msg.toString());

           String cmd = getAnswerAsString() ;

           if (StringNormalizer.normalize(cmd).length() > 0) {
               answer = FreetextAnswer.withText(cmd) ;
               break ;
           }

           if (question.isOptional())
               break ;

           msg = new StringBuilder("Sorry, I need an answer for that.") ;
       }

       if (answer != null)
           response.setAnswer(question, answer) ;

   }

   private void handleNumeric(NumericQuestion question, SurveyState state, Response response) {

       StringBuilder msg = new StringBuilder() ;
       msg.append(replacePlaceholders(question.getQuestion(), response)) ;

       NumericAnswer answer = null ;

       while (answer == null) {

           msg.append("\nPlease respond with a number") ;
           if (question.isOptional())
               msg.append(" or " + getSkipPrompt() + " to skip") ;

           //TODO: prompt for range restrictions

           sendMessage(msg.toString());

           String answerStr = getAnswerAsString() ;

           if (StringNormalizer.normalize(answerStr).length() == 0 && question.isOptional())
               break ;

           try {
               Double number = Double.parseDouble(answerStr);

               answer = NumericAnswer.withNumber(number) ;
               break ;
           } catch (NumberFormatException e) {
               msg = new StringBuilder("Sorry, I don't follow") ;
           }
       }

       if (answer != null)
           response.setAnswer(question, answer) ;
   }



   private String getChoicesPrompt(Collection<Choice> choices, boolean allowOther) {


       List<String> choicesStr = new ArrayList<String>() ;
       for (Choice choice:choices) {
           choicesStr.add(choice.getName()) ;
       }

       if (allowOther)
           choicesStr.add("other") ;

       StringBuilder prompt = new StringBuilder() ;

       int index = 0 ;
       for (String choice:choicesStr) {
           if (index == choices.size() -1)
               prompt.append(" or ") ;
           else if (index > 0)
               prompt.append(", ") ;

           prompt.append("\"" + choice + "\"") ;
           index++ ;
       }

       return prompt.toString() ;
   }

   private String replacePlaceholders(String message, Response response) {

       StringBuilder sb = new StringBuilder() ;

       Matcher m = Field.placeHolderPattern.matcher(message) ;

       int index = 0 ;

       while (m.find()) {

           sb.append(message.substring(index, m.start())) ;

           String questionId = m.group(1) ;

           Answer<?> answer = response.getAnswer(questionId) ;

           if (answer == null) {
               sb.append("{unansweredQuestion:" + questionId + "}") ;
               logger.warn("Could not find answer for placeholder " + questionId) ;
           } else {
               sb.append(answer.toString()) ;
           }

           index = m.end() ;
       }

       sb.append(message.substring(index)) ;

       return sb.toString() ;
   }

}
