package org.poscomp.ask.askers;

import static org.poscomp.ask.model.logic.Actions.showField;
import static org.poscomp.ask.model.logic.Actions.skipPage;
import static org.poscomp.ask.model.logic.Actions.skipToEnd;
import static org.poscomp.ask.model.logic.Actions.skipToPage;
import static org.poscomp.ask.model.logic.Triggers.numericTrigger;
import static org.poscomp.ask.model.logic.Triggers.singlechoiceTrigger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;


import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.error.InvalidRule;
import org.poscomp.ask.error.InvalidSurvey;
import org.poscomp.ask.model.Response;
import org.poscomp.ask.model.Schema;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.answers.NumericAnswer;
import org.poscomp.ask.model.answers.SinglechoiceAnswer;
import org.poscomp.ask.model.fields.FreetextQuestion;
import org.poscomp.ask.model.fields.Instruction;
import org.poscomp.ask.model.fields.NumericQuestion;
import org.poscomp.ask.model.fields.PageBreak;
import org.poscomp.ask.model.fields.SinglechoiceQuestion;
import org.poscomp.ask.model.logic.FieldRule;
import org.poscomp.ask.model.logic.PageRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by dmilne on 16/12/14.
 */
public class CommandLineAsker extends SerialAsker {

    private static Logger logger = LoggerFactory.getLogger(CommandLineAsker.class) ;

    public CommandLineAsker(Survey survey) {
        super(survey) ;
    }

    @Override
    public void sendMessage(String message) {
        System.out.println(message) ;
    }

    @Override
    public String getAnswerAsString() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            return reader.readLine() ;

        } catch (IOException e) {
            logger.warn("Could not fetch response from command line", e) ;
            return "" ;
        }
    }

    @Override
    public String getSkipPrompt() {
        return "press ENTER" ;
    }


    public static void main(String args[]) throws InvalidField, InvalidRule, InvalidSurvey {


//        Survey survey = buildFitnessMonitorSurvey() ;
        Schema schema = getMorningSchema() ;


        CommandLineAsker asker = new CommandLineAsker(schema.getSurvey()) ;

       /* Response response = new Response(survey, "blah")
                .setAnswer("qStreak", NumericAnswer.withNumber(5))
                .setAnswer("qDaysTired", NumericAnswer.withNumber(3))
                .setAnswer("qSickStreak", NumericAnswer.withNumber(2))
                .setAnswer("qDaysBusy", NumericAnswer.withNumber(3))
                .setAnswer("qGender", SinglechoiceAnswer.withChoice("Female"))
                .setAnswer("qStepsYesterday", NumericAnswer.withNumber(9845))
                .setAnswer("qDifficulty", SinglechoiceAnswer.withChoice("Hard")) ;
*/

        Response response = asker.buildResponse(schema.getResponse()) ;

        System.out.println("Your response was ") ;
        for(Map.Entry<String,Answer<?>> e:response.getAnswers().entrySet()) {
            System.out.println("  " + e.getKey() + ": " + e.getValue()) ;
        }
    }




    private static Survey buildSimpleTestSurvey() throws InvalidRule, InvalidField, InvalidSurvey {

        return  new Survey.Builder("Simple test survey")
                .addField(
                        new FreetextQuestion.Builder()
                                .setId("qName")
                                .setQuestion("What is your name?")
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qHappy")
                                .setQuestion("Are you happy?")
                                .setChoices("Yes", "No")
                                .build()
                ).addField(
                        new Instruction.Builder()
                                .setId("iGood")
                                .setText("Good for you!")
                                .build()
                ).addField(
                        new FreetextQuestion.Builder()
                                .setId("qBadReason")
                                .setQuestion("Oh no! How come?")
                                .setOptional()
                                .setLength(FreetextQuestion.Length.LONG)
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qHappy").is("Yes"))
                                .addAction(showField("iGood"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qHappy").is("No"))
                                .addAction(showField("qBadReason"))
                                .build()
                ).build() ;
    }

    public static Survey buildGenderAndParenthoodSurvey() throws InvalidField, InvalidRule, InvalidSurvey {

        return new Survey.Builder("Gender and Parenthood")
                .addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qGender")
                                .setQuestion("Gender")
                                .setChoices("Male", "Female", "I'd rather not say")
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qDependents")
                                .setQuestion("Number of dependents")
                                .build()
                )
                .addField(
                        new PageBreak.Builder()
                                .setId("pMales")
                                .setTitle("Men")
                                .build()
                ).addField(
                        new Instruction.Builder()
                                .setId("iMales")
                                .setText("This is where we ask questions for all men")
                                .build()
                ).addField(
                        new PageBreak.Builder()
                                .setId("pFathers")
                                .setTitle("Fathers")
                                .build()
                ).addField(
                        new Instruction.Builder()
                                .setId("iFathers")
                                .setText("This is where we ask questions for all fathers")
                                .build()
                )
                .addField(
                        new PageBreak.Builder()
                                .setId("pFemales")
                                .setTitle("Women")
                                .build()
                ).addField(
                        new Instruction.Builder()
                                .setId("iFemales")
                                .setText("This is where we ask questions for all women")
                                .build()
                ).addField(
                        new PageBreak.Builder()
                                .setId("pMothers")
                                .setTitle("Mothers")
                                .build()
                ).addField(
                        new Instruction.Builder()
                                .setId("iMothers")
                                .setText("This is where we ask questions for all mothers")
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qGender").is("Male"))
                                .addTrigger(singlechoiceTrigger("qGender").is("Female"))
                                .withOrOperator()
                                .addAction(showField("qDependents"))
                                .build()
                ).addPageRule(
                        new PageRule.Builder()
                                .addTrigger(singlechoiceTrigger("qGender").is("I'd rather not say"))
                                .addAction(skipToEnd())
                                .build()
                ).addPageRule(
                        new PageRule.Builder()
                                .addTrigger(singlechoiceTrigger("qGender").is("Male"))
                                .addAction(skipPage("pFemales"))
                                .addAction(skipPage("pMothers"))
                                .build()
                ).addPageRule(
                        new PageRule.Builder()
                                .addTrigger(singlechoiceTrigger("qGender").is("Female"))
                                .addAction(skipToPage("pFemales"))
                                .build()
                ).addPageRule(
                        new PageRule.Builder()
                                .addTrigger(numericTrigger("qDependents").equals(0))
                                .addAction(skipPage("pMothers"))
                                .addAction(skipPage("pFathers"))
                                .build()
                ).build() ;

    }

    public static Survey buildFitnessMonitorSurvey() throws InvalidRule, InvalidField, InvalidSurvey {


        return new Survey.Builder("FitCoach morning (adaptive)")
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qStreak")
                                .setQuestion("How many days in a row have you exercised")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qDaysTired")
                                .setQuestion("How many days in the last week have you been too tired?")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qSickStreak")
                                .setQuestion("How many days in a row have you been too sick?")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qDaysBusy")
                                .setQuestion("How many days in the last week have you been too busy?")
                                .hide()
                                .build()
                )
                .addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qGender")
                                .setQuestion("What gender are you?")
                                .setChoices("Male", "Female")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qStepsYesterday")
                                .setQuestion("How many steps did you make yesterday?")
                                .hide()
                                .build()
                )
                .addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qDifficulty")
                                .setQuestion("How difficult was your exercise yesterday?")
                                .setChoices("Easy", "Moderate", "Hard")
                                .hide()
                                .build()
                )
                .addField(
                        new Instruction.Builder()
                                .setId("iConsistencyMale")
                                .setText("Good morning, Mr. Consistent :)")
                                .build()
                ).addField(
                new Instruction.Builder()
                        .setId("iConsistencyFemale")
                        .setText("Good morning, Miss Consistent :)")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qExerciseToday")
                        .setQuestion("Do you feel like exercising today?")
                        .setChoices("Yes","No")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qNoExerciseReason")
                        .setQuestion("Oh no, how come?")
                        .setChoices("Tired","Sick","Busy","Lazy")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qNoExerciseReasonWithStreak")
                        .setQuestion("Oh no, you'll ruin your streak! How come?")
                        .setChoices("Tired", "Sick", "Busy", "Lazy")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qTiredConfirm")
                        .setQuestion("Some Light exercise will help you sleep better tonight. Are you sure you can't fit any in today?")
                        .setChoices("No way", "Ok")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qTiredConsistentConfirm")
                        .setQuestion("You've used that excuse a lot, sleepyhead. Come on, lets go for a walk today.")
                        .setChoices("No way", "Ok")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iTiredConfirmed")
                        .setText("Ok, fine. Get to bed early tonight, ok? We're going for a walk tomorrow.")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iSickConfirmed")
                        .setText("Ok, you take it easy today. I'll check in with you tomorrow")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iSickConsistentConfirmed")
                        .setText("Sick [[qSickStreak]] days in a row? This is pretty serious! See a doctor or something, ok? I'll check in on you tomorrow")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qBusyConfirm")
                        .setQuestion("You sure you can't fit in a walk, or take your work with you?")
                        .setChoices("No way","Ok")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qBusyConsistentConfirm")
                        .setQuestion("You've used that excuse [[qDaysBusy]] times in the last week. Sure you can't make time for me?")
                        .setChoices("No way", "Ok")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iBusyConfirmed")
                        .setText("Ok, but set some time aside for me tomorrow. I'm feeling neglected =(")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iLazy")
                        .setText("Oh, come on. That's no excuse")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iGoalEasy")
                        .setText("All right! Yesterday we did [[qStepsYesterday]] steps, piece of cake. Lets aim higher today (and avoid the cake)")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iGoalModerate")
                        .setText("All right! Yesterday we did [[qStepsYesterday]] steps. A bit more today?")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iGoalHard")
                        .setText("All right! Yesterday we did [[qStepsYesterday]] steps. Try for that again, but no worries if we do a bit less")
                        .build()
        )





                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(numericTrigger("qStreak").gt(2))
                                .addTrigger(singlechoiceTrigger("qGender").is("Male"))
                                .withAndOperator()
                                .addAction(showField("iConsistencyMale"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(numericTrigger("qStreak").gt(2))
                        .addTrigger(singlechoiceTrigger("qGender").is("Female"))
                        .withAndOperator()
                        .addAction(showField("iConsistencyFemale"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qExerciseToday").is("No"))
                        .addTrigger(numericTrigger("qStreak").lt(3))
                        .withAndOperator()
                        .addAction(showField("qNoExerciseReason"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qExerciseToday").is("No"))
                        .addTrigger(numericTrigger("qStreak").gt(2))
                        .withAndOperator()
                        .addAction(showField("qNoExerciseReasonWithStreak"))
                        .build()
        )


                //no, without streak

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Tired"))
                                .addTrigger(numericTrigger("qDaysTired").lt(3))
                                .withAndOperator()
                                .addAction(showField("qTiredConfirm"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Tired"))
                        .addTrigger(numericTrigger("qDaysTired").gt(2))
                        .withAndOperator()
                        .addAction(showField("qTiredConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").lt(2))
                        .withAndOperator()
                        .addAction(showField("iSickConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").gt(1))
                        .withAndOperator()
                        .addAction(showField("iSickConsistentConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").lt(3))
                        .withAndOperator()
                        .addAction(showField("qBusyConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").gt(2))
                        .withAndOperator()
                        .addAction(showField("qBusyConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
                        .addAction(showField("iLazy"))
                        .build()
        )


                //no, with streak

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Tired"))
                                .addTrigger(numericTrigger("qDaysTired").lt(3))
                                .withAndOperator()
                                .addAction(showField("qTiredConfirm"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Tired"))
                        .addTrigger(numericTrigger("qDaysTired").gt(2))
                        .withAndOperator()
                        .addAction(showField("qTiredConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").lt(2))
                        .withAndOperator()
                        .addAction(showField("iSickConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").gt(1))
                        .withAndOperator()
                        .addAction(showField("iSickConsistentConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").lt(3))
                        .withAndOperator()
                        .addAction(showField("qBusyConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").gt(2))
                        .withAndOperator()
                        .addAction(showField("qBusyConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
                        .addAction(showField("iLazy"))
                        .build()
        )




                //no confirmations

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qTiredConfirm").is("No way"))
                                .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("No way"))
                                .withOrOperator()
                                .addAction(showField("iTiredConfirmed"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qBusyConfirm").is("No way"))
                        .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("No way"))
                        .withOrOperator()
                        .addAction(showField("iBusyConfirmed"))
                        .build()
        )



                //yes, easy

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                                .withAndOperator()
                                .addAction(showField("iGoalEasy"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                        .withAndOperator()
                        .addAction(showField("iGoalEasy"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                        .withAndOperator()
                        .addAction(showField("iGoalEasy"))
                        .build()
        )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                                .withAndOperator()
                                .addAction(showField("iGoalEasy"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                                .withAndOperator()
                                .addAction(showField("iGoalEasy"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                                .withAndOperator()
                                .addAction(showField("iGoalEasy"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                                .withAndOperator()
                                .addAction(showField("iGoalEasy"))
                                .build()
                )

                        //yes, moderate

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                                .withAndOperator()
                                .addAction(showField("iGoalModerate"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                        .withAndOperator()
                        .addAction(showField("iGoalModerate"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                        .withAndOperator()
                        .addAction(showField("iGoalModerate"))
                        .build()
        )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                                .withAndOperator()
                                .addAction(showField("iGoalModerate"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                                .withAndOperator()
                                .addAction(showField("iGoalModerate"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                                .withAndOperator()
                                .addAction(showField("iGoalModerate"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                                .withAndOperator()
                                .addAction(showField("iGoalModerate"))
                                .build()
                )

                        //yes, hard

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                                .withAndOperator()
                                .addAction(showField("iGoalHard"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                        .withAndOperator()
                        .addAction(showField("iGoalHard"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                        .withAndOperator()
                        .addAction(showField("iGoalHard"))
                        .build()
        )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                                .withAndOperator()
                                .addAction(showField("iGoalHard"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                                .withAndOperator()
                                .addAction(showField("iGoalHard"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                                .withAndOperator()
                                .addAction(showField("iGoalHard"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                                .withAndOperator()
                                .addAction(showField("iGoalHard"))
                                .build()
                )
                .build() ;

    }
    
    public static Schema getMorningSchema() throws InvalidRule, InvalidField, InvalidSurvey {


        Survey survey= new Survey.Builder("FitCoach morning (adaptive)")
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qStreak")
                                .setQuestion("How many days in a row have you exercised")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qDaysTired")
                                .setQuestion("How many days in the last week have you been too tired?")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qSickStreak")
                                .setQuestion("How many days in a row have you been too sick?")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qDaysBusy")
                                .setQuestion("How many days in the last week have you been too busy?")
                                .hide()
                                .build()
                )
                .addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qGender")
                                .setQuestion("What gender are you?")
                                .setChoices("Male", "Female")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qStepsYesterday")
                                .setQuestion("How many steps did you make yesterday?")
                                .hide()
                                .build()
                )
                .addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qDifficulty")
                                .setQuestion("How difficult was your exercise yesterday?")
                                .setChoices("Easy", "Moderate", "Hard", "NoExcercise")
                                .hide()
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qGoal")
                                .setQuestion("What is your current goals(steps)?")
                                .hide()
                                .build()
                )
                
                
                
                .addField(
                        new Instruction.Builder()
                                .setId("iConsistencyMale")
                                .setText("Good morning, Mr. Consistent :)")
                                .build()
                ).addField(
                new Instruction.Builder()
                        .setId("iConsistencyFemale")
                        .setText("Good morning, Miss Consistent :)")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qExerciseToday")
                        .setQuestion("Do you feel like exercising today?")
                        .setChoices("Yes","No")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qNoExerciseReason")
                        .setQuestion("Oh no, how come?")
                        .setChoices("Tired","Sick","Busy","Lazy")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qNoExerciseReasonWithStreak")
                        .setQuestion("Oh no, you'll ruin your streak! How come?")
                        .setChoices("Tired", "Sick", "Busy", "Lazy")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qTiredConfirm")
                        .setQuestion("Some Light exercise will help you sleep better tonight. Are you sure you can't fit any in today?")
                        .setChoices("No way", "Ok")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qTiredConsistentConfirm")
                        .setQuestion("You've used that excuse a lot, sleepyhead. Come on, lets go for a walk today.")
                        .setChoices("No way", "Ok")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iTiredConfirmed")
                        .setText("Ok, fine. Get to bed early tonight, ok? We're going for a walk tomorrow.")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iSickConfirmed")
                        .setText("Ok, you take it easy today. I'll check in with you tomorrow")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iSickConsistentConfirmed")
                        .setText("Sick [[qSickStreak]] days in a row? This is pretty serious! See a doctor or something, ok? I'll check in on you tomorrow")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qBusyConfirm")
                        .setQuestion("You sure you can't fit in a walk, or take your work with you?")
                        .setChoices("No way","Ok")
                        .build()
        ).addField(
                new SinglechoiceQuestion.Builder()
                        .setId("qBusyConsistentConfirm")
                        .setQuestion("You've used that excuse [[qDaysBusy]] times in the last week. Sure you can't make time for me?")
                        .setChoices("No way", "Ok")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iBusyConfirmed")
                        .setText("Ok, but set some time aside for me tomorrow. I'm feeling neglected =(")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iLazy")
                        .setText("Oh, come on. That's no excuse")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iGoalEasy")
                        .setText("All right! Yesterday we did [[qStepsYesterday]] steps, piece of cake. Lets aim higher today (and avoid the cake)")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iGoalModerate")
                        .setText("All right! Yesterday we did [[qStepsYesterday]] steps. A bit more today?")
                        .build()
        ).addField(
                new Instruction.Builder()
                        .setId("iGoalHard")
                        .setText("All right! Yesterday we did [[qStepsYesterday]] steps. Try for that again, but no worries if we do a bit less")
                        .build()
         ).addField(
	                new Instruction.Builder()
                    .setId("iGoalNoExcercise")
                    .setText("All right! As you didn't excercise yesterday. Lets try to take [[qGoal]] steps today, but no worries if we do a bit less")
                    .build()
         )




                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(numericTrigger("qStreak").gt(2))
                                .addTrigger(singlechoiceTrigger("qGender").is("Male"))
                                .withAndOperator()
                                .addAction(showField("iConsistencyMale"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(numericTrigger("qStreak").gt(2))
                        .addTrigger(singlechoiceTrigger("qGender").is("Female"))
                        .withAndOperator()
                        .addAction(showField("iConsistencyFemale"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qExerciseToday").is("No"))
                        .addTrigger(numericTrigger("qStreak").lt(3))
                        .withAndOperator()
                        .addAction(showField("qNoExerciseReason"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qExerciseToday").is("No"))
                        .addTrigger(numericTrigger("qStreak").gt(2))
                        .withAndOperator()
                        .addAction(showField("qNoExerciseReasonWithStreak"))
                        .build()
        )


                //no, without streak

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Tired"))
                                .addTrigger(numericTrigger("qDaysTired").lt(3))
                                .withAndOperator()
                                .addAction(showField("qTiredConfirm"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Tired"))
                        .addTrigger(numericTrigger("qDaysTired").gt(2))
                        .withAndOperator()
                        .addAction(showField("qTiredConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").lt(2))
                        .withAndOperator()
                        .addAction(showField("iSickConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").gt(1))
                        .withAndOperator()
                        .addAction(showField("iSickConsistentConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").lt(3))
                        .withAndOperator()
                        .addAction(showField("qBusyConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").gt(2))
                        .withAndOperator()
                        .addAction(showField("qBusyConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
                        .addAction(showField("iLazy"))
                        .build()
        )


                //no, with streak

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Tired"))
                                .addTrigger(numericTrigger("qDaysTired").lt(3))
                                .withAndOperator()
                                .addAction(showField("qTiredConfirm"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Tired"))
                        .addTrigger(numericTrigger("qDaysTired").gt(2))
                        .withAndOperator()
                        .addAction(showField("qTiredConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").lt(2))
                        .withAndOperator()
                        .addAction(showField("iSickConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Sick"))
                        .addTrigger(numericTrigger("qSickStreak").gt(1))
                        .withAndOperator()
                        .addAction(showField("iSickConsistentConfirmed"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").lt(3))
                        .withAndOperator()
                        .addAction(showField("qBusyConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Busy"))
                        .addTrigger(numericTrigger("qDaysBusy").gt(2))
                        .withAndOperator()
                        .addAction(showField("qBusyConsistentConfirm"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
                        .addAction(showField("iLazy"))
                        .build()
        )




                //no confirmations

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qTiredConfirm").is("No way"))
                                .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("No way"))
                                .withOrOperator()
                                .addAction(showField("iTiredConfirmed"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qBusyConfirm").is("No way"))
                        .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("No way"))
                        .withOrOperator()
                        .addAction(showField("iBusyConfirmed"))
                        .build()
        )



                //yes, easy

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                                .withAndOperator()
                                .addAction(showField("iGoalEasy"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                        .withAndOperator()
                        .addAction(showField("iGoalEasy"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                        .withAndOperator()
                        .addAction(showField("iGoalEasy"))
                        .build()
        )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                                .withAndOperator()
                                .addAction(showField("iGoalEasy"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                                .withAndOperator()
                                .addAction(showField("iGoalEasy"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                                .withAndOperator()
                                .addAction(showField("iGoalEasy"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Easy"))
                                .withAndOperator()
                                .addAction(showField("iGoalEasy"))
                                .build()
                )

                        //yes, moderate

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                                .withAndOperator()
                                .addAction(showField("iGoalModerate"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                        .withAndOperator()
                        .addAction(showField("iGoalModerate"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                        .withAndOperator()
                        .addAction(showField("iGoalModerate"))
                        .build()
        )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                                .withAndOperator()
                                .addAction(showField("iGoalModerate"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                                .withAndOperator()
                                .addAction(showField("iGoalModerate"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                                .withAndOperator()
                                .addAction(showField("iGoalModerate"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Moderate"))
                                .withAndOperator()
                                .addAction(showField("iGoalModerate"))
                                .build()
                )

                        //yes, hard

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                                .withAndOperator()
                                .addAction(showField("iGoalHard"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                        .withAndOperator()
                        .addAction(showField("iGoalHard"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                        .withAndOperator()
                        .addAction(showField("iGoalHard"))
                        .build()
        )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                                .withAndOperator()
                                .addAction(showField("iGoalHard"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                                .withAndOperator()
                                .addAction(showField("iGoalHard"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                                .withAndOperator()
                                .addAction(showField("iGoalHard"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("Hard"))
                                .withAndOperator()
                                .addAction(showField("iGoalHard"))
                                .build()
                )
                
                         //yes, NoExcercise Yesterday 

                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qExerciseToday").is("Yes"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
                                .withAndOperator()
                                .addAction(showField("iGoalNoExcercise"))
                                .build()
                ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
                        .withAndOperator()
                        .addAction(showField("iGoalNoExcercise"))
                        .build()
        ).addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger("qTiredConsistentConfirm").is("Ok"))
                        .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
                        .withAndOperator()
                        .addAction(showField("iGoalNoExcercise"))
                        .build()
        )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
                                .withAndOperator()
                                .addAction(showField("iGoalNoExcercise"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qBusyConsistentConfirm").is("Ok"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
                                .withAndOperator()
                                .addAction(showField("iGoalNoExcercise"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReason").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
                                .withAndOperator()
                                .addAction(showField("iGoalNoExcercise"))
                                .build()
                )
                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qNoExerciseReasonWithStreak").is("Lazy"))
                                .addTrigger(singlechoiceTrigger("qDifficulty").is("NoExcercise"))
                                .withAndOperator()
                                .addAction(showField("iGoalNoExcercise"))
                                .build()
                )
                
                .build() ;
        

  //      survey.setUsername(user.getUsername());
//		LocalDateTime date=new LocalDateTime();
//		survey.setExpiry(date.plusHours(7).toDate());
		survey.setNeedResponse(true);
//		survey.setMobileNumber(user.getMobileNnumber());
		survey.setSurvey_id(1);
        
		
		// Create response for hidden fields
        
		//date.toDate();
		
		
		
        Response response = new Response(survey, "blah")
        .setAnswer("qStreak", NumericAnswer.withNumber(0))
        .setAnswer("qDaysTired", NumericAnswer.withNumber(0))
        .setAnswer("qSickStreak", NumericAnswer.withNumber(0))
        .setAnswer("qDaysBusy", NumericAnswer.withNumber(0))
        .setAnswer("qGender", SinglechoiceAnswer.withChoice("male"))
        .setAnswer("qStepsYesterday", NumericAnswer.withNumber(1000))
        .setAnswer("qDifficulty", SinglechoiceAnswer.withChoice("noexcercise"))
        .setAnswer("qGoal", NumericAnswer.withNumber(10000));
        response.setSurvey_id(1);
       
        
        Schema schema = new Schema();
        schema.setSurvey(survey);
        schema.setResponse(response);
        return schema;

    }

}
