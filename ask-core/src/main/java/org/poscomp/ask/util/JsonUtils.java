package org.poscomp.ask.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.bson.types.ObjectId;
import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.logic.Trigger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by dmilne on 18/12/14.
 */
public class JsonUtils {

    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z") ;


    public static ObjectMapper getObjectMapper() {

        SimpleModule module = new SimpleModule() ;
        module.addDeserializer(Trigger.class, new TriggerDeserializer());
        module.addDeserializer(Answer.class, new AnswerDeserializer()) ;
        module.addDeserializer(ObjectId.class, new IdFormatting.IdDeserializer()) ;
        module.addSerializer(ObjectId.class, new IdFormatting.IdSerializer()) ;

        ObjectMapper mapper = new ObjectMapper() ;
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(module) ;

        mapper.setDateFormat(dateFormat) ;

        return mapper ;
    }
}
