package org.poscomp.ask.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by dmilne on 16/12/14.
 */
public class StringNormalizer {

    public static String normalize(String value) {

        if (value == null)
            return null ;

        String normalized = value.replaceAll("\\p{Punct}", " ") ;
        normalized = normalized.replaceAll("\\s+", " ") ;
        normalized = normalized.trim().toLowerCase() ;

        return normalized ;
    }

    public static List<String> normalize(Collection<String> values) {

        ArrayList<String> normalizedValues = new ArrayList<String>() ;
        for (String value:values)
            normalizedValues.add(normalize(value)) ;

        return normalizedValues ;
    }
}
