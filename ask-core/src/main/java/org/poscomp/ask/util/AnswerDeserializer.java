package org.poscomp.ask.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.poscomp.ask.model.answers.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


/**
 * Created by dmilne on 18/12/14.
 */
public class AnswerDeserializer extends StdDeserializer<Answer<?>> {

    private static Logger logger = LoggerFactory.getLogger(AnswerDeserializer.class) ;

    public AnswerDeserializer()
    {
        super(Answer.class);
    }


    @Override
    public Answer deserialize(
            JsonParser parser, DeserializationContext context
    ) throws IOException
    {
        ObjectMapper mapper = (ObjectMapper) parser.getCodec();
        ObjectNode node = mapper.readTree(parser);

        if (node.has("text"))
            return mapper.treeToValue(node, FreetextAnswer.class) ;

        if (node.has("number"))
            return mapper.treeToValue(node, NumericAnswer.class) ;

        if (node.has("mood"))
            return mapper.treeToValue(node, MoodAnswer.class) ;

        if (node.has("choice"))
            return mapper.treeToValue(node, SinglechoiceAnswer.class) ;

        if (node.has("choices"))
            mapper.treeToValue(node, MultichoiceAnswer.class) ;

        String json = mapper.writeValueAsString(node) ;
        logger.warn("Cannot identify answer type of" + json) ;

        return null ;
    }
}
