package org.poscomp.ask.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.poscomp.ask.model.logic.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Created by dmilne on 18/12/14.
 */
public class TriggerDeserializer extends StdDeserializer<Trigger<?>>
{
    private static Logger logger = LoggerFactory.getLogger(TriggerDeserializer.class) ;

    public TriggerDeserializer()
    {
        super(Trigger.class);
    }


    @Override
    public Trigger deserialize(
            JsonParser parser, DeserializationContext context
    )
            throws IOException, JsonProcessingException
    {
        ObjectMapper mapper = (ObjectMapper) parser.getCodec();
        ObjectNode node = mapper.readTree(parser);

        Class<? extends Trigger> triggerClass = null;


        if (node.has("text"))
            return mapper.treeToValue(node, FreetextTrigger.class) ;

        if (node.has("number"))
            return mapper.treeToValue(node, NumericTrigger.class) ;

        if (node.has("mood"))
            return mapper.treeToValue(node, MoodTrigger.class) ;

        if (node.has("choice")) {

            String condition = node.get("condition").asText() ;

            MultichoiceTrigger.Condition mCondition = getMultichoiceCondition(condition) ;
            if (mCondition != null)
                return mapper.treeToValue(node, MultichoiceTrigger.class) ;

            SinglechoiceTrigger.Condition sCondition = getSinglechoiceCondition(condition) ;
            if (sCondition != null)
                return mapper.treeToValue(node, SinglechoiceTrigger.class) ;

            throw new IOException(condition + " is not a multichoice or singlechoice condition") ;
        }

        String json = mapper.writeValueAsString(node) ;
        logger.warn("Cannot identify trigger type of" + json) ;

        return null ;
    }


    private MultichoiceTrigger.Condition getMultichoiceCondition(String condition) {

        try {
            return MultichoiceTrigger.Condition.valueOf(condition);
        } catch (Exception e) {
            return null ;
        }
    }

    private SinglechoiceTrigger.Condition getSinglechoiceCondition(String condition) {

        try {
            return SinglechoiceTrigger.Condition.valueOf(condition);
        } catch (Exception e) {
            return null ;
        }
    }
}