package org.poscomp.ask.model.answers;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import org.bson.types.ObjectId;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.fields.Field;
import org.poscomp.ask.model.fields.Question;
import org.poscomp.ask.util.AnswerDeserializer;

import java.util.Date;

/**
 * An answer to a question. These are stored as individual rows in db
 * (rather than part of a larger Response object) so we can easily do queries
 * for individual questions across multiple responses.
 */

public abstract class Answer<Q extends Question> {

    public Answer() {

    }

    @JsonIgnore
    public abstract boolean isAnswered() ;

}
