package org.poscomp.ask.model.answers;

import org.poscomp.ask.model.data.Mood;
import org.poscomp.ask.model.fields.MoodQuestion;

/**
 * Created by dmilne on 25/11/14.
 */
public class MoodAnswer extends Answer<MoodQuestion> {

    public Mood mood ;

    private MoodAnswer() {

    }

    @Override
    public boolean isAnswered() {
        return mood != null && mood.getName() != null ;
    }

    public Mood getMood() {
        return mood;
    }

    public String toString() {
        if (mood != null)
            return mood.getName() ;

        return "none" ;
    }


    public static MoodAnswer withMood(String name, double valence, double arousal) {

        return withMood(new Mood(name, valence, arousal)) ;
    }

    public static MoodAnswer withMood(Mood mood) {

        MoodAnswer answer = new MoodAnswer() ;
        answer.mood = mood ;
        return answer ;
    }
}
