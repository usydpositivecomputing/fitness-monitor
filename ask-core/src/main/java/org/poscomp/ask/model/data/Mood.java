package org.poscomp.ask.model.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by dmilne on 26/11/14.
 */
public class Mood {

    public enum Quadrant {ne,se,sw,nw} ;

    private static final double equalityThreshold = 0.05 ;

    private static final double nearnessThreshold = 0.25 ;

    private String name ;

    private double valence ;

    private double arousal ;

    private Mood() {

    }

    public Mood(String name, double valence, double arousal) {
        this.name = name;
        this.valence = valence;
        this.arousal = arousal;
    }

    public String getName() {
        return name;
    }

    public double getValence() {
        return valence;
    }

    public double getArousal() {
        return arousal;
    }

    @JsonIgnore
    public Quadrant getQuadrant() {

        if (arousal >= 0) {
            if (valence >= 0)
                return Quadrant.ne ;
            else
                return Quadrant.nw ;

        } else {
            if (valence >= 0)
                return Quadrant.se ;
            else
                return Quadrant.sw ;
        }
    }

    public double getDistance(Mood mood) {

        return Math.sqrt(Math.pow(valence, mood.valence)+Math.pow(arousal, mood.arousal)) ;

    }

    public boolean equals(Object o) {
        return equals((Mood)o) ;
    }

    public boolean equals(Mood mood) {

        if (Math.abs(mood.valence - valence) > equalityThreshold)
            return false ;

        if (Math.abs(mood.arousal - arousal) > equalityThreshold)
            return false ;

        return true ;
    }

    public boolean isNear(Mood mood) {

        if (Math.abs(mood.valence - valence) > nearnessThreshold)
            return false ;

        if (Math.abs(mood.arousal - arousal) > nearnessThreshold)
            return false ;

        return true ;
    }


}
