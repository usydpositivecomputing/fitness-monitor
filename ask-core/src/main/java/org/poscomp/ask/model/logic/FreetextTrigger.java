package org.poscomp.ask.model.logic;

import org.poscomp.ask.error.InvalidTrigger;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.answers.FreetextAnswer;
import org.poscomp.ask.model.fields.Field;
import org.poscomp.ask.model.fields.FreetextQuestion;
import org.poscomp.ask.util.StringNormalizer;

/**
 * Created by dmilne on 26/11/14.
 */
public class FreetextTrigger extends Trigger<FreetextQuestion> {

    public enum Condition {is, isNot, contains, startsWith}

    private Condition condition ;

    private String text ;

    private FreetextTrigger() {

    }

    protected FreetextTrigger(FreetextQuestion question) {
        super(question) ;
    }

    protected FreetextTrigger(String questionId) {
        super(questionId) ;
    }

    public FreetextTrigger is(String text) {
        this.condition = Condition.is ;
        this.text = text ;
        return this ;
    }

    public FreetextTrigger isNot(String text) {
        this.condition = Condition.isNot ;
        this.text = text ;
        return this ;
    }

    public FreetextTrigger contains(String text) {
        this.condition = Condition.contains ;
        this.text = text ;
        return this ;
    }

    public FreetextTrigger notContains(String text) {
        this.condition = Condition.contains ;
        this.text = text ;
        return this ;
    }


    public Condition getCondition() {
        return condition;
    }

    public String getText() {
        return text;
    }

    public boolean firesOn(Answer<FreetextQuestion> answer) {

        if (answer == null)
            return false ;

        FreetextAnswer a = (FreetextAnswer)answer ;

        String answerText = StringNormalizer.normalize(a.getText()) ;
        String triggerText = StringNormalizer.normalize(text) ;

        switch (condition) {

            case is:
                return answerText.equals(triggerText) ;
            case isNot:
                return !answerText.equals(triggerText) ;
            case contains:
                return answerText.contains(triggerText) ;
            case startsWith:
                return answerText.startsWith(triggerText) ;
        }
        return false;
    }

    public void assertIsComplete(Survey survey) throws InvalidTrigger {

        super.assertIsComplete(survey); ;

        if (condition == null)
            throw new InvalidTrigger("no condition specified") ;

        if (text == null)
            throw new InvalidTrigger("no text specified") ;

        if (survey == null)
            return ;

        Field field = survey.getField(getQuestionId()) ;
        if (field.getType() != Field.Type.freetext)
            throw new InvalidTrigger("'" + field.getId() + "' is not a freetext question") ;
    }



    public String toString() {
        return condition + ":" + text ;
    }

}
