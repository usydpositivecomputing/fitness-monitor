package org.poscomp.ask.model.fields;

import org.poscomp.ask.error.InvalidField;

import java.util.ArrayList;

/**
 * Created by dmilne on 25/11/14.
 */
public class FreetextQuestion extends Question {

    public enum Length{SHORT, LONG} ;

    protected Length length ;

    private FreetextQuestion() {
        super(Type.freetext) ;
    }

    public Length getLength() {
        if (length == null)
            length = Length.SHORT ;

        return length ;
    }

    public static class Builder extends Question.Builder<FreetextQuestion> {

        private FreetextQuestion question ;

        @Override
        public FreetextQuestion f() {
            return question ;
        }

        public Builder() {
            super() ;
            question = new FreetextQuestion() ;
            question.length = Length.SHORT ;
        }

        public Builder setId(String id) {
            super.setId(id) ;
            return this ;
        }

        public Builder setQuestion(String question) {
            super.setQuestion(question) ;
            return this ;
        }

        public Builder setNotes(String notes) {
            super.setNotes(notes) ;
            return this ;
        }

        public Builder setTags(String... tags) {
            super.setTags(tags) ;
            return this ;
        }

        public Builder setOptional() {
            super.setOptional() ;
            return this ;
        }

        public Builder hide() {
            super.hide() ;
            return this ;
        }

        public Builder setLength(Length length) {
            f().length = length ;
            return this ;
        }

    }
}
