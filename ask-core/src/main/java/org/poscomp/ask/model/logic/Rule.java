package org.poscomp.ask.model.logic;

import org.apache.commons.lang3.StringUtils;
import org.poscomp.ask.error.InvalidAction;
import org.poscomp.ask.error.InvalidRule;
import org.poscomp.ask.error.InvalidTrigger;
import org.poscomp.ask.model.Survey;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dmilne on 4/12/14.
 */
public abstract class Rule<A extends Action> implements Cloneable {

    public enum Operator {and, or} ;

    protected List<Trigger> triggers ;

    protected Operator operator ;

    protected List<A> actions ;

    public List<Trigger> getTriggers() {

        if (triggers == null)
            triggers = new ArrayList<Trigger>() ;

        return Collections.unmodifiableList(triggers) ;
    }

    public List<A> getActions() {

        if (actions == null)
            actions = new ArrayList<A>() ;

        return Collections.unmodifiableList(actions) ;
    }

    public Operator getOperator() {
        return operator;
    }

    public void assertIsComplete(Survey survey) throws InvalidRule {

        if (triggers.isEmpty())
            throw new InvalidRule("No triggers specified") ;

        if (triggers.size() > 1 && operator == null)
            throw new InvalidRule("Rule involves multiple triggers, but no operator is specified") ;

        for (Trigger trigger:triggers) {
            try {
                trigger.assertIsComplete(survey);
            } catch (InvalidTrigger e) {
                throw new InvalidRule("Trigger " + trigger.toString() + " is incomplete", e) ;
            }
        }

        if (actions.isEmpty())
            throw new InvalidRule("No actions specified") ;

        for (Action action:actions) {
            try {
                action.assertIsComplete(survey);
            } catch (InvalidAction e) {
                throw new InvalidRule("Action " + action.toString() + " is incomplete", e) ;
            }
        }


    }


    public String toString() {

        StringBuilder sb = new StringBuilder("if ") ;

        if (operator == null || operator == Operator.or)
            sb.append("(" + StringUtils.join(triggers, " || ") + ")") ;
        else
            sb.append("(" + StringUtils.join(triggers, " && ") + ")") ;

        sb.append(" then ") ;

        sb.append("[" + StringUtils.join(actions, " + ") + "]") ;

        return sb.toString() ;
    }




    public static abstract class Builder<A extends Action, R extends Rule<A>> {

        public abstract R r();

        public Builder() {

        }

        public Builder addTrigger(Trigger trigger) {

            if (r().triggers == null)
                r().triggers = new ArrayList<Trigger>();

            r().triggers.add(trigger);
            return this;
        }

        public Builder addAction(A action) {

            if (r().actions == null)
                r().actions = new ArrayList<A>();

            r().actions.add(action);
            return this;
        }

        public Builder withAndOperator() {
            r().operator = Operator.and;
            return this;
        }

        public Builder withOrOperator() {
            r().operator = Operator.or;
            return this;
        }


        public R build() throws InvalidRule {

            r().assertIsComplete(null);

            return r();
        }
    }
}
