package org.poscomp.ask.model.logic;

import org.poscomp.ask.error.InvalidTrigger;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.answers.NumericAnswer;
import org.poscomp.ask.model.fields.Field;
import org.poscomp.ask.model.fields.NumericQuestion;


/**
 * Created by dmilne on 26/11/14.
 */
public class NumericTrigger extends Trigger<NumericQuestion> {

    private static final double equalityThreshold = 0.0000001 ;

    public enum Condition {equal, greaterThan, lessThan}

    private Condition condition ;

    private Double number ;

    private NumericTrigger() {

    }

    public Condition getCondition() {
        return condition;
    }

    public Double getNumber() {
        return number;
    }

    public NumericTrigger(NumericQuestion question) {
        super(question) ;
    }

    protected NumericTrigger(String questionId) {
        super(questionId) ;
    }

    public NumericTrigger equals(double number) {
        this.condition = Condition.equal ;
        this.number = number ;
        return this ;
    }

    public NumericTrigger gt(double number) {
        this.condition = Condition.greaterThan ;
        this.number = number ;
        return this ;
    }

    public NumericTrigger lt(double number) {
        this.condition = Condition.lessThan ;
        this.number = number ;
        return this ;
    }

    @Override
    public boolean firesOn(Answer<NumericQuestion> answer) {

        if (answer == null)
            return false ;

        NumericAnswer a = (NumericAnswer) answer ;

        switch(condition) {

            case equal:
                return Math.abs(a.getNumber() - number) < equalityThreshold ;
            case lessThan:
                return a.getNumber() < number ;
            case greaterThan:
                return a.getNumber() > number ;
        }

        return false;
    }

    public void assertIsComplete(Survey survey) throws InvalidTrigger {

        super.assertIsComplete(survey); ;

        if (condition == null)
            throw new InvalidTrigger("no condition specified") ;

        if (number == null)
            throw new InvalidTrigger("no number specified") ;

        if (survey == null)
            return ;

        Field field = survey.getField(getQuestionId()) ;
        if (field.getType() != Field.Type.numeric)
            throw new InvalidTrigger("'" + field.getId() + "' is not a mood question") ;
    }

    public String toString() {
        return condition + ":" + number ;
    }
}
