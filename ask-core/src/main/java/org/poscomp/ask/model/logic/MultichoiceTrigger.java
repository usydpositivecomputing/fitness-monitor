package org.poscomp.ask.model.logic;

import org.poscomp.ask.error.InvalidTrigger;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.answers.MultichoiceAnswer;
import org.poscomp.ask.model.fields.Field;
import org.poscomp.ask.model.fields.MultichoiceQuestion;

/**
 * Created by dmilne on 26/11/14.
 */
public class MultichoiceTrigger extends Trigger<MultichoiceQuestion> {

    public enum Condition {contains, notContains} ;

    private Condition condition ;

    private String choice ;


    private MultichoiceTrigger() {

    }

    protected MultichoiceTrigger(MultichoiceQuestion question) {
        super(question);
    }

    protected MultichoiceTrigger(String questionId) {
        super(questionId);
    }

    public MultichoiceTrigger contains(String choice) {
        this.condition = Condition.contains ;
        this.choice = choice ;
        return this ;
    }

    public MultichoiceTrigger notContains(String choice) {
        this.condition = Condition.notContains ;
        this.choice = choice ;
        return this ;
    }


    public Condition getCondition() {
        return condition;
    }

    public String getChoice() {
        return choice;
    }

    @Override
    public boolean firesOn(Answer<MultichoiceQuestion> answer) {

        if (answer == null)
            return false ;

        MultichoiceAnswer a = (MultichoiceAnswer) answer ;

        switch (condition) {

            case contains:
                return a.getChoices().contains(choice) ;
            case notContains:
                return !a.getChoices().contains(choice) ;
        }

        return false;
    }

    public void assertIsComplete(Survey survey) throws InvalidTrigger {

        super.assertIsComplete(survey); ;

        if (condition == null)
            throw new InvalidTrigger("no condition specified") ;

        if (choice == null)
            throw new InvalidTrigger("no choice specified") ;

        if (survey == null)
            return ;

        Field field = survey.getField(getQuestionId()) ;
        if (field.getType() != Field.Type.multichoice)
            throw new InvalidTrigger("'" + field.getId() + "' is not a multichoice question") ;
    }

    public String toString() {
        return condition + ":" + choice ;
    }
}
