package org.poscomp.ask.model.fields;

import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.model.data.Choice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dmilne on 25/11/14.
 */
public class MultichoiceQuestion extends Question {

    protected List<Choice> choices ;

    protected boolean allowOther ;

    private MultichoiceQuestion() {
        super(Type.multichoice) ;
    }

    public List<Choice> getChoices() {

        if (choices == null)
            choices = new ArrayList<Choice>() ;

        return Collections.unmodifiableList(choices) ;
    }

    public boolean getAllowOther() {
        return allowOther;
    }

    public void assertIsComplete() throws InvalidField {

        super.assertIsComplete();

        if (choices == null || choices.isEmpty())
            throw new InvalidField(this, "No choices specified") ;

        if (choices.size() < 2)
            throw new InvalidField(this, "Must specify at least two choices") ;
    }


    public static class Builder extends Question.Builder<MultichoiceQuestion> {

        private MultichoiceQuestion question ;

        @Override
        public MultichoiceQuestion f() {
            return question ;
        }

        public Builder() {
            super() ;
            question = new MultichoiceQuestion() ;
            question.allowOther = false ;

        }

        public Builder setId(String id) {
            super.setId(id) ;
            return this ;
        }

        public Builder setQuestion(String question) {
            super.setQuestion(question) ;
            return this ;
        }

        public Builder setNotes(String notes) {
            super.setNotes(notes) ;
            return this ;
        }

        public Builder setTags(String... tags) {
            super.setTags(tags) ;
            return this ;
        }

        public Builder setOptional() {
            super.setOptional() ;
            return this ;
        }

        public Builder hide() {
            super.hide() ;
            return this ;
        }

        public Builder setChoices(String... choices) {
            question.choices = new ArrayList<Choice>() ;

            for (String choice:choices)
                question.choices.add(new Choice(choice, null)) ;

            return this;
        }

        public Builder addChoice(String choice) {

            if (question.choices == null)
                question.choices = new ArrayList<Choice>() ;

            question.choices.add(new Choice(choice, null)) ;

            return this ;
        }

        public Builder addChoice(String name, String description) {

            if (question.choices == null)
                question.choices = new ArrayList<Choice>() ;

            question.choices.add(new Choice(name, description)) ;

            return this ;
        }

        public Builder allowOther() {
            question.allowOther = true ;
            return this ;
        }

    }
}
