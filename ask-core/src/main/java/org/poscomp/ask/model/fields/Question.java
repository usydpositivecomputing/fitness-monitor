package org.poscomp.ask.model.fields;


import org.poscomp.ask.error.InvalidField;

/**
 * Created by dmilne on 25/11/14.
 */
public abstract class Question extends Field {

    protected String question ;
    protected boolean optional ;

    public String getQuestion() {
        return question;
    }

    public boolean isOptional() {
        return optional;
    }

    protected Question(Type type) {
        super(type) ;
    }

    public void assertIsComplete() throws InvalidField {
        if (question == null)
            throw new InvalidField(this, "Missing question") ;
    }

    public static abstract class Builder<Q extends Question> extends Field.Builder<Q> {


        public Builder hide() {
            super.hide() ;
            return this ;
        }

        public Builder<Q> setQuestion(String question) {
            f().question = question ;
            return this ;
        }

        public Builder<Q> setOptional() {
            f().optional = true ;
            return this ;
        }
    }
}
