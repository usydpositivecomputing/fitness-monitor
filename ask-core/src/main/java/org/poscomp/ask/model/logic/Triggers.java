package org.poscomp.ask.model.logic;

import org.poscomp.ask.model.fields.*;

/**
 * Created by dmilne on 6/01/15.
 */
public class Triggers {

    public static SinglechoiceTrigger singlechoiceTrigger(String questionId) {
        return new SinglechoiceTrigger(questionId) ;
    }

    public static SinglechoiceTrigger singlechoiceTrigger(SinglechoiceQuestion question) {
        return new SinglechoiceTrigger(question) ;
    }

    public static MultichoiceTrigger multichoiceTrigger(String questionId) {
        return new MultichoiceTrigger(questionId) ;
    }

    public static MultichoiceTrigger multichoiceTrigger(MultichoiceQuestion question) {
        return new MultichoiceTrigger(question) ;
    }

    public static NumericTrigger numericTrigger(String questionId) {
        return new NumericTrigger(questionId) ;
    }

    public static NumericTrigger numericTrigger(NumericQuestion question) {
        return new NumericTrigger(question) ;
    }

    public static FreetextTrigger freetextTrigger(String questionId) {
        return new FreetextTrigger(questionId) ;
    }

    public static FreetextTrigger freetextTrigger(FreetextQuestion question) {
        return new FreetextTrigger(question) ;
    }

    public static MoodTrigger moodTrigger(String questionId) {
        return new MoodTrigger(questionId) ;
    }

    public static MoodTrigger moodTrigger(MoodQuestion question) {
        return new MoodTrigger(question) ;
    }
}
