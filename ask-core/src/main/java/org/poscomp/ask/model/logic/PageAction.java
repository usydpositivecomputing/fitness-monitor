package org.poscomp.ask.model.logic;

import org.poscomp.ask.error.InvalidAction;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.fields.Field;
import org.poscomp.ask.model.fields.PageBreak;

/**
 * Created by dmilne on 4/12/14.
 */
public class PageAction implements Action{

    public enum Action {skip, skipTo, skipToEnd} ;

    private Action action ;

    private String pageId ;

    private PageAction() {

    }

    protected PageAction(Action action, String pageId) {
        this.action = action ;
        this.pageId = pageId ;
    }


    public Action getAction() {
        return action;
    }

    public String getPageId() {
        return pageId;
    }


 //   @Override
    public void assertIsComplete(Survey survey) throws InvalidAction {

        if (action == null)
            throw new InvalidAction("No action specified") ;

        if (action == Action.skipToEnd)
            return ;

        if (pageId == null)
            throw new InvalidAction("No pageId specified") ;

        if (survey == null)
            return ;

        Field field = survey.getField(pageId) ;
        if (field == null)
            throw new InvalidAction("'" + pageId + "' is not a valid field") ;

        if (field.getType() != Field.Type.pageBreak)
            throw new InvalidAction("Page rules can only operate on pagebreaks. Use field rules instead.") ;
    }

    public String toString() {

        if (pageId != null)
            return action + ":" + pageId ;
        else
            return action.toString() ;
    }

}
