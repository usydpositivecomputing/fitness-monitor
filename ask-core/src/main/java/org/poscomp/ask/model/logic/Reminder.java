package org.poscomp.ask.model.logic;

import java.util.Date;

public class Reminder {
	
	String message;
	Date alertDate;
	
	public Reminder()
	{
		
	}
	
	public Reminder(String msg, Date alertDate)
	{
		this.message=msg;
		this.alertDate=alertDate;
	}
	public String getMessage() {
		return message;
	}
	public Date getAlertDate() {
		return alertDate;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public void setAlertDate(Date alertDate) {
		this.alertDate = alertDate;
	}
	

}
