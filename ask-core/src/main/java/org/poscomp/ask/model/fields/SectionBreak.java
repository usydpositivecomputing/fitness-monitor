package org.poscomp.ask.model.fields;

/**
 * Created by dmilne on 28/11/14.
 */
public class SectionBreak extends Field {

    private String title ;
    private String text ;

    private SectionBreak() {
        super(Type.sectionBreak) ;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public static class Builder extends Field.Builder<SectionBreak> {

        private SectionBreak sectionBreak ;

        @Override
        public SectionBreak f() {
            return sectionBreak ;
        }

        public Builder() {
            super() ;
            sectionBreak = new SectionBreak() ;
        }

        public Builder setId(String id) {
            super.setId(id) ;
            return this ;
        }

        public Builder setTitle(String title) {
            sectionBreak.title = title ;
            return this ;
        }

        public Builder setText(String text) {
            sectionBreak.text = text ;
            return this ;
        }

        public Builder setNotes(String notes) {
            super.setNotes(notes) ;
            return this ;
        }

        public Builder setTags(String... tags) {
            super.setTags(tags) ;
            return this ;
        }

        public Builder hide() {
            super.hide() ;
            return this ;
        }

    }
}
