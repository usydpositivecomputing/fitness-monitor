package org.poscomp.ask.model.fields;

import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.model.fields.Field;

/**
 * Created by dmilne on 25/11/14.
 */
public class Instruction extends Field {

    private String text ;

    private Instruction() {
        super(Type.instruction) ;
    }

    public String getText() {
        return text ;
    }


    public static class Builder extends Field.Builder<Instruction> {

        private Instruction instruction ;

        @Override
        public Instruction f() {
            return instruction ;
        }

        public Builder() {
            super() ;
            instruction = new Instruction() ;
        }

        public Builder setId(String id) {
            super.setId(id) ;
            return this ;
        }

        public Builder setText(String text) {
            instruction.text = text ;
            return this ;
        }

        public Builder setNotes(String notes) {
            super.setNotes(notes) ;
            return this ;
        }

        public Builder setTags(String... tags) {
            super.setTags(tags) ;
            return this ;
        }

        public Builder hide() {
            super.hide() ;
            return this ;
        }

    }
}
