package org.poscomp.ask.model.answers;

import org.poscomp.ask.model.fields.FreetextQuestion;

/**
 * Created by dmilne on 25/11/14.
 */
public class FreetextAnswer extends Answer<FreetextQuestion> {

    private String text ;

    private FreetextAnswer() {

    }

    @Override
    public boolean isAnswered() {
        return text != null;
    }

    public String getText() {
        return text ;
    }

    public static FreetextAnswer withText(String text) {
        FreetextAnswer answer = new FreetextAnswer() ;
        answer.text = text ;
        return answer ;
    }

    public String toString() {
        return text ;
    }


}
