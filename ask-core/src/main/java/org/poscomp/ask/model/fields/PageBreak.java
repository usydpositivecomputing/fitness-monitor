package org.poscomp.ask.model.fields;

/**
 * Created by dmilne on 26/11/14.
 */
public class PageBreak extends Field {


    private String title ;
    private String text ;


    private PageBreak() {
        super(Type.pageBreak) ;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public static class Builder extends Field.Builder<PageBreak> {

        private PageBreak pageBreak ;

        @Override
        public PageBreak f() {
            return pageBreak ;
        }

        public Builder() {
            super() ;
            pageBreak = new PageBreak() ;
        }

        public Builder setId(String id) {
            super.setId(id) ;
            return this ;
        }

        public Builder setTitle(String title) {
            pageBreak.title = title ;
            return this ;
        }

        public Builder setText(String text) {
            pageBreak.text = text ;
            return this ;
        }

        public Builder setNotes(String notes) {
            super.setNotes(notes) ;
            return this ;
        }

        public Builder setTags(String... tags) {
            super.setTags(tags) ;
            return this ;
        }

        public Builder hide() {
            super.hide() ;
            return this ;
        }

    }
}
