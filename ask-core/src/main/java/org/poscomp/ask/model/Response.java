package org.poscomp.ask.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.types.ObjectId;
import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.fields.Question;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;

/**
 * Created by dmilne on 25/11/14.
 */

public class Response {

	private int survey_id;
	
	private String username;
	
    private ObjectId id ;

    private ObjectId surveyId ;

    private String respondentId ;

    private Integer pageIndex ;

    private Date startedAt ;

    private Date completedAt ;

    private Date modifiedAt ;

    private Map<String, Answer<?>> answers ;

    private Response() {

    }

    public Response(Survey survey, String respondentId) {

    	this.survey_id=survey.getSurvey_id();
    	this.username=survey.getUsername();
        this.surveyId = survey.getId() ;
        this.respondentId = respondentId ;
        this.startedAt = new Date() ;
        this.modifiedAt = new Date() ;
    }


    public ObjectId getId() {
        return id;
    }

    public Response setId(ObjectId id) {
        this.id = id ;
        this.modifiedAt = new Date() ;

        return this ;
    }

    public ObjectId getSurveyId() {
        return surveyId;
    }

    public String getRespondentId() {
        return respondentId;
    }

    public Integer getPageIndex() {

        return pageIndex ;
    }

    public Response setPageIndex(int index) {

        this.pageIndex = index ;
        this.modifiedAt = new Date() ;
        return this ;
    }

    public Date getStartedAt() {
        return startedAt ;
    }

    public Response setStartedAt(Date date) {
        this.startedAt = date ;
        return this ;
    }

    public Date getCompletedAt() {
        return completedAt;
    }

    @JsonIgnore
    public boolean isCompleted() {
        return completedAt != null ;
    }

    public Response setCompletedAt(Date date) {

        this.completedAt = date ;
        this.modifiedAt = new Date() ;

        return this ;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public Map<String,Answer<?>> getAnswers() {

        if (answers == null)
            answers = new HashMap<String, Answer<?>>() ;


        return Collections.unmodifiableMap(answers) ;
    }

    public Answer<?> getAnswer(String questionId) {

        if (answers == null)
            return null ;

        return answers.get(questionId) ;
    }

    public <Q extends Question> Response setAnswer(Q question, Answer<Q> answer) {

        if (answers == null)
            answers = new LinkedHashMap<String, Answer<?>>() ;

        answers.put(question.getId(), answer) ;
        modifiedAt = new Date() ;

        return this ;
    }

    public Response setAnswer(String questionId, Answer<?> answer) {

        if (answers == null)
            answers = new LinkedHashMap<String, Answer<?>>() ;

        answers.put(questionId, answer) ;
        modifiedAt = new Date() ;

        return this ;
    }

    public Response setAnswers(Map<String, Answer<?>> answers) {

        this.answers = new LinkedHashMap<String, Answer<?>>() ;

        this.answers.putAll(answers) ;
        modifiedAt = new Date() ;

        return this ;
    }

	public int getSurvey_id() {
		return survey_id;
	}

	public String getUsername() {
		return username;
	}

	public void setSurvey_id(int survey_id) {
		this.survey_id = survey_id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
