package org.poscomp.ask.model.logic;

import org.poscomp.ask.error.InvalidTrigger;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.answers.MultichoiceAnswer;
import org.poscomp.ask.model.answers.SinglechoiceAnswer;
import org.poscomp.ask.model.fields.Field;
import org.poscomp.ask.model.fields.MultichoiceQuestion;
import org.poscomp.ask.model.fields.SinglechoiceQuestion;
import org.poscomp.ask.util.StringNormalizer;

/**
 * Created by dmilne on 27/11/14.
 */
public class SinglechoiceTrigger extends Trigger<SinglechoiceQuestion> {

    public enum Condition {is, isNot} ;

    private Condition condition ;

    private String choice ;


    private SinglechoiceTrigger() {
        super() ;
    }

    protected SinglechoiceTrigger(SinglechoiceQuestion question) {
        super(question);
    }

    protected SinglechoiceTrigger(String questionId) {
        super(questionId);
    }

    public SinglechoiceTrigger is(String choice) {
        this.condition = Condition.is ;
        this.choice = choice ;
        return this ;
    }

    public SinglechoiceTrigger isNot(String choice) {
        this.condition = Condition.isNot ;
        this.choice = choice ;
        return this ;
    }


    public Condition getCondition() {
        return condition;
    }

    public String getChoice() {
        return choice;
    }

    @Override
    public boolean firesOn(Answer<SinglechoiceQuestion> answer) {

        if (answer == null)
            return false ;

        SinglechoiceAnswer a = (SinglechoiceAnswer) answer ;

        String normalizedAnswerChoice = StringNormalizer.normalize(a.getChoice()) ;
        String normalizedTriggerChoice = StringNormalizer.normalize(choice) ;

        switch (condition) {

            case is:
                return normalizedAnswerChoice.equals(normalizedTriggerChoice) ;
            case isNot:
                return !normalizedAnswerChoice.equals(normalizedTriggerChoice) ;
        }

        return false;
    }

    public void assertIsComplete(Survey survey) throws InvalidTrigger {

        super.assertIsComplete(survey); ;

        if (condition == null)
            throw new InvalidTrigger("no condition specified") ;

        if (choice == null)
            throw new InvalidTrigger("no choice specified") ;

        if (survey == null)
            return ;

        Field field = survey.getField(getQuestionId()) ;
        if (field.getType() != Field.Type.singlechoice)
            throw new InvalidTrigger("'" + field.getId() + "' is not a singlechoice question") ;
    }

    public String toString() {
        return condition + ":" + choice ;
    }
}