package org.poscomp.ask.model.logic;

import org.poscomp.ask.error.InvalidAction;
import org.poscomp.ask.model.Survey;

/**
 * Created by dmilne on 4/12/14.
 */
public interface Action {

    public void assertIsComplete(Survey survey) throws InvalidAction;
}
