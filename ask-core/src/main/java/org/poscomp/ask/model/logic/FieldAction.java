package org.poscomp.ask.model.logic;

import org.poscomp.ask.error.InvalidAction;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.fields.Field;


/**
 * Created by dmilne on 26/11/14.
 */
public class FieldAction implements Action{

    public enum Action {show, hide} ;

    private Action action ;

    private String fieldId ;

    private FieldAction() {

    }

    protected FieldAction(Action action, String fieldId) {
        this.action = action ;
        this.fieldId = fieldId ;
    }


    public Action getAction() {
        return action;
    }

    public String getFieldId() {
        return fieldId;
    }


 //   @Override
    public void assertIsComplete(Survey survey) throws InvalidAction {

        if (action == null)
            throw new InvalidAction("No action specified") ;

        if (fieldId == null)
            throw new InvalidAction("No fieldId specified") ;

        if (survey == null)
            return ;

        Field field = survey.getField(fieldId) ;
        if (field == null)
            throw new InvalidAction("'" + fieldId + "' is not a valid field") ;

        if (field.getType() == Field.Type.pageBreak)
            throw new InvalidAction("Field rules cannot operate on pagebreaks. Use page rules instead.") ;
    }

    public String toString() {

        return action + ":" + fieldId ;
    }

}
