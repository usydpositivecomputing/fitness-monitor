package org.poscomp.ask.model.logic;

import org.poscomp.ask.error.InvalidTrigger;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.fields.*;

/**
 * A response to a question, that might cause us to invoke a particular rule
 */
public abstract class Trigger<Q extends Question> implements Cloneable {


    private String questionId ;

    public String getQuestionId() {
        return questionId ;
    }

    protected Trigger() {

    }

    protected Trigger(Q question) {
        this.questionId = question.getId() ;
    }

    protected Trigger(String questionId) {
        this.questionId = questionId ;
    }

    public boolean firesOn(Answer<Q> answer) {
        throw new UnsupportedOperationException() ;
    }

    public void assertIsComplete(Survey survey) throws InvalidTrigger {

        if (questionId == null)
            throw new InvalidTrigger("no questionId specified") ;

        if (survey == null)
            return ;

        Field field = survey.getField(questionId) ;
        if (field == null)
            throw new InvalidTrigger("\"" + questionId + "\" is not a valid field");
    }

}
