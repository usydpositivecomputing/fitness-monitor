package org.poscomp.ask.model.fields;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.model.Survey;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;


/**
 * Created by dmilne on 25/11/14.
 */

@JsonTypeInfo(
        property = "type",
        include = JsonTypeInfo.As.PROPERTY,
        use = JsonTypeInfo.Id.NAME
)
@JsonSubTypes(
        {
                @JsonSubTypes.Type(value=Instruction.class, name="instruction"),
                @JsonSubTypes.Type(value=PageBreak.class, name="pageBreak"),
                @JsonSubTypes.Type(value=SectionBreak.class, name="sectionBreak"),
                @JsonSubTypes.Type(value=FreetextQuestion.class, name="freetext"),
                @JsonSubTypes.Type(value=NumericQuestion.class, name="numeric"),
                @JsonSubTypes.Type(value=SinglechoiceQuestion.class, name="singlechoice"),
                @JsonSubTypes.Type(value=MultichoiceQuestion.class, name="multichoice"),
                @JsonSubTypes.Type(value=MoodQuestion.class, name="mood"),
                @JsonSubTypes.Type(value=MatrixQuestion.class, name="matrix")
        }
)
public abstract class Field {

    public static final Pattern placeHolderPattern = Pattern.compile("\\[\\[([^]]+)\\]\\]") ;

    public enum Type {
        instruction,
        pageBreak,
        sectionBreak,
        freetext,
        numeric,
        singlechoice,
        multichoice,
        mood,
        matrix
    }

    protected String id ;

    protected Type type ;

    protected String notes ;

    protected boolean hidden ;

    protected List<String> tags ;

    public String getId() {
        return id ;
    }

    public Field(Type type) {
        this.type = type ;
    }

    public void setId(String id) {

        if (this.id != null)
            throw new IllegalArgumentException("This field already has an id, which cannot be modified") ;

        this.id = id ;
    }



    public String getNotes() {
        return notes;
    }

    public boolean isHidden() {
        return hidden;
    }

    public List<String> getTags() {

        if (tags == null)
            return null ;

        return Collections.unmodifiableList(tags) ;
    }

    @JsonIgnore
    public Type getType() {
        return type ;
    }

    public void assertIsComplete(Survey survey) throws InvalidField {

        if (id==null)
            throw new InvalidField(this, "Missing id") ;

        if (survey == null)
            return ;

        if (notes != null)
            checkPlaceholders(notes, survey);

    }

    public void checkPlaceholders(String text, Survey survey) throws InvalidField {

        Matcher m = placeHolderPattern.matcher(text) ;

        while (m.matches()) {

            String questionId = m.group(1) ;

            Field field = survey.getField(questionId) ;

            if (field == null)
                throw new InvalidField(this, "Placeholder [[" + questionId + "]] points to an unknown field") ;

            if (! (field instanceof Question))
                throw new InvalidField(this, "Placeholder [[" + questionId + "]] does not point to a question field") ;

            if (survey.getFieldIndex(questionId) >= survey.getFieldIndex(this.id))
                throw new InvalidField(this, "Placeholder [[" + questionId + "]] refers to a field found after this one") ;

        }
    }



    public static abstract class Builder<F extends Field> {

        public abstract F f() ;

        public Builder() {

        }

        public Builder<F> setId(String id) {
            f().id = id ;
            return this ;
        }


        public Builder<F> setNotes(String notes) {
            f().notes = notes ;
            return this ;
        }

        public Builder<F> setTags(String... tags) {
            f().tags = new ArrayList<String>() ;

            for (String tag:tags)
                f().tags.add(tag) ;

            return this ;
        }

        public Builder<F> hide() {
            f().hidden = true ;
            return this ;
        }

        protected void assertIsBuildable() throws InvalidField {
            f().assertIsComplete(null);
        }

        public F build() throws InvalidField {

            assertIsBuildable();

            return f() ;
        }
    }

}
