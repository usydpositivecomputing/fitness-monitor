package org.poscomp.ask.model;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.bson.types.ObjectId;
import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.error.InvalidRule;
import org.poscomp.ask.error.InvalidSurvey;
import org.poscomp.ask.model.fields.Field;
import org.poscomp.ask.model.logic.FieldRule;
import org.poscomp.ask.model.logic.PageRule;
import org.poscomp.ask.model.logic.Reminder;
import org.poscomp.ask.util.JsonUtils;

import java.io.IOException;
import java.util.*;

/**
 * Created by dmilne on 25/11/14.
 */

@JsonIgnoreProperties({ "fieldsById" })
public class Survey {

    private ObjectId id ;


    private String readableId ;


    private String creatorId ;

    private String groupId ;

    private String title ;

    private String completionMessage ;

    private List<String> tags ;

    private List<Field> fields ;

    private transient Map<String,Field> fieldsById ;

    private List<FieldRule> fieldRules ;

    private List<PageRule> pageRules ;


    private Date createdAt ;


    private Date modifiedAt ;


    private boolean finalized ;


    private boolean deleted ;

    
    // added Fields for Rest API 
    private String username;
    
    private String mobileNumber;
    
    private Date expiry;
    
    private List<Reminder> reminders;
    
    private boolean needResponse;
    
    private int survey_id;

    public ObjectId getId() {

        return id;
    }

    public String getReadableId() {
        return readableId;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getTitle() {
        return title;
    }

    public String getCompletionMessage() {
        return completionMessage ;
    }

    public List<String> getTags() {
        return tags;
    }

    public List<Field> getFields() {
        return fields;
    }

    public List<FieldRule> getFieldRules() {

        if (fieldRules == null)
            fieldRules = new ArrayList<FieldRule>() ;

        return Collections.unmodifiableList(fieldRules) ;
    }

    public List<PageRule> getPageRules() {

        if (pageRules == null)
            pageRules = new ArrayList<PageRule>() ;

        return Collections.unmodifiableList(pageRules) ;

    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public boolean isFinalized() { return finalized; }

    public boolean isDeleted() {
        return deleted;
    }


    public Field getField(String id) {

        if (fieldsById == null) {
            fieldsById = new HashMap<String,Field>() ;
            for(Field field: fields)
                fieldsById.put(field.getId(), field) ;
        }

        return fieldsById.get(id) ;
    }

    public void assertIsComplete() throws InvalidSurvey {

        if (title == null)
            throw new InvalidSurvey("No title specified") ;

        if (fields == null || fields.isEmpty())
            throw new InvalidSurvey("No fields specified") ;

        for (Field field:getFields()) {
            try {
                field.assertIsComplete(this);
            } catch (InvalidField e) {
                throw new InvalidSurvey("Field " + field.getId() + " is invalid", e);
            }
        }

        for(FieldRule rule:getFieldRules()) {
            try {
                rule.assertIsComplete(this);
            } catch (InvalidRule e) {
                throw new InvalidSurvey("Field rule " + rule.toString() + " is invalid", e);
            }
        }

        for(PageRule rule:getPageRules()) {
            try {
                rule.assertIsComplete(this);
            } catch (InvalidRule e) {
                throw new InvalidSurvey("Page rule " + rule.toString() + " is invalid", e);
            }
        }

    }

    public static class Builder {

        private Survey survey ;

        private int nextFieldId = 1 ;

        private Set<String> usedFieldIds = new HashSet<String>() ;

        public Builder(String title) {

            survey = new Survey() ;

            survey.createdAt = new Date() ;
            survey.title = title ;

            survey.fields = new ArrayList<Field>() ;
        }

        public Builder(Survey s) {
            survey = new Survey() ;

            survey.id = s.getId() ;
            survey.readableId = s.getReadableId() ;
            survey.creatorId = s.getCreatorId() ;
            survey.createdAt = s.getCreatedAt() ;

            copy(s) ;
        }

        public Builder copy(Survey s) {

            survey.title = s.title ;

            setGroupId(s.groupId) ;

            setCompletionMessage(s.completionMessage) ;
            setTags(s.getTags()) ;

            usedFieldIds = new HashSet<String>() ;
            survey.fields = new ArrayList<Field>() ;
            for (Field field:s.getFields()) {
                //we don't have to bother with cloning because fields are immutable (no setter methods)
                addField(field);
            }

            if (s.fieldRules != null) {
                survey.fieldRules = new ArrayList<FieldRule>() ;
                for (FieldRule rule : s.fieldRules)
                    addFieldRule(rule);
            }

            if (s.pageRules != null) {
                survey.pageRules = new ArrayList<PageRule>() ;
                for (PageRule rule: s.pageRules)
                    addPageRule(rule) ;
            }

            survey.deleted = s.isDeleted() ;
            survey.finalized = s.isFinalized() ;

            return this ;
        }


        public Builder setId(ObjectId id) {
            survey.id = id ;
            return this ;
        }

        public Builder setReadableId(String id) {
            survey.readableId = id ;
            return this ;
        }

        public Builder setGroupId(String groupId) {
            survey.groupId = groupId ;
            return this ;
        }

        public Builder setCreatorId(String creatorId) {
            survey.creatorId = creatorId ;
            return this ;
        }

        public Builder setCompletionMessage(String msg) {
            survey.completionMessage = msg ;
            return this ;
        }

        public Builder setTags(String... tags) {
            survey.tags = new ArrayList<String>() ;

            for (String tag:tags)
                survey.tags.add(tag) ;

            return this ;
        }

        public Builder setTags(Collection<String> tags) {

            if (tags == null) {
                survey.tags = null;
                return this ;
            }

            survey.tags = new ArrayList<String>() ;
            survey.tags.addAll(tags) ;
            return this ;
        }

        public Builder addTag(String tag) {
            if(survey.tags == null)
                survey.tags = new ArrayList<String>() ;

            survey.tags.add(tag) ;
            return this ;
        }

        public Builder addField(Field field) {

            if (survey.fields == null)
                survey.fields = new ArrayList<Field>() ;

            if (field.getId() == null) {
                field.setId(String.valueOf(nextFieldId));
                nextFieldId++;
            }


            survey.fields.add(field) ;

            return this ;
        }

        public Builder addFieldRule(FieldRule rule) {

            if (survey.fieldRules == null)
                survey.fieldRules = new ArrayList<FieldRule>() ;

            survey.fieldRules.add(rule) ;

            return this ;
        }

        public Builder addPageRule(PageRule rule) {

            if (survey.pageRules == null)
                survey.pageRules = new ArrayList<PageRule>() ;

            survey.pageRules.add(rule) ;

            return this ;
        }

        public Survey build() throws InvalidSurvey {

            survey.modifiedAt = new Date() ;
            survey.assertIsComplete() ;

            return survey ;
        }



    }
    
    public int getFieldIndex(String fieldId) {

        int index = 0 ;

        for (Field field : fields){
            if (fieldId.equals(field.getId()))
                return index ;

            index++ ;
        }

        return -1 ;

    }




    public static Survey fromJson(String json) throws IOException, InvalidSurvey {

        ObjectMapper mapper = JsonUtils.getObjectMapper() ;

        Survey survey = mapper.readValue(json, Survey.class) ;

        survey.assertIsComplete();

        return survey ;
    }

    
    public static List<Survey> fromJsonList(String json) throws IOException, InvalidSurvey {

        ObjectMapper mapper = JsonUtils.getObjectMapper() ;

        Survey[] survey = mapper.readValue(json, Survey[].class) ;
        List<Survey> list=Arrays.asList(survey);

        for(Survey s:list){
			  s.assertIsComplete();
			  
		  }

        return list ;
    }
    
    
    
    // Getter and Setters (Generated)
    
	public Map<String, Field> getFieldsById() {
		return fieldsById;
	}

	public String getUsername() {
		return username;
	}

	public Date getExpiry() {
		return expiry;
	}

	public List<Reminder> getReminders() {
		return reminders;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public void setReadableId(String readableId) {
		this.readableId = readableId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCompletionMessage(String completionMessage) {
		this.completionMessage = completionMessage;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public void setFieldsById(Map<String, Field> fieldsById) {
		this.fieldsById = fieldsById;
	}

	public void setFieldRules(List<FieldRule> fieldRules) {
		this.fieldRules = fieldRules;
	}

	public void setPageRules(List<PageRule> pageRules) {
		this.pageRules = pageRules;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public void setFinalized(boolean finalized) {
		this.finalized = finalized;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}

	public void setReminders(List<Reminder> reminders) {
		this.reminders = reminders;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public boolean isNeedResponse() {
		return needResponse;
	}

	public void setNeedResponse(boolean needResponse) {
		this.needResponse = needResponse;
	}

	public int getSurvey_id() {
		return survey_id;
	}

	public void setSurvey_id(int survey_id) {
		this.survey_id = survey_id;
	}

}
