package org.poscomp.ask.model.fields;

import org.poscomp.ask.error.InvalidField;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dmilne on 25/11/14.
 */
public class MatrixQuestion extends Question {

    private List<String> rows ;

    private List<String> columns ;

    public MatrixQuestion() {
        super(Type.matrix) ;
    }

    public List<String> getRows() {

        if (rows == null)
            rows = new ArrayList<String>() ;

        return Collections.unmodifiableList(rows) ;
    }

    public List<String> getColumns() {

        if (columns == null)
            columns = new ArrayList<String>() ;

        return Collections.unmodifiableList(columns) ;

    }

    public void assertIsComplete() throws InvalidField {
        super.assertIsComplete();

        if (rows == null || rows.isEmpty())
            throw new InvalidField(this, "No rows specified") ;

        if (columns == null || columns.isEmpty())
            throw new InvalidField(this, "No columns specified") ;

        if (columns.size() < 2)
            throw new InvalidField(this, "Matrix must contain at least 2 columns") ;

    }


    public static class Builder extends Question.Builder<MatrixQuestion> {

        private MatrixQuestion question ;

        @Override
        public MatrixQuestion f() {
            return question ;
        }

        public Builder() {
            super() ;
            question = new MatrixQuestion() ;
        }

        public Builder setId(String id) {
            super.setId(id) ;
            return this ;
        }

        public Builder setQuestion(String question) {
            super.setQuestion(question) ;
            return this ;
        }

        public Builder setNotes(String notes) {
            super.setNotes(notes) ;
            return this ;
        }

        public Builder setTags(String... tags) {
            super.setTags(tags) ;
            return this ;
        }

        public Builder setOptional() {
            super.setOptional() ;
            return this ;
        }

        public Builder hide() {
            super.hide() ;
            return this ;
        }

        public Builder setRows(String... rows) {

            question.rows = new ArrayList<String>() ;
            for (String row:rows)
                question.rows.add(row) ;

            return this ;
        }

        public Builder setColumns(String... columns) {

            question.columns = new ArrayList<String>() ;
            for (String col:columns)
                question.columns.add(col) ;

            return this ;
        }


    }


}
