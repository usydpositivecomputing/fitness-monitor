package org.poscomp.ask.model.answers;

import org.poscomp.ask.model.fields.SinglechoiceQuestion;
import org.poscomp.ask.util.StringNormalizer;

import java.util.List;

/**
 * Created by dmilne on 27/11/14.
 */
public class SinglechoiceAnswer extends Answer<SinglechoiceQuestion> {

    private String choice ;

    private String other ;

    private SinglechoiceAnswer() {

    }

    @Override
    public boolean isAnswered() {

        if (choice != null)
            return true ;

        if (other != null)
            return true ;

        return false ;
    }

    public String getChoice() {
        return choice;
    }

    public String getOther() {
        return other;
    }

    public static SinglechoiceAnswer withChoice(String choice) {

        SinglechoiceAnswer answer = new SinglechoiceAnswer() ;
        answer.choice = StringNormalizer.normalize(choice) ;

        return answer ;
    }

    public static SinglechoiceAnswer withOther(String otherDescription) {

        SinglechoiceAnswer answer = new SinglechoiceAnswer() ;
        answer.choice = "other" ;
        answer.other = otherDescription ;

        return answer ;
    }

    public String toString() {

        if (choice != null)
            return choice ;

        if (other != null)
            return "other:" + other ;

        return "none" ;
    }
}