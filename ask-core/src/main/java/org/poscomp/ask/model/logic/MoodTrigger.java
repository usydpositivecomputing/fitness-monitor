package org.poscomp.ask.model.logic;

import org.poscomp.ask.error.InvalidTrigger;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.answers.MoodAnswer;
import org.poscomp.ask.model.data.Mood;
import org.poscomp.ask.model.fields.Field;
import org.poscomp.ask.model.fields.MoodQuestion;

/**
 * Created by dmilne on 26/11/14.
 */
public class MoodTrigger extends Trigger<MoodQuestion> {

    public enum Condition {is, isNot, isNear, isSameQuadrant}

    private Condition condition ;

    private Mood mood ;

    private MoodTrigger() {

    }

    public Condition getCondition() {
        return condition;
    }

    public Mood getMood() {
        return mood;
    }

    protected MoodTrigger(MoodQuestion question) {
        super(question) ;
    }

    protected MoodTrigger(String questionId) {
        super(questionId) ;
    }

    public MoodTrigger is(Mood mood) {
        this.condition = Condition.is ;
        this.mood = mood ;
        return this ;
    }

    public MoodTrigger isNot(Mood mood) {
        this.condition = Condition.isNot ;
        this.mood = mood ;
        return this ;
    }

    public MoodTrigger isNear(Mood mood) {
        this.condition = Condition.isNear ;
        this.mood = mood ;
        return this ;
    }

    public MoodTrigger isSameQuadrant(Mood mood) {
        this.condition = Condition.isSameQuadrant ;
        this.mood = mood ;
        return this ;
    }

    @Override
    public boolean firesOn(Answer<MoodQuestion> answer) {

        if (answer == null)
            return false ;

        MoodAnswer a = (MoodAnswer) answer ;
        Mood answerMood = a.getMood() ;

        if (answerMood == null)
            return false ;

        switch(condition) {
            case is:
                return answerMood.equals(mood) ;
            case isNot:
                return !answerMood.equals(mood) ;
            case isNear:
                return answerMood.isNear(mood) ;
            case isSameQuadrant:
                return answerMood.getQuadrant() == mood.getQuadrant() ;
        }

        return false;
    }


    public void assertIsComplete(Survey survey) throws InvalidTrigger {

        super.assertIsComplete(survey); ;

        if (condition == null)
            throw new InvalidTrigger("no condition specified") ;

        if (mood == null)
            throw new InvalidTrigger("no mood specified") ;

        if (survey == null)
            return ;

        Field field = survey.getField(getQuestionId()) ;
        if (field.getType() != Field.Type.mood)
            throw new InvalidTrigger("'" + field.getId() + "' is not a mood question") ;
    }

    public String toString() {

        if (mood != null)
            return condition + ":" + mood.getName() ;
        else
            return condition.toString() ;
    }
}
