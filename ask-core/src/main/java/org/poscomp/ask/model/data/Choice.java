package org.poscomp.ask.model.data;

/**
 * Created by dmilne on 28/11/14.
 */
public class Choice {

    private String name ;
    private String description ;

    private Choice() {

    }

    public Choice(String name, String description) {

        this.name = name ;
        this.description = description ;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
