package org.poscomp.ask.model.logic;

import org.poscomp.ask.model.fields.Field;
import org.poscomp.ask.model.fields.PageBreak;

/**
 * Created by dmilne on 6/01/15.
 */
public class Actions {


    public static FieldAction showField(String fieldId) {
        return new FieldAction(FieldAction.Action.show, fieldId) ;
    }

    public static FieldAction showField(Field field) {
        return new FieldAction(FieldAction.Action.show, field.getId()) ;
    }

    public static FieldAction hideField(String fieldId) {
        return new FieldAction(FieldAction.Action.hide, fieldId) ;
    }

    public static FieldAction hideField(Field field) {
        return new FieldAction(FieldAction.Action.hide, field.getId()) ;
    }


    public static PageAction skipPage(String pageId) {
        return new PageAction(PageAction.Action.skip, pageId) ;
    }

    public static PageAction skipPage(PageBreak page) {
        return new PageAction(PageAction.Action.skip, page.getId()) ;
    }

    public static PageAction skipToPage(String pageId) {
        return new PageAction(PageAction.Action.skipTo, pageId) ;
    }

    public static PageAction skipToPage(PageBreak page) {
        return new PageAction(PageAction.Action.skipTo, page.getId()) ;
    }

    public static PageAction skipToEnd() {
        return new PageAction(PageAction.Action.skipToEnd, null) ;
    }

}
