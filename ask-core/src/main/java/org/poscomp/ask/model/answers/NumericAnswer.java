package org.poscomp.ask.model.answers;

import org.poscomp.ask.model.fields.NumericQuestion;

/**
 * Created by dmilne on 26/11/14.
 */
public class NumericAnswer extends Answer<NumericQuestion> {

    private Double number ;

    private NumericAnswer() {

    }

    @Override
    public boolean isAnswered() {
        return number != null;
    }

    public Double getNumber() {
        return number;
    }

    public static NumericAnswer withNumber(double number) {

        NumericAnswer answer = new NumericAnswer() ;
        answer.number = number ;
        return answer ;
    }

    public static NumericAnswer withNumber(int number) {

        NumericAnswer answer = new NumericAnswer() ;
        answer.number = (double)number ;
        return answer ;
    }

    public String toString() {

        if (number == null)
            return "-" ;

        return String.valueOf(number) ;
    }
}
