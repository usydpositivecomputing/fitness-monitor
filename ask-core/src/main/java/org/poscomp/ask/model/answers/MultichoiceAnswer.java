package org.poscomp.ask.model.answers;

import org.apache.commons.lang3.StringUtils;
import org.poscomp.ask.model.fields.MultichoiceQuestion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by dmilne on 25/11/14.
 */
public class MultichoiceAnswer extends Answer<MultichoiceQuestion> {

    private List<String> choices ;

    private String other ;

    private MultichoiceAnswer() {

    }

    @Override
    public boolean isAnswered() {
        if (choices != null && !choices.isEmpty())
            return true ;

        if (other != null)
            return true ;

        return false ;
    }


    public List<String> getChoices() {
        return choices;
    }

    public String getOther() {
        return other;
    }

    public static MultichoiceAnswer withChoices(Collection<String> choices) {

        MultichoiceAnswer answer = new MultichoiceAnswer() ;
        answer.choices = new ArrayList<String>() ;
        answer.choices.addAll(choices) ;

        return answer ;
    }

    public static MultichoiceAnswer withChoicesAndOther(Collection<String> choices, String other) {
        MultichoiceAnswer answer = new MultichoiceAnswer() ;
        answer.choices = new ArrayList<String>() ;
        answer.choices.addAll(choices) ;
        answer.other = other ;

        return answer ;
    }

    public String toString() {

        StringBuilder sb = new StringBuilder() ;

        if (!choices.isEmpty())
            sb.append(StringUtils.join(choices, "|")) ;

        if (other != null) {
            if (sb.length() > 0) sb.append("|") ;
            sb.append("other:" + other) ;
        }

        if (sb.length() == 0)
            sb.append("none") ;

        return sb.toString() ;
    }

}
