package org.poscomp.ask.model.logic;

import org.poscomp.ask.error.InvalidRule;
import org.poscomp.ask.model.Survey;

/**
 * Created by dmilne on 4/12/14.
 */
public class PageRule extends Rule<PageAction> {



    public void assertIsComplete(Survey survey) throws InvalidRule {

        super.assertIsComplete(survey);

        //check that skipTo and skipToEnd rules are on their own

        //check that skip and skipTo actions have a target page
    }

    public static class Builder extends Rule.Builder<PageAction, PageRule> {

        PageRule r = new PageRule() ;

        public PageRule r() {
            return r ;
        }

        public Builder addTrigger(Trigger trigger) {
            super.addTrigger(trigger) ;
            return this ;
        }

        public Builder addAction(PageAction action) {
            super.addAction(action);
            return this ;
        }

        public Builder withAndOperator() {
            super.withAndOperator();
            return this ;
        }

        public Builder withOrOperator() {
            super.withOrOperator();
            return this ;
        }

        public PageRule build() throws InvalidRule {
            return super.build() ;
        }
    }
}
