package org.poscomp.ask.model;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.poscomp.ask.error.InvalidSurvey;
import org.poscomp.ask.util.JsonUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Schema 
{
	private Survey survey;
	private Response response;
	
	public Response getResponse() {
		return response;
	}
	public void setResponse(Response response) {
		this.response = response;
	}
	public Survey getSurvey() {
		return survey;
	}
	public void setSurvey(Survey survey) {
		this.survey = survey;
	}
	
	 public static Schema fromJson(String json) throws IOException, InvalidSurvey {

	        ObjectMapper mapper = JsonUtils.getObjectMapper() ;

	        Schema schema = mapper.readValue(json, Schema.class) ;

	        schema.getSurvey().assertIsComplete();

	        return schema ;
	    }

	    
	    public static List<Schema> fromJsonList(String json) throws IOException, InvalidSurvey {

	        ObjectMapper mapper = JsonUtils.getObjectMapper() ;

	        Schema[] schema = mapper.readValue(json, Schema[].class) ;
	        List<Schema> list=Arrays.asList(schema);

	        for(Schema s:list){
				  s.getSurvey().assertIsComplete();
				  
			  }

	        return list ;
	    }
	
}
