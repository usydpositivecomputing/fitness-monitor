package org.poscomp.ask.model.logic;

import org.poscomp.ask.error.InvalidRule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dmilne on 25/11/14.
 */
public class FieldRule extends Rule<FieldAction> {

    private FieldRule() {

    }


    public static class Builder extends Rule.Builder<FieldAction, FieldRule> {

        FieldRule r = new FieldRule() ;

        public FieldRule r() {
            return r ;
        }

        public Builder addTrigger(Trigger trigger) {
            super.addTrigger(trigger) ;
            return this ;
        }

        public Builder addAction(FieldAction action) {
            super.addAction(action);
            return this ;
        }

        public Builder withAndOperator() {
            super.withAndOperator();
            return this ;
        }

        public Builder withOrOperator() {
            super.withOrOperator();
            return this ;
        }

        public FieldRule build() throws InvalidRule {
            return super.build() ;
        }
    }

}
