package org.poscomp.ask.model.fields;

/**
 * Created by dmilne on 26/11/14.
 */
public class NumericQuestion extends Question {

    private Range range  ;

    private NumericQuestion() {
        super(Type.numeric) ;
    }

    public Range getRange() {
        return range;
    }























    public static class Range {

        public enum Type {digits, value} ;

        private Integer min ;
        private Integer max ;
        private Type type ;

        private Range() {

        }

        public Range(Integer min, Integer max, Type type) {
            this.min = min ;
            this.max = max ;
            this.type = type ;
        }

        public Integer getMin() {
            return min;
        }

        public Integer getMax() {
            return max;
        }

        public Type getType() {
            return type;
        }
    }



    public static class Builder extends Question.Builder<NumericQuestion> {

        private NumericQuestion question ;

        @Override
        public NumericQuestion f() {
            return question ;
        }

        public Builder() {
            super() ;
            question = new NumericQuestion() ;
        }


        public Builder setId(String id) {
            super.setId(id) ;
            return this ;
        }

        public Builder setQuestion(String question) {
            super.setQuestion(question) ;
            return this ;
        }

        public Builder setNotes(String notes) {
            super.setNotes(notes) ;
            return this ;
        }

        public Builder setTags(String... tags) {
            super.setTags(tags) ;
            return this ;
        }

        public Builder setOptional() {
            super.setOptional() ;
            return this ;
        }

        public Builder hide() {
            super.hide() ;
            return this ;
        }

        public Builder setDigitRange(Integer min, Integer max) {
            question.range = new Range(min, max, Range.Type.digits) ;
            return this ;
        }

        public Builder setValueRange(Integer min, Integer max) {
            question.range = new Range(min, max, Range.Type.value) ;
            return this ;
        }


    }

}
