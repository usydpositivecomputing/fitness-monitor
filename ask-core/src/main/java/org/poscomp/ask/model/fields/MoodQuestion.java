package org.poscomp.ask.model.fields;


/**
 * A question where we ask respondent how they feel, how they felt, etc.
 * Intended to be used with angular-mood
 */
public class MoodQuestion extends Question {


    public MoodQuestion() {
        super(Type.mood) ;
    }

    public static class Builder extends Question.Builder<MoodQuestion> {

        private MoodQuestion question ;

        @Override
        public MoodQuestion f() {
            return question ;
        }

        public Builder setId(String id) {
            super.setId(id) ;
            return this ;
        }

        public Builder setQuestion(String question) {
            super.setQuestion(question) ;
            return this ;
        }

        public Builder setNotes(String notes) {
            super.setNotes(notes) ;
            return this ;
        }

        public Builder setTags(String... tags) {
            super.setTags(tags) ;
            return this ;
        }

        public Builder setOptional() {
            super.setOptional() ;
            return this ;
        }

        public Builder hide() {
            super.hide() ;
            return this ;
        }

        public Builder() {
            super() ;
            question = new MoodQuestion() ;
        }

    }

}
