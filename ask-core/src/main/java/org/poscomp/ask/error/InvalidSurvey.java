package org.poscomp.ask.error;

/**
 * Created by dmilne on 26/11/14.
 */
public class InvalidSurvey extends Exception {

    public InvalidSurvey() {
        super() ;
    }

    public InvalidSurvey(String message) {
        super(message) ;
    }

    public InvalidSurvey(String message, Throwable cause) {
        super(message, cause) ;
    }

}
