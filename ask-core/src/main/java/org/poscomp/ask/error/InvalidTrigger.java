package org.poscomp.ask.error;

/**
 * Created by dmilne on 18/12/14.
 */
public class InvalidTrigger extends Exception {

    public InvalidTrigger() {
        super() ;
    }

    public InvalidTrigger(String message) {
        super(message) ;
    }

    public InvalidTrigger(String message, Throwable cause) {
        super(message, cause) ;
    }

}
