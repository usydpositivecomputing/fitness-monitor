package org.poscomp.ask.error;

import org.poscomp.ask.model.fields.Field;

/**
 * Created by dmilne on 26/11/14.
 */
public class InvalidField extends Exception {

    private String fieldId ;

    public InvalidField(Field field) {

        super("Field " + field.getId() + " is invalid") ;
        fieldId = field.getId() ;
    }

    public InvalidField(Field field, String message) {

        super("Field " + field.getId() + " is invalid: " + message) ;
        fieldId = field.getId() ;
    }

    public InvalidField(Field field, String message, Throwable cause) {


        super("Field " + field.getId() + " is invalid: " + message, cause) ;
        fieldId = field.getId() ;
    }

    public String getFieldId() {
        return fieldId ;
    }
}
