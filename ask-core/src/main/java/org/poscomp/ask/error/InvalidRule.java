package org.poscomp.ask.error;

/**
 * Created by dmilne on 26/11/14.
 */
public class InvalidRule extends Exception {

    public InvalidRule() {
        super() ;
    }

    public InvalidRule(String message) {
        super(message) ;
    }

    public InvalidRule(String message, Throwable cause) {
        super(message, cause) ;
    }


}
