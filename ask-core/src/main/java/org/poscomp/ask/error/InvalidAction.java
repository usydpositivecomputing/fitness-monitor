package org.poscomp.ask.error;

import org.poscomp.ask.model.fields.Field;

/**
 * Created by dmilne on 18/12/14.
 */
public class InvalidAction extends Exception {

    public InvalidAction() {
        super() ;
    }

    public InvalidAction(String message) {
        super(message) ;
    }

    public InvalidAction(String message, Throwable cause) {
        super(message, cause) ;
    }

}