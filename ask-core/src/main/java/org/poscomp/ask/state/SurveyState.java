package org.poscomp.ask.state;

import org.poscomp.ask.model.Response;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.fields.Field;
import org.poscomp.ask.model.fields.PageBreak;
import org.poscomp.ask.model.fields.Question;
import org.poscomp.ask.model.logic.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by dmilne on 12/12/14.
 */
public class SurveyState {

    private static Logger logger = LoggerFactory.getLogger(SurveyState.class) ;

    private Map<String,FieldState> fieldStatesById ;

    private List<PageState> pageStates ;
    private Map<String,PageState> pageStatesById ;

    private List<RuleState> fieldRuleStates ;
    private List<RuleState> pageRuleStates ;

    private Response response ;

    public SurveyState(Survey survey, Response response) {

        this.response = response ;

        fieldStatesById = new LinkedHashMap<String, FieldState>() ;
        pageStates = new ArrayList<PageState>() ;
        pageStatesById = new HashMap<String,PageState>() ;


        for (Field f:survey.getFields()) {

            PageState pageState ;

            if (f.getType() == Field.Type.pageBreak) {

                pageState = new PageState((PageBreak) f, pageStates.size());
                pageStates.add(pageState);
                pageStatesById.put(f.getId(), pageState);

            } else {
                if (pageStates.isEmpty()) {
                    pageState = new PageState(null, 0);
                    pageStates.add(pageState);
                } else {
                    pageState = pageStates.get(pageStates.size() - 1) ;
                }
            }

            FieldState fieldState = new FieldState(f, pageStates.size()-1) ;
            fieldStatesById.put(f.getId(), fieldState) ;
            pageState.addRelevantFieldState(fieldState);
        }


        fieldRuleStates = new ArrayList<RuleState>() ;
        for (FieldRule fr:survey.getFieldRules()){

            RuleState ruleState = new RuleState(fr, fieldRuleStates.size()) ;

            for (TriggerState triggerState:ruleState.getTriggerStates())
                fieldStatesById.get(triggerState.getTrigger().getQuestionId()).addTrigger(triggerState);

            fieldRuleStates.add(ruleState) ;
        }

        pageRuleStates = new ArrayList<RuleState>() ;
        for (PageRule pr:survey.getPageRules()){

            RuleState ruleState = new RuleState(pr, pageRuleStates.size()) ;

            for (TriggerState triggerState:ruleState.getTriggerStates())
                fieldStatesById.get(triggerState.getTrigger().getQuestionId()).addTrigger(triggerState);

            pageRuleStates.add(ruleState) ;
        }

        this.handleResponseUpdated(response);
    }


    public List<Field> getRelevantFields(int pageIndex) {

        PageState pageState = pageStates.get(pageIndex) ;

        List<Field> fields = new ArrayList<Field>() ;

        for (FieldState fieldState:pageState.getRelevantFieldStates())
            fields.add(fieldState.getField()) ;

        return fields ;
    }

    public int getPageIndex(PageBreak pagebreak) {

        PageState pageState = pageStatesById.get(pagebreak.getId()) ;

        return pageState.getIndex() ;
    }



    public boolean isQuestion(Field field) {
        FieldState fieldState = fieldStatesById.get(field.getId()) ;
        return fieldState.isQuestion() ;
    }

    public boolean isQuestion(String fieldId) {
        FieldState fieldState = fieldStatesById.get(fieldId) ;
        return fieldState.isQuestion() ;
    }

    public boolean isAnswered(Question question) {
        FieldState fieldState = fieldStatesById.get(question.getId()) ;
        return fieldState.isAnswered() ;
    }

    public boolean isAnswered(String questionId) {
        FieldState fieldState = fieldStatesById.get(questionId) ;
        return fieldState.isAnswered() ;
    }

    public boolean isVisible(Field field) {
        FieldState fieldState = fieldStatesById.get(field.getId()) ;
        return fieldState.isVisible() ;
    }

    public boolean isVisible(String fieldId) {
        FieldState fieldState = fieldStatesById.get(fieldId) ;
        return fieldState.isVisible() ;
    }





    public void handleResponseUpdated(Response response) {

        this.response = response ;

        if (response.getPageIndex() == null) {
            if (response.isCompleted())
                response.setPageIndex(pageStates.size());
            else
                response.setPageIndex(0);
        }

        //check state of triggers for all questions
        for (FieldState fieldState:this.fieldStatesById.values()) {
            logger.debug("Checking field state of " + fieldState.getField().getId()) ;
            if (fieldState.isQuestion())
                this.handleAnswerChanged(fieldState.getField().getId());
        }

        this.handleCurrentPageChanged() ;
    }

    public void handleAnswerChanged(String questionId) {

        logger.debug("Handling question answered " + questionId) ;

        FieldState fieldState = fieldStatesById.get(questionId) ;

        Answer answer = response.getAnswer(questionId) ;

        fieldState.updateIsAnswered(answer);

        Set<RuleState> affectedRuleStates = new HashSet<RuleState>() ;

        for (TriggerState triggerState:fieldState.getRelevantTriggerStates()) {

            logger.debug("Checking trigger " + triggerState.getTrigger()) ;

            boolean triggerStateChanged = triggerState.recheckFireState(answer) ;

            if (triggerStateChanged) {
                logger.debug(" trigger state changed") ;
                affectedRuleStates.add(triggerState.getRuleState()) ;
            }
        }

        for (RuleState ruleState:affectedRuleStates) {
            if (ruleState.recheckFireState())
                handleRuleStateChange(ruleState) ;
        }

    }

    public void handleContinue() {

        logger.debug("handling continue") ;

        if (this.response.getPageIndex() >= pageStates.size()) {
            this.response.setCompletedAt(new Date());
            return;
        }

        PageState currPageState = pageStates.get(this.response.getPageIndex());

        boolean hasMissingFields = false;
        for (FieldState fieldState : currPageState.getRelevantFieldStates()) {

            if (fieldState.getFieldRuleState() == FieldAction.Action.hide)
                continue;

            if (fieldState.isAnswered())
                continue;

            if (!fieldState.isVisible())
                continue;

            if (!fieldState.isQuestion())
                continue;

            Question question = (Question) fieldState.getField() ;

            if (question.isOptional())
                continue ;

            if (question.isHidden())
                continue ;

            logger.debug(fieldState.getField().getId() + " is missing") ;

            fieldState.setMissing(true);
            hasMissingFields = true;
        }

        if (hasMissingFields)
            return;

        PageState nextUnskippedPage = null;

        for (int i = this.response.getPageIndex() + 1; i < this.pageStates.size(); i++) {

            PageState ps = this.pageStates.get(i) ;

            logger.debug("checking page " + ps.getPage().getId() + " skipped:" + ps.isSkipped()) ;

            if (!ps.isSkipped()) {
                nextUnskippedPage = ps;
                break;
            }
        }

        if (nextUnskippedPage != null) {
            this.response.setPageIndex(nextUnskippedPage.getIndex());
        } else {
            this.response.setPageIndex(this.pageStates.size()) ;
            this.response.setCompletedAt(new Date());
        }

        this.handleCurrentPageChanged();
    }

    public void handleBack() {

        this.response.setCompletedAt(null) ;

        if (this.response.getPageIndex() == 0)
            return ;

        PageState prevUnskippedPage = null ;

        for (int i = this.response.getPageIndex() - 1 ; i>=0 ; i--) {

            PageState ps = this.pageStates.get(i) ;

            if (!ps.isSkipped()) {
                prevUnskippedPage = ps ;
                break ;
            }
        }

        if (prevUnskippedPage != null) {
            this.response.setPageIndex(prevUnskippedPage.getIndex()) ;
        } else {
            this.response.setPageIndex(0);
        }

        this.handleCurrentPageChanged() ;
    }

    private void handleCurrentPageChanged() {

        if (this.response.getPageIndex() == null) {
            if (this.response.isCompleted())
                this.response.setPageIndex(this.pageStates.size());
            else
                this.response.setPageIndex(0) ;
        }

        for (FieldState fieldState:this.fieldStatesById.values())
            fieldState.updateVisibility(this.response.getPageIndex());

    }


    private void handleRuleStateChange(RuleState ruleState) {

        logger.debug("handling " + ruleState.getType() + " rule state changed ") ;

        if (ruleState.getType() == RuleState.Type.field)
            handleFieldRuleStateChange((FieldRule)ruleState.getRule(), ruleState.isFired());
        else
            handlePageRuleStateChange((PageRule) ruleState.getRule(), ruleState.getIndex(), ruleState.isFired());
    }

    private void handleFieldRuleStateChange(FieldRule rule, boolean fired) {

        for (FieldAction action:rule.getActions()) {

            logger.debug("Evoking field Action " + action + " fired:" + fired) ;

            FieldState fieldState = fieldStatesById.get(action.getFieldId()) ;

            fieldState.handleActionStateChanged(action, fired) ;


            //if a field gets hidden, wipe any answers to it
            if (fieldState.getFieldRuleState() == FieldAction.Action.hide) {

                Answer<?> answer = response.getAnswer(action.getFieldId()) ;
                if (answer != null) {
                    response.setAnswer(action.getFieldId(), null);
                    this.handleAnswerChanged(action.getFieldId());
                }
            }

            fieldState.updateVisibility(this.response.getPageIndex());
        }



    }

    private void handlePageRuleStateChange(PageRule rule, int ruleIndex, boolean fired) {


        //identify earliest possible page effected by this page rule
        Integer earliestEffectedPageIndex = null ;
        for (Trigger trigger:rule.getTriggers()) {

            FieldState fieldState = fieldStatesById.get(trigger.getQuestionId()) ;

            if (earliestEffectedPageIndex == null || earliestEffectedPageIndex < fieldState.getPageIndex())
                earliestEffectedPageIndex = fieldState.getPageIndex() ;

        }

        earliestEffectedPageIndex ++ ;


        for (PageAction action:rule.getActions()) {

            switch (action.getAction()) {

                case skip:

                    PageState pageToSkip = pageStatesById.get(action.getPageId()) ;

                    if (fired)
                        pageToSkip.setPageRuleState(ruleIndex, true) ;
                    else
                        pageToSkip.setPageRuleState(ruleIndex, false) ;

                    break ;

                case skipTo:

                    PageState pageToSkipTo = pageStatesById.get(action.getPageId()) ;

                    for (int i=earliestEffectedPageIndex ; i<pageToSkipTo.getIndex() ; i++) {

                        if (fired) {
                            //skip all pages between current one and action.page
                            pageStates.get(i).setPageRuleState(ruleIndex, true) ;
                        } else {
                            //cancel any intention to skip this page due to this rule
                            pageStates.get(i).setPageRuleState(ruleIndex, false) ;
                        }
                    }
                    break ;

                case skipToEnd:

                    for (int i=earliestEffectedPageIndex ; i<pageStates.size() ; i++) {

                        if (fired) {
                            //skip all pages between current one and action.page
                            pageStates.get(i).setPageRuleState(ruleIndex, true) ;
                        } else {
                            //cancel any intention to skip this page due to this rule
                            pageStates.get(i).setPageRuleState(ruleIndex, false) ;
                        }
                    }
                    break ;
            }
        }
    }


}
