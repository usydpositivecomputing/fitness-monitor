package org.poscomp.ask.state;

import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.logic.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by dmilne on 15/12/14.
 */
public class TriggerState {

    private static Logger logger = LoggerFactory.getLogger(TriggerState.class) ;

    private RuleState ruleState ;

    private Boolean fired ;

    private Trigger trigger ;

    protected TriggerState(Trigger trigger, RuleState ruleState) {
        this.trigger = trigger ;
        this.ruleState = ruleState ;
    }

    public Boolean isFired() {
        return fired;
    }

    public Trigger getTrigger() {
        return trigger;
    }

    public boolean recheckFireState(Answer answer) {

        boolean fired = trigger.firesOn(answer) ;

        logger.debug("checking " + trigger + ": " + fired) ;

        if (this.fired == null || this.fired != fired) {
            this.fired = fired ;
            return true ;
        }

        return false ;
    }

    public RuleState getRuleState() {
        return ruleState ;
    }

    public String toString() {
        return this.trigger + " : " + this.fired ;
    }
}