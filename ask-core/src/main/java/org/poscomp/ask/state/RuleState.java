package org.poscomp.ask.state;

import org.poscomp.ask.model.logic.FieldRule;
import org.poscomp.ask.model.logic.PageRule;
import org.poscomp.ask.model.logic.Rule;
import org.poscomp.ask.model.logic.Trigger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmilne on 15/12/14.
 */
public class RuleState {

    public enum Type {page,field} ;

    private Type type ;

    private int index ;

    private Rule rule ;

    private List<TriggerState> triggerStates ;

    private Boolean fired ;

    protected RuleState (FieldRule rule, int index) {

        this.type = Type.field ;

        this.rule = rule ;

        this.index = index ;

        this.triggerStates = new ArrayList<TriggerState>() ;

        for (Trigger trigger:rule.getTriggers()) {
            triggerStates.add(new TriggerState(trigger, this)) ;
        }
    }

    protected RuleState(PageRule rule, int index) {

        this.type = Type.page ;

        this.rule = rule ;

        this.index = index ;

        this.triggerStates = new ArrayList<TriggerState>() ;

        for (Trigger trigger:rule.getTriggers()) {
            triggerStates.add(new TriggerState(trigger, this)) ;
        }
    }

    public boolean recheckFireState() {

        boolean fired ;
        if (rule.getOperator() == Rule.Operator.or) {

            fired = false ;

            for (TriggerState t:triggerStates) {
                if (t.isFired() != null && t.isFired()) {
                    fired = true;
                    break;
                }
            }
        } else {

            fired = true ;

            for (TriggerState t:triggerStates) {
                if (t.isFired() == null || !t.isFired()) {
                    fired = false;
                    break;
                }
            }
        }

        if (this.fired == null || this.fired != fired) {
            this.fired = fired ;
            return true ;
        }

        return false ;
    }

    public Type getType() {
        return type;
    }

    public int getIndex() {
        return index;
    }

    public Rule getRule() {
        return rule;
    }

    public List<TriggerState> getTriggerStates() {
        return triggerStates;
    }

    public Boolean isFired() {
        return fired;
    }

    public String toString() {
        return this.rule + " : " + this.fired ;
    }
}
