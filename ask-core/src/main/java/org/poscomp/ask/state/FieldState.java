package org.poscomp.ask.state;

import org.poscomp.ask.model.answers.Answer;
import org.poscomp.ask.model.fields.Field;
import org.poscomp.ask.model.logic.FieldAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmilne on 15/12/14.
 */
public class FieldState {

    private static Logger logger = LoggerFactory.getLogger(FieldState.class) ;

    private Field field ;

    private int pageIndex ;


    private boolean isQuestion ;
    private boolean isVisible ;
    private boolean isMissing ;

    private FieldAction.Action fieldRuleState ;

    private List<TriggerState> triggerStates = new ArrayList<TriggerState>();
    private boolean answered;

    protected FieldState(Field field, int pageIndex) {
        this.field = field ;
        this.pageIndex = pageIndex ;

        switch (field.getType()) {
            case pageBreak:
            case sectionBreak:
                case instruction:
                    isQuestion = false ;
                    break ;
            default:
                isQuestion = true ;
        }
    }



    public void addTrigger(TriggerState trigger) {
        triggerStates.add(trigger) ;
    }

    public void updateVisibility(int currentPageIndex) {

        boolean visible = true;

        if (field.isHidden())
            visible = false ;

        if (fieldRuleState == FieldAction.Action.hide)
            visible = false ;

        if (pageIndex != currentPageIndex)
            visible = false ;

        this.isVisible = visible ;
    }

    public Field getField() {
        return field ;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public boolean isQuestion() {
        return isQuestion;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public boolean isMissing() {
        return isMissing;
    }

    public void setMissing(boolean missing) {
        this.isMissing = missing ;

    }

    public boolean isAnswered() {
        return answered;
    }

    public void updateIsAnswered(Answer<?> answer) {

        if (answer == null)
            this.answered = false;
        else {
            this.answered = answer.isAnswered() ;
        }
    }

    public FieldAction.Action getFieldRuleState() {
        return fieldRuleState;
    }

    public List<TriggerState> getRelevantTriggerStates() {
        return triggerStates;
    }

    public void handleActionStateChanged(FieldAction action, boolean fired) {

        logger.debug(field.getId() + " - " + action + " state changed to " + fired) ;

        switch(action.getAction()) {
            case show:
                if (fired)
                    this.fieldRuleState = FieldAction.Action.show ;
                else
                    this.fieldRuleState = FieldAction.Action.hide ;
                break ;
            case hide:
                if (fired)
                    this.fieldRuleState = FieldAction.Action.hide ;
                else
                    this.fieldRuleState = FieldAction.Action.show ;
                break ;
        }

    }

    public String toString() {

        return field.getId() + " answered: " + answered + " visible: " + isVisible ;
    }
}