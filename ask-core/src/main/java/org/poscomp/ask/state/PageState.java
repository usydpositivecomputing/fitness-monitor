package org.poscomp.ask.state;

import org.poscomp.ask.model.fields.PageBreak;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dmilne on 15/12/14.
 */
public class PageState {

    private static Logger logger = LoggerFactory.getLogger(PageState.class) ;

    private PageBreak page ;

    private int index ;

    private List<FieldState> relevantFieldStates = new ArrayList<FieldState>() ;

    private Map<Integer,Boolean> pageRuleSkips = new HashMap<Integer,Boolean>() ;

    protected PageState(PageBreak page, int index) {
        this.page = page ;
        this.index = index ;
    }

    public void addRelevantFieldState(FieldState fieldState) {
        this.relevantFieldStates.add(fieldState) ;
    }

    public PageBreak getPage() {
        return page;
    }

    public int getIndex() {
        return index;
    }

    public List<FieldState> getRelevantFieldStates() {
        return relevantFieldStates ;
    }

    public void setPageRuleState(int ruleIndex, boolean skip) {

        logger.debug("skip rule " + ruleIndex + ": " + skip) ;

        pageRuleSkips.put(ruleIndex, skip);
    }

    public boolean isSkipped() {

        for (boolean skip:pageRuleSkips.values()) {
            if (skip)
                return true ;
        }
        return false ;
    }

}