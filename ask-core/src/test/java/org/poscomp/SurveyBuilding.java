package org.poscomp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;
import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.error.InvalidRule;
import org.poscomp.ask.error.InvalidSurvey;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.fields.*;
import org.poscomp.ask.model.logic.*;

import static org.poscomp.ask.model.logic.Actions.*;
import static org.poscomp.ask.model.logic.Triggers.*;


/**
 * Created by dmilne on 26/11/14.
 */
public class SurveyBuilding {

    @Test
    public void buildJunkSurvey() throws InvalidField, InvalidRule, JsonProcessingException, InvalidSurvey {

        Survey survey = new Survey.Builder("Simple test survey")
                .addField(
                        new FreetextQuestion.Builder()
                                .setId("qName")
                            .setQuestion("What is your name?")
                            .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                            .setId("qHappy")
                            .setQuestion("Are you happy?")
                            .setChoices("Yes", "No")
                        .build()
                ).addField(
                        new Instruction.Builder()
                            .setId("iGood")
                            .setText("Good for you!")
                            .build()
                ).addField(
                        new FreetextQuestion.Builder()
                                .setId("qBadReason")
                        .setQuestion("Oh no! How come?")
                        .setLength(FreetextQuestion.Length.LONG)
                        .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qHappy").is("Yes"))
                                .addAction(showField("iGood"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qHappy").is("No"))
                                .addAction(showField("qBadReason"))
                                .build()
                ).build() ;

        ObjectMapper mapper = new ObjectMapper() ;
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL) ;


        System.out.println(mapper.writeValueAsString(survey)) ;

    }


   

    @Test
    public void buildMentalHealthLiteratureSurvey() throws InvalidField, InvalidSurvey, JsonProcessingException, InvalidRule {

        Survey survey = new Survey.Builder("Mental health literature survey")
                .addField(
                        new PageBreak.Builder()
                                .setId("pBasics")
                                .setTitle("Basics")
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("fWho")
                                .setQuestion("Who are you?")
                                .setChoices("Rafa", "Dave", "Sazzad")
                                .build()
                ).addField(
                        new FreetextQuestion.Builder()
                                .setId("qCitation")
                                .setQuestion("What paper are you reviewing?")
                                .setNotes("Please give full citation")
                                .setLength(FreetextQuestion.Length.LONG)
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qRelevance")
                                .setQuestion("Is the paper relevant to our survey article?")
                                .setNotes("Does it talk about analysis of textual data for mental health, or text-based interventions?")
                                .setChoices("Yes","No")
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qIsSurvey")
                                .setQuestion("Is this a survey article?")
                                .setChoices("Yes","No")
                                .build()
                ).addField(
                        new NumericQuestion.Builder()
                                .setId("qYear")
                                .setQuestion("When was the page published?")
                                .setDigitRange(4, 4)
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qRating")
                                .setQuestion("How good is the paper?")
                                .setChoices("Excellent","Fair","Poor")
                                .build()
                )


                .addField(
                        new PageBreak.Builder()
                                .setId("pDetails")
                                .setTitle("Details")
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qStages")
                                .setQuestion("What stages are involved?")
                                .addChoice("Data", "The paper talks about some kind of textual resource, and how it might give insights into mental health.")
                                .addChoice("Diagnosis", "The paper applies an algorithm to the data, that provides further insights into mental health.")
                                .addChoice("Intervention", " The paper trials an intervention, where participants use a system, and the result is measured.")
                                .addChoice("Concerns", "The paper expresses opinions about privacy, ethics, or similar concerns.")
                                .allowOther()
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qScale")
                                .setQuestion("What scale is involved?")
                                .addChoice("Individual","The paper aims to identify or treat individuals")
                                .addChoice("Population", "The paper focuses on entire countries/cities")
                                .build()
                )

                .addField(
                        new SectionBreak.Builder()
                                .setId("sData")
                                .setText("Data")
                                .setNotes("What does this paper have to say about gathering textual data for mental health?")
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qDataType")
                                .setQuestion("What kind of data is involved?")
                                .setChoices("Twitter", "Facebook", "Blog articles", "Other social media data", "Other textual data", "Other non-textual data")
                                .build()
                ).addField(
                        new FreetextQuestion.Builder()
                                .setId("qOtherSMData")
                                .setQuestion("What do you mean by **other social media data**?")
                                .build()
                ).addField(
                        new FreetextQuestion.Builder()
                                .setId("qOtherTextData")
                                .setQuestion("What do you mean by **other textual data**?")
                                .build()
                ).addField(
                        new FreetextQuestion.Builder()
                                .setId("qOtherNonTextData")
                                .setQuestion("What do you mean by **other non-textual data**?")
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qDemographic")
                                .setQuestion("Whose data is involved?")
                                .setChoices("General public","Young people","The elderly","Men","Women")
                                .allowOther()
                                .build()
                )

                .addField(
                        new SectionBreak.Builder()
                                .setId("sDiagnosis")
                                .setText("Diagnosis")
                                .setNotes("What does this paper have to say about analysing textual data for mental health?")
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qVocabs")
                                .setQuestion("Does the paper talk about using any vocabularies")
                                .setChoices("ANEW","LIWC","Wordnet","Wikipedia")
                                .allowOther()
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qDataset")
                                .setQuestion("Does the paper talk about using any datasets?")
                                .setChoices("SemEval","Other existing dataset","This paper gathers a new dataset")
                                .build()
                ).addField(
                        new FreetextQuestion.Builder()
                                .setId("qOtherDataset")
                                .setQuestion("What do you mean by **other existing dataset**?")
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qDatasetAvailable")
                                .setQuestion("Is the new dataset available to researchers?")
                                .addChoice("Yes")
                                .addChoice("No, but researchers could apply the same process to gather a similar one")
                                .addChoice("No, and researchers would need special access to data/participants to gather a similar one")
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qDiagnosisGoal")
                                .setQuestion("What were they trying to diagnose/identify?")
                                .setChoices("Depression","Happiness/Contentment","Moods in general")
                                .allowOther()
                                .build()
                )

                .addField(
                        new SectionBreak.Builder()
                                .setId("sIntervention")
                                .setText("Intervention")
                                .setNotes("What does this paper have to say about conduction interventions for mental health?")
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qProblemAddressed")
                                .setQuestion("What do the interventions address?")
                                .setChoices("Mental health", "Physical health")
                                .allowOther()
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qInterventionType")
                                .setQuestion("What kind of intervention does this involve?")
                                .addChoice("Canned text")
                                .addChoice("Automatically generated text", "e.g. NLG")
                                .addChoice("Self generated text", "e.g. self reflection")
                                .addChoice("Other textual intervention")
                                .addChoice("Other non-textual intervention")
                                .build()
                ).addField(
                        new FreetextQuestion.Builder()
                                .setId("qOtherTextIntervention")
                                .setQuestion("What do you mean by **other textual intervention**?")
                                .build()
                ).addField(
                        new FreetextQuestion.Builder()
                                .setId("qOtherNontextIntervention")
                                .setQuestion("What do you mean by **other non-textual intervention**?")
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qTailored")
                                .setQuestion("Is the intervention tailored/personalized for each recipient?")
                                .setChoices("Yes","No")
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qHasTrial")
                                .setQuestion("Does this involve a trial of an intervention?")
                                .setChoices("Yes","No")
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qHasClinicalTrial")
                                .setQuestion("Was this a clinical trial, where outcomes are measured against improvement of symptoms, etc?")
                                .setChoices("Yes","No")
                                .build()
                ).addField(
                        new NumericQuestion.Builder()
                                .setId("qParticipants")
                                .setQuestion("How many people were involved in the trial?")
                                .build()
                )


                .addField(
                        new PageBreak.Builder()
                                .setId("pFollowup")
                                .setTitle("Followup")
                                .build()
                ).addField(
                        new FreetextQuestion.Builder()
                                .setId("qReason")
                                .setQuestion("In one sentence (or so), explain how this paper fits into the survey article")
                                .setLength(FreetextQuestion.Length.LONG)
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qProjects")
                                .setQuestion("Can this paper inform one of these projects?")
                                .setNotes("If so, it should be forwarded to those working on them")
                                .addChoice("Clinic (Video analysis)")
                                .addChoice("Clinic (Questionairre presentation/analysis)")
                                .addChoice("Moderator Assistant (many-to-many dialogs)")
                                .addChoice("Cybermate (one-on-one dialogs)")
                                .addChoice("Movember (experience sampling)")
                                .addChoice("Affect (emotion detection in free text)")
                                .allowOther()
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qRelevantChecklist")
                                .setQuestion("Please check all of the following (for a relevant paper)")
                                .addChoice("I've attached the PDF of this paper to Mendeley")
                                .addChoice("I've tagged this paper in Mendeley with **read:[your_name]**")
                                .addChoice("I've looked through the references and added promising ones to Mendeley")
                                .addChoice("I've looked through top citing papers (in Google Scholar) and added promising ones to Mendeley")
                                .addChoice("I've synced Mendeley")
                                .build()
                ).addField(
                        new MultichoiceQuestion.Builder()
                                .setId("qIrrelevantChecklist")
                                .setQuestion("Please check all of the following (for an irrelevant paper)")
                                .addChoice("I've tagged this paper in Mendeley with **read:[your_name]** and **irrelevant**")
                                .addChoice("I've synced Mendeley")
                                .build()
                )



                .addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qRelevance").is("Yes"))
                                .addAction(showField("qIsSurvey"))
                                .addAction(showField("qYear"))
                                .addAction(showField("qRating"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(multichoiceTrigger("qStages").contains("Data"))
                                .addAction(showField("sData"))
                                .addAction(showField("qDataType"))
                                .addAction(showField("qDemographic"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(multichoiceTrigger("qDataType").contains("Other social media data"))
                                .addAction(showField("qOtherSMData"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(multichoiceTrigger("qDataType").contains("Other textual data"))
                                .addAction(showField("qOtherTextData"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(multichoiceTrigger("qDataType").contains("Other non-textual data"))
                                .addAction(showField("qOtherNonTextData"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(multichoiceTrigger("qStages").contains("Diagnosis"))
                                .addAction(showField("sDiagnosis"))
                                .addAction(showField("qVocabs"))
                                .addAction(showField("qDataset"))
                                .addAction(showField("qDiagnosisGoal"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(multichoiceTrigger("qDataset").contains("Other existing dataset"))
                                .addAction(showField("qOtherDataset"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(multichoiceTrigger("qDataset").contains("This paper gathers a new dataset"))
                                .addAction(showField("qDatasetAvailable"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(multichoiceTrigger("qStages").contains("Intervention"))
                                .addAction(showField("sIntervention"))
                                .addAction(showField("qProblemAddressed"))
                                .addAction(showField("qInterventionType"))
                                .addAction(showField("qHasTrial"))
                                .addAction(showField("qTailored"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(multichoiceTrigger("qInterventionType").contains("Other textual intervention"))
                                .addAction(showField("qOtherTextIntervention"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(multichoiceTrigger("qInterventionType").contains("Other non-textual intervention"))
                                .addAction(showField("qOtherNontextIntervention"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qHasTrial").is("Yes"))
                                .addAction(showField("qHasClinicalTrial"))
                                .addAction(showField("qParticipants"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qRelevance").is("Yes"))
                                .addAction(showField("qReason"))
                                .addAction(showField("qRelevantChecklist"))
                                .addAction(hideField("qIrrelevantChecklist"))
                                .build()
                ).addPageRule(
                        new PageRule.Builder()
                                .addTrigger(singlechoiceTrigger("qRelevance").is("No"))
                                .addAction(skipToPage("pFollowup"))
                                .build()
                ).build() ;


        ObjectMapper mapper = new ObjectMapper() ;
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL) ;


        System.out.println(mapper.writeValueAsString(survey)) ;
    }



}
