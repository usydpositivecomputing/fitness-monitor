package org.poscomp.state;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.error.InvalidRule;
import org.poscomp.ask.error.InvalidSurvey;
import org.poscomp.ask.model.Response;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.NumericAnswer;
import org.poscomp.ask.model.answers.SinglechoiceAnswer;
import org.poscomp.ask.model.fields.*;
import org.poscomp.ask.model.logic.*;
import org.poscomp.ask.state.SurveyState;
import org.poscomp.ask.util.JsonUtils;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.poscomp.ask.model.logic.Actions.*;
import static org.poscomp.ask.model.logic.Triggers.*;

/**
 * Created by dmilne on 17/12/14.
 */
public class ChatScript {

    private Survey survey ;

    @Before
    public void constructSurvey() throws InvalidRule, InvalidSurvey, InvalidField, IOException {

        Survey.Builder surveyBuilder = new Survey.Builder("Reachout Chat script") ;

        surveyBuilder.setCompletionMessage("Great, keep up the good work!\n\nYou can safely close this window now") ;

        PageBreak pIntro = new PageBreak.Builder()
                .setId("pIntro")
                .setTitle("Intro & Consent")
                .setText("Use this script when you start chatting with a visitor. \n\n" +
                        "**Please follow this, and don't take shortcuts.** You can word-smith the individual statements if you like, but they all need to be said to the visitor to satisfy ethics.")
                .build() ;
        surveyBuilder.addField(pIntro) ;


        FreetextQuestion qVisitorId =
                new FreetextQuestion.Builder()
                        .setId("qVisitorId")
                        .setQuestion("Visitor ID")
                        .hide()
                        .build() ;
        surveyBuilder.addField(qVisitorId) ;


        FreetextQuestion qSessionId =
                new FreetextQuestion.Builder()
                        .setId("qSessionId")
                        .setQuestion("Session ID")
                        .hide()
                        .build() ;
        surveyBuilder.addField(qSessionId) ;


        SinglechoiceQuestion qLaunch =
                new SinglechoiceQuestion.Builder()
                        .setId("qLaunch")
                        .setQuestion("Did visitor launch immediately into a question?")
                        .setChoices("Yes","No")
                        .build() ;
        surveyBuilder.addField(qLaunch) ;


        Instruction iHiWithoutQuestion =
                new Instruction.Builder()
                        .setId("iHiWithoutQuestion")
                        .setText("`h1`\n\n > Hi there! =) Before we start chatting, can I just check, how old are you?")
                        .build() ;
        surveyBuilder.addField(iHiWithoutQuestion) ;


        Instruction iHiWithQuestion =
                new Instruction.Builder()
                        .setId("iHiWithQuestion")
                        .setText("`h2`\n\n > I’ll help you out with that real soon. First can I just check, how old are you?")
                        .build() ;
        surveyBuilder.addField(iHiWithQuestion) ;


        NumericQuestion qAge =
                new NumericQuestion.Builder()
                        .setId("qAge")
                        .setQuestion("Visitors age")
                        .setDigitRange(1,3)
                        .build() ;
        surveyBuilder.addField(qAge) ;


        Instruction iUnderage =
                new Instruction.Builder()
                        .setId("iUnderage")
                        .setText(
                                "`ageUnder`\n\n" +
                                        "> I’m really sorry, but I’m not allowed to chat to anyone under 14.  If you need to chat with someone, you can call Kids Helpline on 1800 55 1800. Sorry!"
                        )
                        .setNotes("Follow this with a shortened link to **Kids Helpline**")
                        .build() ;
        surveyBuilder.addField(iUnderage) ;


        Instruction iOverage =
                new Instruction.Builder()
                        .setId("iOverage")
                        .setText(
                                "`ageOver`\n\n" +
                                        "> I’m really sorry, but this service is really aimed at 14-25 year olds. If you are in crisis or urgently need help, you’ll find better help at Lifeline. They have a chat service that runs between 8pm and 4am, or you can call them any time at 13 11 14."
                        )
                        .setNotes("Follow this with a shortened link to **Lifeline**")
                        .build() ;
        surveyBuilder.addField(iOverage) ;


        Instruction iConsentCheck1 =
                new Instruction.Builder()
                        .setId("iConsentCheck1")
                        .setText(
                                "`consentCheck1`\n\n" +
                                        "> Ok, great. I also need you to read something real quick:"
                        )
                        .setNotes("Follow this with a shortened link to **the information sheet**. Make sure this gets followed before continuing.")
                        .build() ;
        surveyBuilder.addField(iConsentCheck1) ;


        Instruction iConsentCheck2 =
                new Instruction.Builder()
                        .setId("iConsentCheck2")
                        .setText(
                                "`consentCheck2`\n\n" +
                                        "> Basically, this chat thing is part of a research study we are doing with Sydney Uni. The link tells you what they are trying to do."
                        )
                        .build() ;
        surveyBuilder.addField(iConsentCheck2) ;


        Instruction iConsentCheck3 =
                new Instruction.Builder()
                        .setId("iConsentCheck3")
                        .setText(
                                "`consentCheck3`\n\n" +
                                        "> So, are you ok with sharing your chat session with these guys, and filling in a short survey afterwards? They will treat your information very carefully and sensitively, and they won’t have any details about who you are. We can still chat even if you don’t want to be part of the study. "
                        )
                        .build() ;
        surveyBuilder.addField(iConsentCheck3) ;


        SinglechoiceQuestion qConsentGiven =
                new SinglechoiceQuestion.Builder()
                        .setId("qConsentGiven")
                        .setQuestion("Is visitor ok to participate in study?")
                        .setChoices("Yes", "No")
                        .build() ;
        surveyBuilder.addField(qConsentGiven) ;


        Instruction iConsentNo =
                new Instruction.Builder()
                        .setId("iConsentNo")
                        .setText(
                                "`consentNo`\n\n" +
                                        "> Ok, no worries. I’ll make sure this chat stays between us, and you won’t need to fill in that survey."
                        )
                        .build() ;
        surveyBuilder.addField(iConsentNo) ;


        Instruction iConsentYes =
                new Instruction.Builder()
                        .setId("iConsentYes")
                        .setText(
                                "`consentYes`\n\n" +
                                        "> Great! If at any point you feel uncomfortable about being in the study, just let me know."
                        )
                        .build() ;
        surveyBuilder.addField(iConsentYes) ;




        PageBreak pMainChat =
                new PageBreak.Builder()
                        .setId("pMainChat")
                        .setTitle("Main chat session")
                        .setText("All right! You are pretty much on your own, until it comes time to close the chat, or if you start to feel that the visitor is in crisis.")
                        .build() ;
        surveyBuilder.addField(pMainChat) ;


        Instruction iHelpNoQuestion =
                new Instruction.Builder()
                        .setId("iHelpNoQuestion")
                        .setText(
                                "`help1`\n\n" +
                                        "> So, what can I help you with?"
                        )
                        .build() ;
        surveyBuilder.addField(iHelpNoQuestion) ;


        Instruction iHelpWithQuestion =
                new Instruction.Builder()
                        .setId("iHelpWithQuestion")
                        .setText(
                                "`help2`\n\n" +
                                        "> So, back to your original question..."
                        )
                        .build() ;
        surveyBuilder.addField(iHelpWithQuestion) ;


        SinglechoiceQuestion qInCrisis =
                new SinglechoiceQuestion.Builder()
                        .setId("qInCrisis")
                        .setQuestion("Has the visitor said anything that indicates they are in crisis?")
                        .setChoices("Yes","No")
                        .build() ;
        surveyBuilder.addField(qInCrisis);


        SinglechoiceQuestion qConsentRevoked =
                new SinglechoiceQuestion.Builder()
                        .setId("qConsentRevoked")
                        .setQuestion("Has the visitor indicated that they no-longer want to be part of the study?")
                        .setChoices("Yes","No")
                        .build() ;
        surveyBuilder.addField(qConsentRevoked);


        Instruction iConsentRevoked =
                new Instruction.Builder()
                        .setId("iConsentRevoked")
                        .setText(
                                "`consentRevoked`\n\n" +
                                        "> Ok, no worries. I’ll make sure this chat stays between us."
                        ).build() ;
        surveyBuilder.addField(iConsentRevoked) ;




        PageBreak pClosing =
                new PageBreak.Builder()
                        .setId("pClosing")
                        .setTitle("Closing chat")
                        .setText("Follow this script to finish up a normal (non-crisis) chat session.")
                        .build() ;
        surveyBuilder.addField(pClosing) ;

        Instruction iSurvey =
                new Instruction.Builder()
                        .setId("iSurvey")
                        .setText(
                                "`survey`\n\n" +
                                        "> Before you go, would you mind completing a short survey for me? It will only take a minute or so. It’s just so we can measure how the chat went, and see what we can improve on."
                        )
                        .setNotes("Follow this with a shortened link to the **Visitor Feedback Survey**")
                        .build() ;
        surveyBuilder.addField(iSurvey) ;

        Instruction iThanks =
                new Instruction.Builder()
                        .setId("iThanks")
                        .setText(
                                "`thanks`\n\n" +
                                        "> Thanks for chatting with me today. I hope I was able to help you out. If you ever need to chat again, just swing by our forums:"
                        )
                        .setNotes("Follow this with a shortened link to the **ReachOut Forums**")
                        .build() ;
        surveyBuilder.addField(iThanks) ;




        PageBreak pEscalation =
                new PageBreak.Builder()
                        .setId("pEscalation")
                        .setTitle("Escalation")
                        .setText("Follow this script if it becomes clear that the visitor is in crisis, and needs to be referred to some other organisation or emergency service.")
                        .build() ;
        surveyBuilder.addField(pEscalation) ;


        SinglechoiceQuestion qAtRisk =
                new SinglechoiceQuestion.Builder()
                        .setId("qAtRisk")
                        .setQuestion("Do you think the visitor is in imminent risk of serious harm?")
                        .setNotes("Ask any questions you need (gently) to figure this out")
                        .addChoice("Yes","I think they have the means and a plan to harm themselves or others")
                        .addChoice("No", "I think they are distressed but do not have any immediate plan to harm themselves or others")
                        .build() ;
        surveyBuilder.addField(qAtRisk) ;


        Instruction iEncourage =
                new Instruction.Builder()
                        .setId("iEncourage")
                        .setText(
                                "`encourage`\n\n" +
                                        "> You are doing the right thing by looking for help and I want you to get the best help possible."
                        )
                        .build() ;
        surveyBuilder.addField(iEncourage) ;


        SinglechoiceQuestion qService =
                new SinglechoiceQuestion.Builder()
                        .setId("qService")
                        .setQuestion("What service is most appropriate for this visitor?")
                        .setNotes("Ask any questions you need (gently) to figure out what service is most appropriate.")
                        .addChoice("Suicide Callback Service", "for suicide ideation")
                        .addChoice("1800 RESPECT", "for sexual assault/domestic violence")
                        .addChoice("Councelling Online", "for drug and alcohol abuse")
                        .addChoice("Mensline", "for general help for Men")
                        .addChoice("Kidsline (kids)", "for general help for children")
                        .addChoice("Kidsline (teens)", "for general help for young adults (under 25)")
                        .addChoice("Lifeline", "for anyone else")
                        .build() ;
        surveyBuilder.addField(qService) ;


        Instruction iScbs =
                new Instruction.Builder()
                        .setId("iScbs")
                        .setText(
                                "`scbs`\n\n" +
                                        "> I think it is best if you talk to Suicide Call Back Service. They have counsellors online right now that can webchat with you or you can give them a call at 1300 659 467."
                        )
                        .setNotes("Follow this with a shortened link to **Suicide Call Back Service**")
                        .build() ;
        surveyBuilder.addField(iScbs) ;


        Instruction iRespect =
                new Instruction.Builder()
                        .setId("iRespect")
                        .setText(
                                "`respect`\n\n" +
                                        "> I think it is best if you talk to 1800 RESPECT. They have counsellors online right now that you can webchat with, or you can call them on 1800 737 732."
                        )
                        .setNotes("Follow this with a shortened link to **1800 RESPECT**")
                        .build() ;
        surveyBuilder.addField(iRespect) ;


        Instruction iDrugs =
                new Instruction.Builder()
                        .setId("iDrugs")
                        .setText(
                                "`drugs`\n\n" +
                                        "> I think it is best if you talk to Counselling Online. They have people you can webchat with right now, or you can call them any time on 1800 888 236."
                        )
                        .setNotes("Follow this with a shortened link to **Counselling Online**")
                        .build() ;
        surveyBuilder.addField(iDrugs) ;


        Instruction iMensline =
                new Instruction.Builder()
                        .setId("iMensline")
                        .setText(
                                "`men`\n\n" +
                                        "> I think it is best if you talk to Mensline. They have counsellors that you can chat to online (by text or video), or you can give them a call on 1300 78 99 78"
                        )
                        .setNotes("Follow this with a shortened link to **Mensline**")
                        .build() ;
        surveyBuilder.addField(iMensline) ;


        Instruction iKids =
                new Instruction.Builder()
                        .setId("iKids")
                        .setText(
                                "`kids`\n\n" +
                                        "> I think it is best if you talk to Kids Line. You can call any time on 1300 78 99 78, or you can web chat with them most days from 12pm to 10pm."
                        )
                        .setNotes("Follow this with a shortened link to **Kidsline (kids)**")
                        .build() ;
        surveyBuilder.addField(iKids) ;


        Instruction iTeens =
                new Instruction.Builder()
                        .setId("iTeens")
                        .setText(
                                "`teens`\n\n" +
                                        "> I think it is best if you talk to Kids Line (please don't be put off by the name, they talk to anyone under 25!). You can call any time on 1300 78 99 78, or you can web chat with them most days from 12pm to 10pm."
                        )
                        .setNotes("Follow this with a shortened link to **Kidsline (teens)**")
                        .build() ;
        surveyBuilder.addField(iTeens) ;


        Instruction iLifeline =
                new Instruction.Builder()
                        .setId("iLifeline")
                        .setText(
                                "`lifeline`\n\n" +
                                        "> I think it is best if you talk to Lifeline. You can call them any time on  13 11 14, or you can chat to them online every evening between 8pm and 4am."
                        )
                        .setNotes("Follow this with a shortened link to **Lifeline chat**")
                        .build() ;
        surveyBuilder.addField(iLifeline) ;


        Instruction iRiskSelfReferral =
                new Instruction.Builder()
                        .setId("iRiskSelfReferral")
                        .setText(
                                "`riskSelfReferral`\n\n" +
                                        "> I’m really worried about you and I don’t want you to die. You need to call 000 or go to the emergency department at your nearest hospital. Is that something you can do? \n\n"
                        )
                        .build() ;
        surveyBuilder.addField(iRiskSelfReferral) ;


        SinglechoiceQuestion qCalled000 =
                new SinglechoiceQuestion.Builder()
                        .setId("qCalled000")
                        .setQuestion("Did visitor call 000")
                        .setChoices("Yes","No")
                        .build() ;
        surveyBuilder.addField(qCalled000) ;


        Instruction iRiskReferral =
                new Instruction.Builder()
                        .setId("iRiskReferral")
                        .setText(
                                "`riskReferral`\n\n" +
                                        "> I’m really worried and I want to make sure you get the help you deserve. Would it be easier if I called emergency services for you? Can you please give me your name, address and phone number so I can get them out to help you? How does that sound?"
                        )
                        .build() ;
        surveyBuilder.addField(iRiskReferral) ;


        SinglechoiceQuestion qGaveDetails =
                new SinglechoiceQuestion.Builder()
                        .setId("qGaveDetails")
                        .setQuestion("Has visitor given you their details?")
                        .setChoices("Yes","No")
                        .build() ;
        surveyBuilder.addField(qGaveDetails) ;


        Instruction iRiskReferred =
                new Instruction.Builder()
                        .setId("iRiskReferred")
                        .setText(
                                "`riskReferred`\n\n" +
                                        "> Ok, I've given those details to 000. You should get a call from them very soon."

                        )
                        .setNotes("Use the existing ReachOut.com escalation processes to engage an external agency - 000.")
                        .build() ;
        surveyBuilder.addField(iRiskReferred) ;


        Instruction iRiskNoncompliant =
                new Instruction.Builder()
                        .setId("iRiskNoncompliant")
                        .setText(
                                "`riskNoncompliant`\n\n" +
                                        "> If you are not up for giving that info to me, I’m going to have to pass on your IP address to emergency services because I’m really concerned about you and I want to make sure you are safe."
                        )
                        .setNotes("Give visitor time to call 000 or give you their details")
                        .build() ;
        surveyBuilder.addField(iRiskNoncompliant) ;


        Instruction iRiskReported =
                new Instruction.Builder()
                        .setId("iRiskReported")
                        .setText(
                                "`riskReported`\n\n" +
                                        "> I'm really worried about you! I've passed on your IP address to emergency services. Please look after yourself until they arrive."

                        )
                        .setNotes("Use the existing ReachOut.com escalation processes to engage the police telecommunications department in the relevant state, using the visitor’s IP address")
                        .build() ;
        surveyBuilder.addField(iRiskReported) ;

        SectionBreak sRiskClosing =
                new SectionBreak.Builder()
                        .setId("sRiskClosing")
                        .setTitle("Closing an escalated chat")
                        .setText("Once you have followed the procedure above, you should stay on the line until the visitor leaves. Keep repeating the same simple messages")
                        .build() ;
        surveyBuilder.addField(sRiskClosing) ;

        Instruction iRiskClosing1 =
                new Instruction.Builder()
                        .setId("iRiskClosing1")
                        .setText(
                                "`riskClosing1`\n\n" +
                                        "> You've bravely taken the step towards us now keep going. Take the next step to `REFERRED_SERVICE`. They are the people who can help you best."
                        )
                        .build() ;
        surveyBuilder.addField(iRiskClosing1);

        Instruction iRiskClosing2 =
                new Instruction.Builder()
                        .setId("iRiskClosing2")
                        .setText(
                                "`riskClosing2`\n\n" +
                                        "> Think about the ways that you will keep safe and promise yourself that before you attempt anything to harm yourself, you will contact `REFERRED_SERVICE`"
                        )
                        .setNotes("Follow this with a shortened link to **Reachout Keep Safe**")
                        .build() ;
        surveyBuilder.addField(iRiskClosing2);


        Instruction iRiskClosing3 =
                new Instruction.Builder()
                        .setId("iRiskClosing3")
                        .setText(
                                "`riskClosing3`\n\n" +
                                        "> You are not alone, contact `REFERRED_SERVICE`"
                        )
                        .build() ;
        surveyBuilder.addField(iRiskClosing3);


        PageBreak pFeedback =
                new PageBreak.Builder()
                        .setId("pFeedback")
                        .setTitle("Feedback")
                        .setText("Fill in this survey once the chat has been concluded")
                        .build() ;
        surveyBuilder.addField(pFeedback) ;


        SinglechoiceQuestion qWorthwhile =
                new SinglechoiceQuestion.Builder()
                        .setId("qWorthwhile")
                        .setQuestion("Was talking to this visitor a good use of your time?")
                        .setChoices("Yes","No")
                        .build() ;
        surveyBuilder.addField(qWorthwhile) ;


        SinglechoiceQuestion qNotWorthwhileReason =
                new SinglechoiceQuestion.Builder()
                        .setId("qNotWorthwhileReason")
                        .setQuestion("How come?")
                        .addChoice("Visitor didn't take chat seriously.")
                        .addChoice("Visitor didn't have anything specific to ask about.")
                        .addChoice("Visitor didn't respond well to my advice/recommendations.")
                        .addChoice("Visitor just wanted help with an assignment.")
                        .allowOther()
                        .build() ;
        surveyBuilder.addField(qNotWorthwhileReason) ;


        SinglechoiceQuestion qDistress =
                new SinglechoiceQuestion.Builder()
                        .setId("qDistress")
                        .setQuestion("Did the visitor seem distressed at any point?")
                        .setChoices("Extremely distressed", "Mildly distressed", "Not at all distressed")
                        .build() ;
        surveyBuilder.addField(qDistress) ;


        MultichoiceQuestion qTopics =
                new MultichoiceQuestion.Builder()
                        .setId("qTopics")
                        .setQuestion("What topics did you talk about?")
                        .setChoices("Tough times", "Wellbeing", "Getting involved")
                        .allowOther()
                        .build() ;
        surveyBuilder.addField(qTopics) ;


        MultichoiceQuestion qToughTimesTopics =
                new MultichoiceQuestion.Builder()
                        .setId("qToughTimesTopics")
                        .setQuestion("What **tough times** topics did you talk about?")
                        .addChoice("Something's not right")
                        .addChoice("Mental health issues")
                        .addChoice("Physical health")
                        .addChoice("Alcohol and other drugs")
                        .addChoice("Bullying, abuse and violence")
                        .addChoice("Loss and grief")
                        .addChoice("Getting help")
                        .allowOther()
                        .build() ;
        surveyBuilder.addField(qToughTimesTopics) ;


        MultichoiceQuestion qWellbeingTopics =
                new MultichoiceQuestion.Builder()
                        .setId("qWellbeingTopics")
                        .setQuestion("What **wellbeing** topics did you talk about?")
                        .addChoice("Mental fitness")
                        .addChoice("Personal identity")
                        .addChoice("School, uni and study")
                        .addChoice("Goals and motivation")
                        .addChoice("Being independent")
                        .addChoice("Social skills")
                        .addChoice("Staying fit and healthy")
                        .addChoice("Friends and family")
                        .addChoice("Sex and relationships")
                        .allowOther()
                        .build() ;
        surveyBuilder.addField(qWellbeingTopics);


        FreetextQuestion qToolFeedback =
                new FreetextQuestion.Builder()
                        .setId("qToolFeedback")
                        .setQuestion("Did this conversation reveal anything great or frustrating about the chat tool or cheat sheet?")
                        .setLength(FreetextQuestion.Length.LONG)
                        .setOptional()
                        .build() ;
        surveyBuilder.addField(qToolFeedback) ;


        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qLaunch).is("Yes"))
                        .addAction(showField(iHiWithQuestion))
                        .addAction(showField(iHelpWithQuestion))
                        .build()
        );

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qLaunch).is("No"))
                        .addAction(showField(iHiWithoutQuestion))
                        .addAction(showField(iHelpNoQuestion))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qLaunch).is("Yes"))
                        .addTrigger(singlechoiceTrigger(qLaunch).is("No"))
                        .withOrOperator()
                        .addAction(showField(qAge))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(numericTrigger(qAge).lt(14))
                        .addAction(showField(iUnderage))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(numericTrigger(qAge).gt(25))
                        .addAction(showField(iOverage))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(numericTrigger(qAge).gt(13))
                        .addTrigger(numericTrigger(qAge).lt(26))
                        .withAndOperator()
                        .addAction(showField(iConsentCheck1))
                        .addAction(showField(iConsentCheck2))
                        .addAction(showField(iConsentCheck3))
                        .addAction(showField(qConsentGiven))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qConsentGiven).is("No"))
                        .addAction(showField(iConsentNo))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qConsentGiven).is("Yes"))
                        .addAction(showField(iConsentYes))
                        .addAction(showField(qConsentRevoked))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qConsentGiven).is("No"))
                        .addTrigger(singlechoiceTrigger(qConsentRevoked).is("Yes"))
                        .withOrOperator()
                        .addAction(hideField(iSurvey))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qConsentRevoked).is("Yes"))
                        .addAction(showField(iConsentRevoked))
                        .build()
        );

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qAtRisk).is("No"))
                        .addAction(showField(iEncourage))
                        .addAction(showField(qService))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qService).is("Suicide Callback Service"))
                        .addAction(showField(iScbs))
                        .build()
        ) ;
        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qService).is("1800 RESPECT"))
                        .addAction(showField(iRespect))
                        .build()
        ) ;
        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qService).is("Councelling Online"))
                        .addAction(showField(iDrugs))
                        .build()
        ) ;
        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qService).is("Mensline"))
                        .addAction(showField(iMensline))
                        .build()
        ) ;
        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qService).is("Kidsline (kids)"))
                        .addAction(showField(iKids))
                        .build()
        ) ;
        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qService).is("Kidsline (teens)"))
                        .addAction(showField(iTeens))
                        .build()
        ) ;
        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qService).is("Lifeline"))
                        .addAction(showField(iLifeline))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qAtRisk).is("Yes"))
                        .addAction(showField(iRiskSelfReferral))
                        .addAction(showField(qCalled000))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qCalled000).is("No"))
                        .addAction(showField(iRiskReferral))
                        .addAction(showField(qGaveDetails))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qGaveDetails).is("Yes"))
                        .addAction(showField(iRiskReferred))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qGaveDetails).is("No"))
                        .addAction(showField(iRiskNoncompliant))
                        .addAction(showField(iRiskReported))
                        .build()
        ) ;


        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qWorthwhile).is("No"))
                        .addAction(showField(qNotWorthwhileReason))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(singlechoiceTrigger(qWorthwhile).is("Yes"))
                        .addAction(showField(qDistress))
                        .addAction(showField(qTopics))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(multichoiceTrigger(qTopics).contains("Tough times"))
                        .addAction(showField(qToughTimesTopics))
                        .build()
        ) ;

        surveyBuilder.addFieldRule(
                new FieldRule.Builder()
                        .addTrigger(multichoiceTrigger(qTopics).contains("Wellbeing"))
                        .addAction(showField(qWellbeingTopics))
                        .build()
        ) ;




        surveyBuilder.addPageRule(
                new PageRule.Builder()
                        .addTrigger(numericTrigger(qAge).lt(14))
                        .addTrigger(numericTrigger(qAge).gt(26))
                        .withOrOperator()
                        .addAction(skipToEnd())
                        .build()
        ) ;

        surveyBuilder.addPageRule(
                new PageRule.Builder()
                        .addTrigger(singlechoiceTrigger(qInCrisis).is("Yes"))
                        .addAction(skipToPage(pEscalation))
                        .build()
        ) ;

        surveyBuilder.addPageRule(
                new PageRule.Builder()
                        .addTrigger(singlechoiceTrigger(qInCrisis).is("No"))
                        .addAction(skipPage(pEscalation))
                        .build()
        ) ;

        Survey tmpSurvey = surveyBuilder.build() ;



        ObjectMapper mapper = JsonUtils.getObjectMapper() ;

        String surveyJson = mapper.writeValueAsString(tmpSurvey) ;

        survey = mapper.readValue(surveyJson, Survey.class) ;
        survey.assertIsComplete();
    }

    @Test
    public void itShouldManageFieldVisiblityInResponseToQLaunch() {

        Response response = new Response(survey, null) ;
        SurveyState state = new SurveyState(survey, response) ;

        assertTrue(state.isVisible("qLaunch")) ;
        assertFalse(state.isVisible("iHiWithoutQuestion")) ;
        assertFalse(state.isVisible("iHiWithQuestion")) ;
        assertFalse(state.isVisible("qAge")) ;

        response.setAnswer("qLaunch", SinglechoiceAnswer.withChoice("yes"));
        state.handleAnswerChanged("qLaunch");
        assertFalse(state.isVisible("iHiWithoutQuestion")) ;
        assertTrue(state.isVisible("iHiWithQuestion")) ;
        assertTrue(state.isVisible("qAge")) ;

        response.setAnswer("qLaunch", SinglechoiceAnswer.withChoice("no"));
        state.handleAnswerChanged("qLaunch");
        assertTrue(state.isVisible("iHiWithoutQuestion")) ;
        assertFalse(state.isVisible("iHiWithQuestion")) ;
        assertTrue(state.isVisible("qAge")) ;
    }

    @Test
    public void itShouldManageFieldVisiblityInResponseToQAge() {

        Response response = new Response(survey, null);
        response.setAnswer("qLaunch", SinglechoiceAnswer.withChoice("yes"));
        SurveyState state = new SurveyState(survey, response);

        assertFalse(state.isVisible("iUnderage")) ;
        assertFalse(state.isVisible("iOverage")) ;
        assertFalse(state.isVisible("iConsentCheck1")) ;
        assertFalse(state.isVisible("iConsentCheck2")) ;
        assertFalse(state.isVisible("iConsentCheck3")) ;


        response.setAnswer("qAge", NumericAnswer.withNumber(12));
        state.handleAnswerChanged("qAge");

        assertTrue(state.isVisible("iUnderage")) ;
        assertFalse(state.isVisible("iOverage")) ;
        assertFalse(state.isVisible("iConsentCheck1")) ;
        assertFalse(state.isVisible("iConsentCheck2")) ;
        assertFalse(state.isVisible("iConsentCheck3")) ;


        response.setAnswer("qAge", NumericAnswer.withNumber(28));
        state.handleAnswerChanged("qAge");

        assertFalse(state.isVisible("iUnderage")) ;
        assertTrue(state.isVisible("iOverage")) ;
        assertFalse(state.isVisible("iConsentCheck1")) ;
        assertFalse(state.isVisible("iConsentCheck2")) ;
        assertFalse(state.isVisible("iConsentCheck3")) ;


        response.setAnswer("qAge", NumericAnswer.withNumber(22));
        state.handleAnswerChanged("qAge");

        assertFalse(state.isVisible("iUnderage")) ;
        assertFalse(state.isVisible("iOverage")) ;
        assertTrue(state.isVisible("iConsentCheck1")) ;
        assertTrue(state.isVisible("iConsentCheck2")) ;
        assertTrue(state.isVisible("iConsentCheck3")) ;
    }


    @Test
    public void itShouldClearAnswersRecursivelyForFieldsThatGetHiddenByFieldRules() {

        Response response = new Response(survey, null);
        response.setAnswer("qLaunch", SinglechoiceAnswer.withChoice("yes"));
        response.setAnswer("qAge", NumericAnswer.withNumber(15));
        response.setAnswer("qConsentGiven", SinglechoiceAnswer.withChoice("no"));
        response.setAnswer("qInCrisis", SinglechoiceAnswer.withChoice("yes"));
        response.setPageIndex(3);


        SurveyState state = new SurveyState(survey, response) ;

        //follow path for at-risk visitor
        response.setAnswer("qAtRisk", SinglechoiceAnswer.withChoice("yes")) ;
        state.handleAnswerChanged("qAtRisk") ;
        assertTrue(state.isVisible("iRiskSelfReferral")) ;

        response.setAnswer("qCalled000", SinglechoiceAnswer.withChoice("no")) ;
        state.handleAnswerChanged("qCalled000") ;
        assertTrue(state.isVisible("iRiskReferral")) ;

        response.setAnswer("qGaveDetails", SinglechoiceAnswer.withChoice("no")) ;
        state.handleAnswerChanged("qGaveDetails") ;
        assertTrue(state.isVisible("iRiskNoncompliant")) ;
        assertTrue(state.isVisible("iRiskReported")) ;


        //now change path to non-at-risk
        response.setAnswer("qAtRisk", SinglechoiceAnswer.withChoice("no")) ;
        state.handleAnswerChanged("qAtRisk") ;

        assertFalse(state.isAnswered("qCalled000")) ;
        assertFalse(state.isAnswered("qGaveDetails")) ;

        assertFalse(state.isVisible("iRiskSelfReferral")) ;
        assertFalse(state.isVisible("iRiskReferral")) ;
        assertFalse(state.isVisible("iRiskNoncompliant")) ;
        assertFalse(state.isVisible("iRiskReported")) ;
    }


    @Test
    public void itShouldManagePageRulesInResponseToQAge() {


        Response response = new Response(survey, null);
        response.setAnswer("qLaunch", SinglechoiceAnswer.withChoice("no"));
        SurveyState state = new SurveyState(survey, response) ;

        response.setAnswer("qAge", NumericAnswer.withNumber(12)) ;
        state.handleAnswerChanged("qAge") ;
        state.handleContinue() ;
        assertTrue(response.isCompleted()) ;

        state.handleBack() ;
        assertTrue(response.getPageIndex() == 0) ;

        response.setAnswer("qAge", NumericAnswer.withNumber(28)) ;
        state.handleAnswerChanged("qAge") ;
        state.handleContinue() ;
        assertTrue(response.isCompleted()) ;

        state.handleBack() ;
        assertTrue(response.getPageIndex() == 0) ;

        response.setAnswer("qAge", NumericAnswer.withNumber(20)) ;
        state.handleAnswerChanged("qAge") ;
        state.handleContinue() ;
        assertFalse(response.isCompleted()) ;
        assertTrue(response.getPageIndex() == 0) ;
    }



}
