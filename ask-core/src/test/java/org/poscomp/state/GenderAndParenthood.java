package org.poscomp.state;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.error.InvalidRule;
import org.poscomp.ask.error.InvalidSurvey;
import org.poscomp.ask.model.Response;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.NumericAnswer;
import org.poscomp.ask.model.answers.SinglechoiceAnswer;
import org.poscomp.ask.model.fields.Instruction;
import org.poscomp.ask.model.fields.NumericQuestion;
import org.poscomp.ask.model.fields.PageBreak;
import org.poscomp.ask.model.fields.SinglechoiceQuestion;
import org.poscomp.ask.model.logic.*;
import org.poscomp.ask.state.SurveyState;
import org.poscomp.ask.util.JsonUtils;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.poscomp.ask.model.logic.Actions.*;
import static org.poscomp.ask.model.logic.Triggers.*;

/**
 * Created by dmilne on 17/12/14.
 */
public class GenderAndParenthood {

    private Survey survey ;

    @Before
    public void constructSurvey() throws InvalidRule, InvalidField, InvalidSurvey, IOException {

        Survey tmpSurvey = new Survey.Builder("Gender and Parenthood")
                .addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qGender")
                                .setQuestion("Gender")
                                .setChoices("Male","Female","I'd rather not say")
                                .build()
                )
                .addField(
                        new NumericQuestion.Builder()
                                .setId("qDependents")
                                .setQuestion("Number of dependents")
                                .build()
                )
                .addField(
                        new PageBreak.Builder()
                                .setId("pMales")
                                .setTitle("Men")
                                .build()
                ).addField(
                        new Instruction.Builder()
                                .setId("iMales")
                                .setText("This is where we ask questions for all men")
                                .build()
                ).addField(
                        new PageBreak.Builder()
                                .setId("pFathers")
                                .setTitle("Fathers")
                                .build()
                ).addField(
                        new Instruction.Builder()
                                .setId("iFathers")
                                .setText("This is where we ask questions for all fathers")
                                .build()
                )
                .addField(
                        new PageBreak.Builder()
                                .setId("pFemales")
                                .setTitle("Women")
                                .build()
                ).addField(
                        new Instruction.Builder()
                                .setId("iFemales")
                                .setText("This is where we ask questions for all women")
                                .build()
                ).addField(
                        new PageBreak.Builder()
                                .setId("pMothers")
                                .setTitle("Mothers")
                                .build()
                ).addField(
                        new Instruction.Builder()
                                .setId("iMothers")
                                .setText("This is where we ask questions for all mothers")
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qGender").is("Male"))
                                .addTrigger(singlechoiceTrigger("qGender").is("Female"))
                                .withOrOperator()
                                .addAction(showField("qDependents"))
                                .build()
                ).addPageRule(
                        new PageRule.Builder()
                                .addTrigger(singlechoiceTrigger("qGender").is("I'd rather not say"))
                                .addAction(skipToEnd())
                                .build()
                ).addPageRule(
                        new PageRule.Builder()
                                .addTrigger(singlechoiceTrigger("qGender").is("Male"))
                                .addAction(skipPage("pFemales"))
                                .addAction(skipPage("pMothers"))
                                .build()
                ).addPageRule(
                        new PageRule.Builder()
                                .addTrigger(singlechoiceTrigger("qGender").is("Female"))
                                .addAction(skipToPage("pFemales"))
                                .build()
                ).addPageRule(
                        new PageRule.Builder()
                                .addTrigger(numericTrigger("qDependents").equals(0))
                                .addAction(skipPage("pMothers"))
                                .addAction(skipPage("pFathers"))
                                .build()
                ).build() ;

        ObjectMapper mapper = JsonUtils.getObjectMapper() ;

        String surveyJson = mapper.writeValueAsString(tmpSurvey) ;

        survey = mapper.readValue(surveyJson, Survey.class) ;
        survey.assertIsComplete();
    }

    @Test
    public void itShouldHandleNoGender() {

        Response response = new Response(survey, null) ;
        response.setAnswer("qGender", SinglechoiceAnswer.withChoice("I'd rather not say"));

        SurveyState state = new SurveyState(survey, response) ;
        state.handleContinue();

        assertTrue(response.isCompleted()) ;

        state.handleBack() ;
        assertTrue(response.getPageIndex() == 0); ;
        assertFalse(response.isCompleted()) ;
    }

    @Test
    public void itShouldHandleMaleWithNoDependents() {

        Response response = new Response(survey, null);
        response.setAnswer("qGender", SinglechoiceAnswer.withChoice("Male"));
        response.setAnswer("qDependents", NumericAnswer.withNumber(0));

        SurveyState state = new SurveyState(survey, response);

        state.handleContinue();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pMales")));
        assertFalse(response.isCompleted());

        state.handleContinue();
        assertTrue(response.isCompleted());

        state.handleBack();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pMales")));
        assertFalse(response.isCompleted());

        state.handleBack();
        assertTrue(response.getPageIndex() == 0);
        assertFalse(response.isCompleted());
    }

    @Test
    public void itShouldHandleMaleWithDependents() {

        Response response = new Response(survey, null);
        response.setAnswer("qGender", SinglechoiceAnswer.withChoice("Male"));
        response.setAnswer("qDependents", NumericAnswer.withNumber(3));

        SurveyState state = new SurveyState(survey, response);

        state.handleContinue();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pMales")));
        assertFalse(response.isCompleted());

        state.handleContinue();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pFathers")));
        assertFalse(response.isCompleted());

        state.handleContinue();
        assertTrue(response.isCompleted());

        state.handleBack();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pFathers")));
        assertFalse(response.isCompleted());

        state.handleBack();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pMales")));
        assertFalse(response.isCompleted());

        state.handleBack();
        assertTrue(response.getPageIndex() == 0);
        assertFalse(response.isCompleted());

    }

    @Test
    public void itShouldHandleFemaleWithNoDependents() {

        Response response = new Response(survey, null);
        response.setAnswer("qGender", SinglechoiceAnswer.withChoice("Female"));
        response.setAnswer("qDependents", NumericAnswer.withNumber(0));

        SurveyState state = new SurveyState(survey, response);

        state.handleContinue();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pFemales")));
        assertFalse(response.isCompleted());

        state.handleContinue();
        assertTrue(response.isCompleted());

        state.handleBack();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pFemales")));
        assertFalse(response.isCompleted());

        state.handleBack();
        assertTrue(response.getPageIndex() == 0);
        assertFalse(response.isCompleted());
    }

    @Test
    public void itShouldHandleFemaleWithDependents() {

        Response response = new Response(survey, null);
        response.setAnswer("qGender", SinglechoiceAnswer.withChoice("Female"));
        response.setAnswer("qDependents", NumericAnswer.withNumber(1));

        SurveyState state = new SurveyState(survey, response);

        state.handleContinue();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pFemales")));
        assertFalse(response.isCompleted());

        state.handleContinue();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pMothers")));
        assertFalse(response.isCompleted());

        state.handleContinue();
        assertTrue(response.isCompleted());

        state.handleBack();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pMothers")));
        assertFalse(response.isCompleted());

        state.handleBack();
        assertTrue(response.getPageIndex() == state.getPageIndex((PageBreak) survey.getField("pFemales")));
        assertFalse(response.isCompleted());

        state.handleBack();
        assertTrue(response.getPageIndex() == 0);
        assertFalse(response.isCompleted());

    }

}
