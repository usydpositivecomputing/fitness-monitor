package org.poscomp.state;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.poscomp.ask.error.InvalidField;
import org.poscomp.ask.error.InvalidRule;
import org.poscomp.ask.error.InvalidSurvey;
import org.poscomp.ask.model.Response;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.answers.MoodAnswer;
import org.poscomp.ask.model.answers.NumericAnswer;
import org.poscomp.ask.model.answers.SinglechoiceAnswer;
import org.poscomp.ask.model.fields.FreetextQuestion;
import org.poscomp.ask.model.fields.MoodQuestion;
import org.poscomp.ask.model.fields.NumericQuestion;
import org.poscomp.ask.model.fields.SinglechoiceQuestion;
import org.poscomp.ask.model.logic.FieldAction;
import org.poscomp.ask.model.logic.FieldRule;
import org.poscomp.ask.model.logic.SinglechoiceTrigger;
import org.poscomp.ask.state.SurveyState;
import org.poscomp.ask.util.JsonUtils;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.poscomp.ask.model.logic.Actions.showField;
import static org.poscomp.ask.model.logic.Triggers.singlechoiceTrigger;

/**
 * Created by dmilne on 17/12/14.
 */
public class ChatVisitorFeedback {

    private Survey survey ;

    @Before
    public void constructSurvey() throws InvalidRule, InvalidField, InvalidSurvey, IOException {

        Survey tmpSurvey = new Survey.Builder("Visitor Feedback")
                .setCompletionMessage("Great, thank you for this!\n\nIf you ever need to chat again, then please jump on [our forums](http://link.poscomp.org/l/dk) and we’ll be in touch!")
                .addField(
                        new FreetextQuestion.Builder()
                                .setId("qVisitorId")
                                .setQuestion("What is your Visitor ID?")
                                .hide()
                                .build()
                )
                .addField(
                        new FreetextQuestion.Builder()
                                .setId("qSessionId")
                                .setQuestion("What is your Session ID?")
                                .hide()
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qGender")
                                .setQuestion("What gender are you?")
                                .setChoices("Male","Female","More options")
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qGender2")
                                .setQuestion("What best describes you?")
                                .addChoice("Transgender or Transexual (FtM)")
                                .addChoice("Transgender or Transexual (MtF)")
                                .addChoice("Genderqueer")
                                .addChoice("Androgynous")
                                .addChoice("None of these options describe me")
                                .build()
                ).addField(
                        new NumericQuestion.Builder()
                                .setId("qPostcode")
                                .setQuestion("What is your postcode?")
                                .setDigitRange(4,4)
                                .build()
                ).addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qForumMember")
                                .setQuestion("Are you a member of the ReachOut forums")
                                .setChoices("Yes","No")
                                .build())
                .addField(

                        new MoodQuestion.Builder()
                                .setId("qMoodBefore")
                                .setQuestion("How did you feel **before** you started chatting with us today?")
                                .build())
                .addField(
                        new MoodQuestion.Builder()
                                .setId("qMoodNow")
                                .setQuestion("How do you feel **now**?")
                                .build())
                .addField(
                        new SinglechoiceQuestion.Builder()
                                .setId("qWorthwhile")
                                .setQuestion("Did you find this chat session worthwhile? Did you get the help you needed?")
                                .setChoices("Yes", "No")
                                .build()
                ).addField(
                        new FreetextQuestion.Builder()
                                .setId("qSuggestions")
                                .setOptional()
                                .setQuestion("We are very sorry about that. Do you have any suggestions about what we could have done better?")
                                .setLength(FreetextQuestion.Length.LONG)
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qGender").is("More options"))
                                .addAction(showField("qGender2"))
                                .build()
                ).addFieldRule(
                        new FieldRule.Builder()
                                .addTrigger(singlechoiceTrigger("qWorthwhile").is("No"))
                                .addAction(showField("qSuggestions"))
                                .build()
                ).build() ;

        ObjectMapper mapper = JsonUtils.getObjectMapper() ;

        String surveyJson = mapper.writeValueAsString(tmpSurvey) ;

        survey = mapper.readValue(surveyJson, Survey.class) ;
        survey.assertIsComplete();
    }




    @Test
    public void itShouldManageVisiblityOfQGender2() {

        Response response = new Response(survey, null) ;
        SurveyState state = new SurveyState(survey, response) ;

        assertFalse(state.isVisible("qGender2")) ;

        response.setAnswer("qGender", SinglechoiceAnswer.withChoice("male"));
        state.handleAnswerChanged("qGender");
        assertFalse(state.isVisible("qGender2")) ;

        response.setAnswer("qGender", SinglechoiceAnswer.withChoice("female"));
        state.handleAnswerChanged("qGender");
        assertFalse(state.isVisible("qGender2")) ;

        response.setAnswer("qGender", SinglechoiceAnswer.withChoice("more options"));
        state.handleAnswerChanged("qGender");
        assertTrue(state.isVisible("qGender2")) ;
    }


    @Test
    public void itShouldManageVisiblityOfQSuggestions() {

        Response response = new Response(survey, null) ;
        SurveyState state = new SurveyState(survey, response) ;

        assertFalse(state.isVisible("qSuggestions")) ;

        response.setAnswer("qWorthwhile", SinglechoiceAnswer.withChoice("yes"));
        state.handleAnswerChanged("qWorthwhile");
        assertFalse(state.isVisible("qSuggestions")) ;

        response.setAnswer("qWorthwhile", SinglechoiceAnswer.withChoice("no"));
        state.handleAnswerChanged("qWorthwhile");
        assertTrue(state.isVisible("qSuggestions")) ;
    }

    @Test
    public void isShouldOnlyBlockSubmissionForMandatoryFieldsThatAreVisible() {

        Response response = new Response(survey, null) ;
        response.setAnswer("qGender", SinglechoiceAnswer.withChoice("More options"));

        response.setAnswer("qPostcode", NumericAnswer.withNumber(2095));
        response.setAnswer("qForumMember", SinglechoiceAnswer.withChoice("yes"));
        response.setAnswer("qMoodBefore", MoodAnswer.withMood("discouraged", -0.5, -0.3));
        response.setAnswer("qMoodNow", MoodAnswer.withMood("chill", 0.3, -0.5));
        response.setAnswer("qWorthwhile", SinglechoiceAnswer.withChoice("no"));

        SurveyState state = new SurveyState(survey, response) ;

        state.handleContinue();
        assertFalse(response.isCompleted()) ;

        response.setAnswer("qGender", SinglechoiceAnswer.withChoice("male"));
        state.handleAnswerChanged("qGender");
        state.handleContinue();
        assertTrue(response.isCompleted()) ;
    }
}
