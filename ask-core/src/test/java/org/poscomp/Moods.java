package org.poscomp;

import org.junit.Test;
import org.poscomp.ask.model.data.Mood;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by dmilne on 10/12/14.
 */
public class Moods {

    @Test
    public void testMoodDistance() {

        Mood enthusiastic = new Mood("enthusiastic",0.5,0.5) ;
        Mood enthusiastic2 = new Mood("enthusiastic",0.5,0.5) ;

        Mood proud = new Mood("proud", 0.7, 0.3) ;

        Mood playful = new Mood("playful", 0.7, 0.1) ;

        assertTrue(enthusiastic.equals(enthusiastic2)) ;
        assertFalse(enthusiastic.equals(proud)) ;

        assertTrue(enthusiastic.isNear(proud)) ;
        assertTrue(proud.isNear(playful)) ;

        assertFalse(enthusiastic.isNear(playful)) ;

        assertEquals(enthusiastic.getQuadrant(), Mood.Quadrant.ne) ;
    }


}
