package org.poscomp.parsing;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.poscomp.ask.error.InvalidSurvey;
import org.poscomp.ask.error.InvalidTrigger;
import org.poscomp.ask.model.Survey;
import org.poscomp.ask.model.fields.Instruction;
import org.poscomp.ask.model.fields.NumericQuestion;
import org.poscomp.ask.model.fields.PageBreak;
import org.poscomp.ask.model.fields.SinglechoiceQuestion;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertTrue;

/**
 * Created by dmilne on 17/12/14.
 */
public class Parsing {


    @Test
    public void loadGenderAndParenthood() throws IOException, InvalidSurvey, InvalidTrigger {

        InputStream is = Parsing.class.getResourceAsStream("/surveys/genderAndParenthood.json");

        String json = IOUtils.toString(is, "UTF-8") ;

        Survey survey = Survey.fromJson(json) ;

        survey.assertIsComplete();



        assertTrue(survey.getField("qGender") instanceof SinglechoiceQuestion) ;
        assertTrue(survey.getField("qDependents") instanceof NumericQuestion) ;

        assertTrue(survey.getField("pMales") instanceof PageBreak) ;
        assertTrue(survey.getField("iMales") instanceof Instruction) ;




    }
}
