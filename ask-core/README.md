#The Ask survey platform

Ask is a platform for conducting surveys. 

Ask surveys are defined using a schema that describes the fields (pages, questions, instructions, etc) in the survey, and rules for deciding which fields and pages get shown or hidden. The fields and rules are very similar to those provided by Wufoo. 

The survey schemas are intended to be shared easily, so they can be executed upon in different environments (e.g. in Java for server-side, in Javascript for client-side or within an Ionic app). This is in contrast to platforms like Wufoo, Surveymonkey, etc, which expect you to only use the html pages or widgets that they provide. 

#Ask Core

This **ask-core** project focuses on building and validating Ask schemas in Java, and letting you serialize/deserialize 